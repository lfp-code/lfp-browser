var scriptSrcs = JSON.parse("{{{SCRIPT_SRCS}}}");
for (var scriptSrc of scriptSrcs) {
	var id = scriptSrc.id;
	var url = scriptSrc.url;
	var isExtensionURL = scriptSrc.extensionURL;
	var script = document.createElement("script");
	if (isExtensionURL)
		script.src = chrome.extension.getURL(url);
	else
		script.src = url;
	script.setAttribute("id", id);

	script.async = false;
	document.documentElement.appendChild(script);
	console.log("injected headless detection prevention script:" + JSON.stringify(scriptSrc));
}