const browserNamespace = chrome === undefined || chrome === null || chrome === '';
const browser = browserNamespace ? browser : chrome;
const PROXY_HOST = "{{{PROXY_HOST}}}";
const PROXY_PORT = parseInt("{{{PROXY_PORT}}}");
const PROXY_USERNAME = "{{{PROXY_USER}}}";
const PROXY_PASSWORD = "{{{PROXY_PASS}}}";

var config = {
	mode: "fixed_servers",
	rules: {
		singleProxy: {
			scheme: "http",
			host: PROXY_HOST,
			port: PROXY_PORT
		},
		bypassList: ["localhost"]
	}
};


browser.proxy.settings.set({
	value: config,
	scope: 'regular'
}, function () { });



function callbackFn(details) {
	if (!details)
		return;
	if (!details.isProxy)
		return;
	if (!details.challenger)
		return;
	if (details.challenger.host !== PROXY_HOST)
		return;
	if (details.challenger.port != PROXY_PORT)
		return;
	console.log(JSON.stringify(details))
	return {
		authCredentials: {
			username: PROXY_USERNAME,
			password: PROXY_PASSWORD
		}
	};
}

browser.webRequest.onAuthRequired.addListener(
	callbackFn, {
	urls: ["<all_urls>"]
},
	['blocking']);
