var requestJson = arguments[0];
console.log(requestJson);
var request = JSON.parse(requestJson);
var callback = arguments[arguments.length - 1];

function toResponse(xhr) {
	var response = {};
	response.readyState = xhr.readyState;
	response.status = xhr.status;
	response.statusText = xhr.statusText;
	response.headers = xhr.getAllResponseHeaders();
	response.body = xhr.responseText;
	return response;
}

var xhr = new XMLHttpRequest();
xhr.open(request.method, request.uri, true);
if (request.headers)
	Object.keys(request.headers).forEach(function (key) {
		xhr.setRequestHeader(key, request.headers[key])
	});

xhr.onload = function (e) {
	if (xhr.readyState === 4)
		callback(JSON.stringify(toResponse(xhr)));
};
xhr.onerror = function (e) {
	callback(JSON.stringify(toResponse(xhr)));
};
xhr.send(null);
