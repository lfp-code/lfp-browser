function setup(arguments) {

    var headerTracker = {};
    var headerNameFilters = []
    for (var headerNameFilter of arguments) {
        if (headerNameFilter == null || headerNameFilter == "")
            continue
        headerNameFilters.push(new RegExp(headerNameFilter))
    }
    // Reasign the existing setRequestHeader function to
    // something else on the XMLHtttpRequest class
    XMLHttpRequest.prototype.wrappedSetRequestHeader =
        XMLHttpRequest.prototype.setRequestHeader;
    // Override the existing setRequestHeader function so that it stores the headers
    XMLHttpRequest.prototype.setRequestHeader = function (name, value) {
        // Call the wrappedSetRequestHeader function first
        // so we get exceptions if we are in an erronous state etc.
        this.wrappedSetRequestHeader(name, value);
        if (name == null || name == "")
            return
        name = name.toLowerCase()
        if (headerNameFilters.length > 0) {
            var match = false
            for (var headerNameFilter of headerNameFilters) {
                if (name.match(headerNameFilter))
                    match = true
            }
            if (!match)
                return
        }
        if (!headerTracker[name])
            headerTracker[name] = [];
        while (true) {
            var index = headerTracker[name].indexOf(value);
            if (index < 0)
                break
            headerTracker[name].splice(index, 1);
        }
        headerTracker[name].push(value)
    }
    var headerTrackerVar = Math.random().toString(36).substring(2, 10) + Math.random().toString(36).substring(2, 10);
    headerTrackerVar = "x" + headerTrackerVar
    window[headerTrackerVar] = headerTracker
    return headerTrackerVar
}

if (typeof arguments == "undefined")
    arguments = []
var result = setup(arguments)
//console.log(result)
return result

