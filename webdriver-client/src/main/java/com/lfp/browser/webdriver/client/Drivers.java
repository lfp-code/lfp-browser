package com.lfp.browser.webdriver.client;

import java.net.URI;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.remote.http.HttpClient;

import com.lfp.browser.hub.service.config.BrowserHubServiceConfig;
import com.lfp.browser.webdriver.client.command.HttpCommandExecutorLFP;
import com.lfp.browser.webdriver.client.command.OkHttpHttpClientFactory;
import com.lfp.browser.webdriver.client.driver.WebDriverLFP;
import com.lfp.browser.webdriver.client.driver.WebDriverReflections;
import com.lfp.browser.webdriver.client.driver.chrome.ChromeDriverLFP;
import com.lfp.browser.webdriver.client.driver.chrome.ChromeOptionsLFP;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.ServiceConfig;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.core.spi.FilterReply;
import okhttp3.OkHttpClient;

public class Drivers {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	static {// configure logging
		LogFilter.addFilter(ctx -> {
			var loggerName = Optional.ofNullable(ctx.getEvent()).map(v -> v.getLoggerName()).orElse(null);
			if (loggerName == null)
				return FilterReply.NEUTRAL;
			if (!"org.apache.http.wire".equals(loggerName))
				return FilterReply.NEUTRAL;
			Level level = ctx.getLoggerLevel();
			if (level == null || level.levelInt < Level.WARN.levelInt)
				return FilterReply.NEUTRAL;
			return FilterReply.DENY;
		});
	}

	// local gen

	public static ChromeDriverLFP chromeLocal() {
		return chromeLocal((ChromeOptionsLFP) null);
	}

	public static ChromeDriverLFP chromeLocal(Proxy proxy) {
		return chromeLocal(new ChromeOptionsLFP(proxy));
	}

	public static ChromeDriverLFP chromeLocal(ChromeOptionsLFP chromeOptions) {
		if (chromeOptions == null)
			chromeOptions = new ChromeOptionsLFP();
		return new ChromeDriverLFP.Local(chromeOptions);
	}

	// chrome gen

	public static ChromeDriverLFP chrome() {
		return chrome((ChromeOptionsLFP) null);
	}

	public static ChromeDriverLFP chrome(Proxy proxy) {
		return chrome(new ChromeOptionsLFP(proxy));
	}

	public static ChromeDriverLFP chrome(ChromeOptionsLFP chromeOptions) {
		return chrome(chromeOptions, Configs.get(BrowserHubServiceConfig.class));
	}

	public static ChromeDriverLFP chrome(ChromeOptionsLFP chromeOptions, ServiceConfig serviceConfig) {
		if (chromeOptions == null)
			chromeOptions = new ChromeOptionsLFP();
		if (serviceConfig instanceof BrowserHubServiceConfig) {
			var webDriverCapabilities = ((BrowserHubServiceConfig) serviceConfig).webDriverCapabilities();
			if (webDriverCapabilities != null)
				for (var ent : webDriverCapabilities.entrySet()) {
					var name = ent.getKey();
					if (Utils.Strings.isBlank(name))
						continue;
					var value = ent.getValue();
					if (value == null)
						value = "";
					chromeOptions.setCapability(name, value);
				}
		}
		return new ChromeDriverLFP.Remote(chromeOptions, createHttpCommandExecutor(serviceConfig));
	}

	public static HttpCommandExecutor createHttpCommandExecutor() {
		return createHttpCommandExecutor(Configs.get(BrowserHubServiceConfig.class));
	}

	public static HttpCommandExecutor createHttpCommandExecutor(ServiceConfig serviceConfig) {
		Function<OkHttpClient.Builder, OkHttpClient.Builder> clientBuilderModifier;
		if (serviceConfig != null)
			clientBuilderModifier = cb -> {
				var proxyURI = serviceConfig.proxyURI();
				if (proxyURI != null && Ok.Clients.getProxy(cb).isEmpty()) {
					var proxy = Proxy.builder(proxyURI).build();
					cb.proxy(proxy.asJdkProxy());
					var proxyAuthenticatorOp = Ok.Clients.createProxyAuthenticator(proxy);
					if (proxyAuthenticatorOp.isPresent())
						cb.proxyAuthenticator(proxyAuthenticatorOp.get());
				}
				cb.addInterceptor(chain -> {
					Map<String, ? extends Iterable<String>> headers = serviceConfig.headers();
					if (headers == null || headers.isEmpty())
						return chain.proceed(chain.request());
					var rb = chain.request().newBuilder();
					var estream = Utils.Lots.streamMultimap(headers);
					estream = estream.filterKeys(Utils.Strings::isNotBlank);
					estream = estream.filterValues(Utils.Strings::isNotBlank);
					estream.forEach(ent -> rb.header(ent.getKey(), ent.getValue()));
					var request = rb.build();
					var response = chain.proceed(request);
					return response;
				});
				return cb;
			};
		else
			clientBuilderModifier = null;
		URI uri;
		if (serviceConfig instanceof BrowserHubServiceConfig)
			uri = ((BrowserHubServiceConfig) serviceConfig).webDriverURI();
		else
			uri = serviceConfig.uri();
		return createHttpCommandExecutor(uri, clientBuilderModifier);
	}

	// options

	public static ChromeOptionsLFP chromeOptions() {
		return chromeOptions(null);
	}

	public static ChromeOptionsLFP chromeOptions(Proxy proxy) {
		return new ChromeOptionsLFP(proxy);
	}

	// executors

	public static HttpCommandExecutorLFP createHttpCommandExecutor(URI uri,
			Function<OkHttpClient.Builder, OkHttpClient.Builder> clientBuilderModifier) {
		HttpClient.Factory factory;
		if (clientBuilderModifier != null)
			factory = OkHttpHttpClientFactory.INSTANCE.withClientModifier(clientBuilderModifier);
		else
			factory = OkHttpHttpClientFactory.INSTANCE;
		return new HttpCommandExecutorLFP(null, uri, factory);
	}

	// get

	public static Optional<WebDriverLFP> tryGet(SearchContext searchContext) {
		if (searchContext instanceof WebDriver)
			return Optional.of((WebDriver) searchContext).map(WebDriverLFP::create);
		if (!(searchContext instanceof RemoteWebElement))
			return Optional.empty();
		var result = WebDriverReflections.getParent((RemoteWebElement) searchContext);
		return Optional.ofNullable(result).map(WebDriverLFP::create);
	}

}
