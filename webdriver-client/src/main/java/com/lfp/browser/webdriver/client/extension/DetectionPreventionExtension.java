package com.lfp.browser.webdriver.client.extension;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import com.lfp.browser.script.stealth.StealthScript;
import com.lfp.browser.webdriver.client.extension.config.BrowserExtensionConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class DetectionPreventionExtension extends ExtensionSupplier.Abs {

	private static final MemoizedSupplier<DetectionPreventionExtension> INSANCE_S = Utils.Functions
			.memoize(() -> new DetectionPreventionExtension());

	public static DetectionPreventionExtension getInstance() {
		return INSANCE_S.get();
	}

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 11;

	private static final String INJECT_JS_FILE_NAME = "inject.js";
	private static final String STEALTH_JS_FILE_NAME = "stealth.js";
	private static final String STEALTH_LFP_JS_FILE_NAME = "stealth-lfp.js";

	// private static final List<String> EXCLUDE_EVASIONS =
	// List.of("navigator.permissions");
	private static final List<String> EXCLUDE_EVASIONS = List.of();

	protected DetectionPreventionExtension() {
	}

	@Override
	protected File getFile() throws IOException {
		String excludeAppend;
		if (!EXCLUDE_EVASIONS.isEmpty())
			excludeAppend = "_" + Utils.Crypto.hashMD5(EXCLUDE_EVASIONS.toArray(Object[]::new)).encodeHex();
		else
			excludeAppend = "";
		return Utils.Files.tempFile(THIS_CLASS, VERSION,
				String.format("ext_%s%s.zip", THIS_CLASS.getName(), excludeAppend));
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void generateFile(File file) throws IOException {
		Map<String, Bytes> data = new LinkedHashMap<>();
		List<ScriptSrc> scriptSrcs = new ArrayList<>();
		{// scripts from stealth library
			String id = "id" + Utils.Crypto.getSecureRandomString();
			var scriptSrcB = ScriptSrc.builder().id(id).extensionURL(true).url(STEALTH_JS_FILE_NAME);
			String script = StealthScript.INSTANCE.get();
			script = script + "\n" + getRemoveElementScript(id);
			scriptSrcs.add(scriptSrcB.build());
			data.put(STEALTH_JS_FILE_NAME, Utils.Bits.from(script));
		}
		{// custom scripts
			String id = "id" + Utils.Crypto.getSecureRandomString();
			var scriptSrcB = ScriptSrc.builder().id(id).extensionURL(true).url(STEALTH_LFP_JS_FILE_NAME);
			var script = Utils.Resources.getResourceAsString(
					Configs.get(BrowserExtensionConfig.class).detectionPreventionStealthLFPJSPath()).get();
			script = script + "\n" + getRemoveElementScript(id);
			scriptSrcs.add(scriptSrcB.build());
			data.put(STEALTH_LFP_JS_FILE_NAME, Utils.Bits.from(script));
		}
		var manifest = Utils.Resources
				.getResourceAsString(Configs.get(BrowserExtensionConfig.class).detectionPreventionManifestJsonPath())
				.get();
		var injectJS = Utils.Resources
				.getResourceAsString(Configs.get(BrowserExtensionConfig.class).detectionPreventionJSPath()).get();
		var stealthLFPJS = Utils.Resources
				.getResourceAsString(Configs.get(BrowserExtensionConfig.class).detectionPreventionStealthLFPJSPath())
				.get();
		injectJS = Utils.Strings.templateApply(injectJS, "SCRIPT_SRCS",
				StringEscapeUtils.escapeEcmaScript(Serials.Gsons.get().toJson(scriptSrcs)));
		data.put(INJECT_JS_FILE_NAME, Utils.Bits.from(injectJS));
		data.put(STEALTH_LFP_JS_FILE_NAME, Utils.Bits.from(stealthLFPJS));
		var extFile = ExtensionUtils.buildExtension(manifest, data.entrySet());
		FileUtils.moveFile(extFile, file);
	}

	private static String getRemoveElementScript(String elementId) {
		return String.format("document.getElementById(\"%s\").remove()", elementId);
	}

	public static void main(String[] args) throws IOException {
		var file = DetectionPreventionExtension.getInstance().get();
		System.out.println(file.getAbsolutePath());
	}

}
