package com.lfp.browser.webdriver.client.driver;

import java.io.File;
import java.net.URI;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.immutables.gson.Gson;
import org.immutables.value.Value;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Interactive;
import org.openqa.selenium.internal.FindsByClassName;
import org.openqa.selenium.internal.FindsByCssSelector;
import org.openqa.selenium.internal.FindsById;
import org.openqa.selenium.internal.FindsByLinkText;
import org.openqa.selenium.internal.FindsByName;
import org.openqa.selenium.internal.FindsByTagName;
import org.openqa.selenium.internal.FindsByXPath;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.threadly.concurrent.future.ListenableFuture;

import com.github.throwable.beanref.BeanRef;
import com.google.common.reflect.TypeToken;
import com.lfp.browser.webdriver.client.Browsers;
import com.lfp.browser.webdriver.client.config.BrowserClientConfig;
import com.lfp.browser.webdriver.client.controls.SelectOptionOptions;
import com.lfp.browser.webdriver.client.driver.ImmutableWebDriverLFP.XMLHttpRequest;
import com.lfp.browser.webdriver.client.driver.ImmutableWebDriverLFP.XMLHttpResponse;
import com.lfp.browser.webdriver.client.driver.capabilities.HasDelegate;
import com.lfp.browser.webdriver.client.driver.capabilities.HasProxy;
import com.lfp.browser.webdriver.client.driver.capabilities.HasSessionId;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.ImmutableEntry;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.cookie.Cookie;
import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.net.cookie.Cookies;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

@ValueLFP.Style
@Value.Enclosing
@SuppressWarnings("deprecation")
public interface WebDriverLFP extends WebDriver, JavascriptExecutor, TakesScreenshot, FindsById, FindsByClassName,
		FindsByLinkText, FindsByName, FindsByCssSelector, FindsByTagName, FindsByXPath, HasInputDevices,
		HasCapabilities, Interactive, HasSessionId, HasProxy, AutoCloseable, WebStorage {

	public static WebDriverLFP create(WebDriver webDriver) {
		Objects.requireNonNull(webDriver);
		if (webDriver instanceof WebDriverLFP)
			return (WebDriverLFP) webDriver;
		return new AbstractWebDriver() {

			@Override
			protected WebDriver createDelegate() {
				return webDriver;
			}

			@Override
			public Capabilities getCapabilities() {
				var caps = Utils.Types.tryCast(webDriver, HasCapabilities.class).map(HasCapabilities::getCapabilities)
						.orElse(null);
				return caps;
			}

		};
	}

	default void get(URI uri) {
		get(uri == null ? null : uri.toString());
	}

	default void render(Document document) {
		if (document == null) {
			render((String) null);
			return;
		}
		document = document.clone();
		Jsoups.relativeToAbsoluteURLs(document);
		URI baseURI = URIs.parse(document.baseUri()).orElse(null);
		if (baseURI == null) {
			render(document.outerHtml());
			return;
		}
		Jsoups.select(document, "head base").toList().forEach(e -> {
			e.remove();
		});
		String html = "<base target=\"_blank\">";
		var size = document.head().childNodeSize();
		if (size > 0)
			document.head().child(0).before(html);
		else
			document.head().append(html);
		Element element = document.head().child(0);
		element.attr("href", baseURI.toString());
		render(document.outerHtml());
	}

	default void render(String html, URI baseURI) {
		if (baseURI == null) {
			render(html);
			return;
		}
		var doc = Jsoups.tryParse(html, null, baseURI).orElse(null);
		if (doc == null) {
			render(html);
			return;
		}
		render(doc);
	}

	default void render(String html) {
		if (html == null)
			html = "";
		String script = "document.location = 'about:blank';\n" + "  document.open();\n"
				+ "  document.write(arguments[0]);\n" + "  document.close();";
		this.executeScript(script, html);
	}

	default Document html() {
		String html = (String) this.executeScript("return document.documentElement.innerHTML");
		URI uri = URIs.parse(this.getCurrentUrl()).orElse(null);
		if (uri == null)
			return Jsoup.parse(html);
		return Jsoup.parse(html, uri.toString());
	}

	default void scrollToTop() {
		this.executeScript("window.scrollTo(0, 0)");
	}

	default void scrollToBottom() {
		this.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	default void zoom(double value) {
		this.executeScript("document.body.style.zoom=( ( arguments[0] * 100 ) + '%' )", value);
	}

	@SuppressWarnings("serial")
	default Supplier<Map<String, List<String>>> trackAjaxHeaders(Pattern... headerNameFilters) {
		var path = Configs.get(BrowserClientConfig.class).trackAjaxHeadersScriptPath();
		var script = Utils.Resources.getResourceAsString(path).get();
		var args = Utils.Lots.stream(headerNameFilters).nonNull().map(Object::toString)
				.filter(Utils.Strings::isNotBlank).distinct().toArray(Object.class);
		var headerTrackerVar = (String) this.executeScript(script, args);
		var resultType = new TypeToken<Map<String, List<String>>>() {}.getType();
		return () -> {
			String js = "var result = window[arguments[0]]; return !result?'{}':JSON.stringify(result)";
			var json = (String) this.executeScript(js, headerTrackerVar);
			return Serials.Gsons.get().fromJson(json, resultType);
		};
	}

	default URI getCurrentURI() {
		return URIs.parse(getCurrentUrl()).orElse(null);
	}

	default void clearLocalStorage() {
		this.executeScript("localStorage.clear();");
	}

	default void clearSessionStorage() {
		this.executeScript("sessionStorage.clear();");
	}

	default EntryStream<URI, Cookie> streamCookies() {
		var scookies = this.manage().getCookies();
		var skipBps = Arrays.asList(BeanRef.$(org.openqa.selenium.Cookie::getExpiry));
		var cookieStream = Utils.Lots.stream(scookies).map(v -> {
			var cb = Cookie.builder();
			JodaBeans.copyToBuilder(v, cb, (k, sup) -> {
				for (var bp : skipBps)
					if (k.equals(bp.getPath()))
						return false;
				return true;
			});
			if (v.getExpiry() == null)
				cb.maxAge(-1);
			else {
				var ttlMillis = v.getExpiry().getTime() - System.currentTimeMillis();
				var ttl = Duration.ofMillis(Math.max(ttlMillis, 0));
				cb.maxAge(ttl.toSeconds());
			}
			return cb.build();
		});
		var uri = getCurrentURI();
		return cookieStream.mapToEntry(v -> uri, v -> v);
	}

	default CookieStoreLFP getCookieStore() {
		var cookieStore = Cookies.createCookieStore();
		streamCookies().forEach(ent -> {
			cookieStore.add(ent.getKey(), ent.getValue().toHttpCookie());
		});
		return cookieStore;
	}

	@Override
	default Optional<Proxy> getProxy() {
		return Utils.Types.tryCast(this.getCapabilities(), HasProxy.class).flatMap(v -> v.getProxy());
	}

	default StreamEx<LogEntry> streamLogEntries(String logType) {
		if (Utils.Strings.isEmpty(logType))
			return StreamEx.empty();
		var logs = this.manage().logs();
		if (logs == null)
			return StreamEx.empty();
		var availableLogTypes = logs.getAvailableLogTypes();
		if (availableLogTypes == null || !availableLogTypes.contains(logType))
			return StreamEx.empty();
		var logEntries = this.manage().logs().get(logType);
		return Utils.Lots.stream(logEntries);
	}

	@SuppressWarnings("serial")
	default EntryStream<URI, HeaderMap> streamLoggedRequestHeaders() {
		var stream = streamLogEntries(LogType.PERFORMANCE);
		var jsonStream = stream.map(v -> {
			return Utils.Functions.catching(() -> Serials.Gsons.getJsonParser().parse(v.getMessage()), t -> null);
		});
		jsonStream = jsonStream.nonNull();
		TypeToken<Map<String, String>> headerJsonTT = new TypeToken<Map<String, String>>() {};
		return jsonStream.map(v -> {
			var method = Serials.Gsons.tryGetAsString(v, "message", "method").orElse(null);
			if (!Utils.Strings.startsWith(method, "Network.requestWillBeSent"))
				return null;
			var headers = Serials.Gsons.tryGetAsJsonObject(v, "message", "params", "request", "headers").orElse(null);
			if (headers == null)
				headers = Serials.Gsons.tryGetAsJsonObject(v, "message", "params", "headers").orElse(null);
			if (headers == null)
				return null;
			Map<String, String> map = Serials.Gsons.get().fromJson(headers, headerJsonTT.getType());
			if (map == null)
				return null;
			var headerMap = HeaderMap.of(map);
			URI uri = Serials.Gsons.tryGetAsString(v, "message", "params", "request", "url").flatMap(URIs::parse)
					.orElse(null);
			if (uri == null)
				uri = Serials.Gsons.tryGetAsString(v, "message", "params", "documentURL").flatMap(URIs::parse)
						.orElse(null);
			return Utils.Lots.entry(uri, headerMap);
		}).nonNull().mapToEntry(Entry::getKey, Entry::getValue);
	}

	default String getUserAgent() {
		return (String) executeScript("return navigator.userAgent;");
	}

	default void ping() {
		Browsers.Controls.ping(this);
	}

	default void until(Duration timeout) {
		Browsers.Controls.until(this, timeout);
	}

	default <X> X untilComplete(Duration timeout, boolean cancelOnTimeout, Future<X> future) throws ExecutionException {
		return Browsers.Controls.untilComplete(this, timeout, cancelOnTimeout, future);
	}

	default <X, T extends Throwable> X untilNotNull(ThrowingSupplier<X, T> supplier) throws T {
		return untilNotNull(null, supplier);
	}

	default <X, T extends Throwable> X untilNotNull(Duration timeout, ThrowingSupplier<X, T> supplier) throws T {
		return Browsers.Controls.untilNotNull(this, fw -> fw.withTimeout(timeout), nil -> {
			return supplier.get();
		});
	}

	default StreamEx<WebElement> select(String... selects) {
		return Browsers.Controls.select(this, selects);
	}

	default StreamEx<WebElement> select(Predicate<WebElement> filter, String... selects) {
		return select(null, filter, selects);
	}

	default StreamEx<WebElement> select(Duration timeout, String... selects) {
		return select(timeout, null, selects);
	}

	default StreamEx<WebElement> select(Duration timeout, Predicate<WebElement> filter, String... selects) {
		return Browsers.Controls.select(this, fw -> fw.withTimeout(timeout), filter, selects);
	}

	default Optional<WebElement> closest(WebElement webElement, String select) {
		if (webElement == null)
			return Optional.empty();
		String script = "return arguments[0].closest(arguments[1]);";
		WebElement we = (WebElement) this.executeScript(script, webElement, select);
		return Optional.ofNullable(we);
	}

	default Select selectOption(WebElement selectWebElement, Consumer<SelectOptionOptions.Builder> selectOptionsConfig,
			String... matchValues) {
		var selectOptionsB = SelectOptionOptions.builder();
		if (selectOptionsConfig != null)
			selectOptionsConfig.accept(selectOptionsB);
		var selectOptions = selectOptionsB.build();
		var html = (String) this.executeScript("return arguments[0].outerHTML", selectWebElement);
		var selectEl = Jsoup.parse(html).select("select").first();
		Objects.requireNonNull(selectEl, "select element not found");
		var predicate = selectOptions.getPredicate(matchValues);
		var optionEls = Utils.Lots.stream(selectEl.select("option")).filter(predicate);
		var select = new Select(selectWebElement);
		selectOptions.getSelectFunction().accept(optionEls, select);
		return select;
	}

	default String newTab() {
		String currentHandle = this.getWindowHandle();
		String newTabHandle = null;
		try {
			String id = "id" + Utils.Crypto.getRandomString();
			String script = "var win=window.open('about:blank','_blank');win.document.write('<html><body style=\"display:none\" id=\"%s\"></body></html>');win.document.close();";
			script = String.format(script, id);
			this.executeScript(script);
			newTabHandle = untilNotNull(() -> {
				var windowHandleStream = this.getWindowHandles().stream();
				windowHandleStream = windowHandleStream.filter(v -> !currentHandle.equals(v));
				Iterator<String> windowHandleIter = windowHandleStream.iterator();
				while (windowHandleIter.hasNext()) {
					var windowHandle = windowHandleIter.next();
					this.switchTo().window(windowHandle);
					var elements = this.findElements(By.cssSelector("#" + id));
					if (!elements.isEmpty())
						return windowHandle;
				}
				return null;
			});
		} finally {
			if (newTabHandle == null)
				this.switchTo().window(currentHandle);
		}
		if (newTabHandle == null)
			throw new IllegalStateException("new tab creation failed");
		return newTabHandle;
	}

	default void newTab(Runnable newTabTask) {
		newTab(newTabTask == null ? null : () -> {
			newTabTask.run();
			return null;
		});
	}

	default <U> U newTab(Supplier<U> newTabSupplier) {
		Objects.requireNonNull(newTabSupplier);
		var currentHandle = this.getWindowHandle();
		var newTabHandle = newTab();
		try {
			return newTabSupplier.get();
		} finally {
			this.switchTo().window(currentHandle);
			this.closeWindow(newTabHandle);
		}
	}

	default String getPublicIPAddress() {
		var url = this.getCurrentUrl();
		if (URIs.parse(url).isEmpty()) {
			// no xhr on funky urls
			return getPublicIPAddressNavigation();
		}
		{// xhr without nav
			var ipOp = getPublicIPAddressXHR();
			if (ipOp.isPresent())
				return ipOp.get();
		}
		return getPublicIPAddressNavigation();
	}

	default String getPublicIPAddressNavigation() {
		return newTab(() -> {
			var cfg = Configs.get(BrowserClientConfig.class);
			this.get(cfg.ipAddressLookupURI().toString());
			for (var el : this.findElements(By.cssSelector("body"))) {
				String text = Utils.Strings.trim(el.getText());
				if (IPs.isValidIpAddress(text))
					return text;
			}
			throw new IllegalArgumentException("unable to obtain public ip address w/ navigation");
		});
	}

	default XMLHttpResponse sendXMLHttpRequest(XMLHttpRequest request) {
		Objects.requireNonNull(request);
		var cfg = Configs.get(BrowserClientConfig.class);
		String script = Utils.Resources.getResourceAsString(cfg.sendXMLHttpRequestScriptPath()).get();
		var jsonRequest = Serials.Gsons.get().toJson(request);
		try {
			var jsonResponse = this.executeAsyncScript(script, jsonRequest).toString();
			return Serials.Gsons.get().fromJson(jsonResponse, XMLHttpResponse.class);
		} catch (Throwable t) {
			return XMLHttpResponse.builder().body(t.getMessage()).build();
		}
	}

	default void addRequestHeader(String name, String value) {
		addRequestHeaders(Arrays.asList(ImmutableEntry.of(name, value)));
	}

	default void addRequestHeaders(Iterable<? extends Entry<String, String>> nameValueEntries) {
		var estream = Streams.of(nameValueEntries).chain(EntryStreams::of).nonNullKeys()
				.mapValues(v -> v != null ? v : "");
		var urlb = UrlBuilder.fromString("https://webdriver.modheader.com/add");
		for (var ent : estream)
			urlb = urlb.addParameter(ent.getKey(), ent.getValue());
		var uri = urlb.toUri();
		newTab(() -> {
			get(uri.toString());
		});
	}

	default void clearRequestHeaders() {
		var uri = URI.create("https://webdriver.modheader.com/clear");
		newTab(() -> {
			get(uri.toString());
		});
	}

	// make optional bc of weird script policies
	default Optional<String> getPublicIPAddressXHR() {
		var cfg = Configs.get(BrowserClientConfig.class);
		var response = sendXMLHttpRequest(XMLHttpRequest.builder().uri(cfg.ipAddressLookupURI()).build());
		String resultStr = response.body();
		if (!IPs.isValidIpAddress(resultStr))
			return Optional.empty();
		return Optional.of(resultStr);
	}

	default String injectStyleTag(String value) {
		var id = "id" + Utils.Crypto.getSecureRandomString();
		injectStyleTag(value, id);
		return id;
	}

	default boolean injectStyleTag(String value, String elementId) {
		Validate.notBlank(elementId);
		var path = Configs.get(BrowserClientConfig.class).cssInjectorScriptPath();
		var script = Utils.Resources.getResourceAsString(path).get();
		var resultObj = this.executeScript(script, elementId, value);
		Boolean result = Utils.Types.tryCast(resultObj, Boolean.class).orElse(null);
		return Boolean.TRUE.equals(result);
	}

	default void hover(WebElement... webElements) {
		var path = Configs.get(BrowserClientConfig.class).hoverScriptPath();
		var script = Utils.Resources.getResourceAsString(path).get();
		this.executeScript(script, Utils.Lots.stream(webElements).nonNull().toArray(Object.class));
	}

	default void sendKeys(WebElement input, int batchSize1, int batchSize2, Duration delay1, Duration delay2,
			CharSequence... keysToSend) {
		var keysToSendAll = Utils.Lots.stream(keysToSend).nonNull().joining();
		int index = 0;
		while (true) {
			int start = index;
			index = index + Utils.Crypto.getRandomInclusive(batchSize1, batchSize2);
			var chunk = Utils.Strings.substring(keysToSendAll, start, index);
			if (chunk.isEmpty())
				break;
			if (delay1 != null || delay2 != null)
				Threads.sleepRandomUnchecked(delay1, delay2);
			input.sendKeys(chunk);
		}
	}

	default void forceClick(WebElement webElement) {
		try {
			webElement.click();
		} catch (ElementNotInteractableException e) {
			this.executeScript("arguments[0].click()", webElement);
		}

	}

	default File screenshot() {
		return screenshot(false);
	}

	default File screenshot(boolean entireViewport) {
		var result = Utils.Files.tempFile(getClass(), "screen-shots", "v1",
				String.format("ss-%s.png", Utils.Crypto.getSecureRandomString()));
		boolean success = false;
		if (entireViewport) {
			try {
				Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(100))
						.takeScreenshot(this);
				ImageIO.write(screenshot.getImage(), "PNG", result);
				success = true;
			} catch (Exception e) {
				// suppress
			}
		}
		if (!success) {
			File outputFile = this.getScreenshotAs(OutputType.FILE);
			try {
				Utils.Functions.unchecked(() -> FileUtils.moveFile(outputFile, result));
			} finally {
				outputFile.delete();
			}
		}
		return result;
	}

	/**
	 * Quits this driver, closing every associated window. NOTE: this is different
	 * from the typical close, which is now closeWindow();
	 */
	@Override
	default void close() {
		close(null);
	}

	default ListenableFuture<Boolean> close(Executor executor) {
		return Browsers.Controls.quitQuietly(this, executor);
	}

	boolean isClosing();

	/**
	 * Close the current window, quitting the browser if it's the last window
	 * currently open.
	 */
	void closeWindow();

	default void closeWindow(String... windowHandles) {
		var windowHandleList = Utils.Lots.stream(windowHandles).filter(Utils.Strings::isNotBlank).distinct().toList();
		if (windowHandleList.isEmpty()) {
			closeWindow();
			return;
		}
		var currentWindowHandle = this.getWindowHandle();
		for (var windowHandle : windowHandleList) {
			if (!this.getWindowHandles().contains(windowHandle))
				continue;
			this.switchTo().window(windowHandle);
			this.closeWindow();
		}
		var postCloseWindowHandles = this.getWindowHandles();
		if (!postCloseWindowHandles.contains(currentWindowHandle)) {
			if (postCloseWindowHandles.isEmpty())
				return;
			else {
				this.switchTo().window(postCloseWindowHandles.iterator().next());
				return;
			}
		}
		this.switchTo().window(currentWindowHandle);
	}

	default void closeOtherWindows(String... exceptHandles) {
		var exceptHandleList = Utils.Lots.stream(exceptHandles).filter(Utils.Strings::isNotBlank).toList();
		if (exceptHandleList.isEmpty())
			exceptHandleList.add(this.getWindowHandle());
		var otherWindowHandles = Utils.Lots.stream(this.getWindowHandles()).filter(v -> !exceptHandleList.contains(v))
				.toArray(String.class);
		if (otherWindowHandles.length == 0)
			return;
		closeWindow(otherWindowHandles);
	}

	default void logSessionInfo() {
		this.logSessionInfo(null);
	}

	default void logSessionInfo(String message) {
		Map<String, Object> logData = getLogSessionInfoData();
		var outParts = Utils.Lots.stream(logData).mapValues(v -> v == null ? null : v.toString())
				.filterValues(Utils.Strings::isNotBlank).map(e -> String.format("%s - %s", e.getKey(), e.getValue()))
				.toList();
		if (outParts.isEmpty())
			return;
		String prepend = Utils.Strings.trimToNullOptional(message).orElse("session info");
		String delim;
		if (logData.size() == 1) {
			prepend += ".";
			delim = " ";
		} else {
			prepend += ":";
			delim = "\n\t";
		}
		var out = Utils.Lots.stream(outParts).prepend(prepend).joining(delim);
		org.slf4j.LoggerFactory.getLogger(this.getClass()).info(out);
	}

	default Map<String, Object> getLogSessionInfoData() {
		WebDriver webDriver = this;
		while (webDriver instanceof HasDelegate) {
			var delegate = ((HasDelegate) webDriver).getDelegate(false);
			if (delegate == null)
				break;
			webDriver = delegate;
		}
		var sessionId = Browsers.Controls.tryGetSessiondId(webDriver).orElse(null);
		if (sessionId == null)
			return Map.of();
		Map<String, Object> logData = new LinkedHashMap<>();
		logData.put("sessionId", sessionId);
		if (!MachineConfig.isDeveloper())
			return logData;
		var remoteServerURI = Utils.Types.tryCast(webDriver, RemoteWebDriver.class).flatMap(v -> {
			return Utils.Types.tryCast(v.getCommandExecutor(), HttpCommandExecutor.class);
		}).map(v -> {
			return Utils.Functions.catching(() -> v.getAddressOfRemoteServer().toURI(), t -> null);
		}).orElse(null);
		if (remoteServerURI == null)
			return logData;
		return logData;
	}

	Date getLastActivityAt();

	@Gson.TypeAdapters
	@Value.Immutable
	public static abstract class AbstractXMLHttpRequest {

		@Value.Default
		public String method() {
			return "GET";
		}

		public abstract URI uri();

		public abstract Map<String, String> headers();

	}

	@Gson.TypeAdapters
	@Value.Immutable
	public static abstract class AbstractXMLHttpResponse {

		private static final String HEADER_TOKEN = ": ";

		public abstract int readyState();

		@Nullable
		public abstract Integer status();

		@Nullable
		public abstract String statusText();

		@Nullable
		abstract String headers();

		@Nullable
		public abstract String body();

		@Value.Lazy
		public HeaderMap headerMap() {
			var lineStream = Utils.Strings.streamLines(headers());
			lineStream = lineStream.filter(Utils.Strings::isNotBlank);
			var estream = lineStream.map(v -> {
				var splitAt = Utils.Strings.indexOf(v, HEADER_TOKEN);
				if (splitAt == -1)
					return ImmutableEntry.of(v, (String) null);
				var name = v.substring(0, splitAt);
				var value = v.substring(splitAt + HEADER_TOKEN.length());
				return ImmutableEntry.of(name, value);
			}).chain(EntryStreams::of);
			return HeaderMap.of(estream);
		}

		@Value.Check
		AbstractXMLHttpResponse normalize() {
			if (status() == null)
				return XMLHttpResponse.copyOf(this).withStatus(StatusCodes.INTERNAL_SERVER_ERROR);
			if (Utils.Strings.isBlank(statusText()))
				return XMLHttpResponse.copyOf(this).withStatusText(StatusCodes.getReason(status()));
			if (headers() != null && Utils.Strings.isEmpty(headers()))
				return XMLHttpResponse.copyOf(this).withHeaders(null);
			if (body() != null && Utils.Strings.isEmpty(body()))
				return XMLHttpResponse.copyOf(this).withBody(null);
			return this;
		}

	}

}
