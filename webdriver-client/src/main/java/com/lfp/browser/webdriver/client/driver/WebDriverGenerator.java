package com.lfp.browser.webdriver.client.driver;

import java.util.function.Consumer;

import org.openqa.selenium.WebDriver;

public interface WebDriverGenerator {

	WebDriver generate(Consumer<WebDriver> sessionIdLoggingCallback);
}
