package com.lfp.browser.webdriver.client.driver.capabilities;

public interface HasVNC {

	boolean isVNCEnabled();

	void setVNCEnabled(boolean enabled);

}
