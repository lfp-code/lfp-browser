package com.lfp.browser.webdriver.client.driver.capabilities;

import java.util.Optional;

import org.openqa.selenium.Dimension;

public interface HasWindowSize {

	void setWindowSize(Dimension dimension);

	Optional<Dimension> getWindowSize();
}
