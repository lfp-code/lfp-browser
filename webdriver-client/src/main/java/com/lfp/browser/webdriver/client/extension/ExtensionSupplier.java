package com.lfp.browser.webdriver.client.extension;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public interface ExtensionSupplier extends ThrowingSupplier<File, IOException> {

	default String getBase64() throws IOException {
		return Bytes.from(get()).encodeBase64();
	}

	public static abstract class Abs implements ExtensionSupplier {

		private final AtomicReference<File> fileRef = new AtomicReference<>();
		private Duration staleDuration;

		public Abs() {
			this(null);
		}

		public Abs(Duration staleDuration) {
			this.staleDuration = staleDuration;
		}

		@Override
		public File get() throws IOException {
			if (fileRef.get() == null)
				synchronized (fileRef) {
					if (fileRef.get() == null)
						fileRef.set(load());
				}

			return fileRef.get();
		}

		private File load() throws IOException {
			File file = Objects.requireNonNull(getFile());
			if (file.exists()) {
				if (staleDuration == null)
					return file;
				long elapsed = System.currentTimeMillis() - file.lastModified();
				if (elapsed < staleDuration.toMillis())
					return file;
				file.delete();
			}
			generateFile(file);
			Validate.isTrue(file.exists(), "file generation failed:%s", file.getAbsolutePath());
			return file;
		}

		protected abstract void generateFile(File file) throws IOException;

		protected abstract File getFile() throws IOException;

	}

	public static class URISource extends ExtensionSupplier.Abs {
		private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();

		private final URI uri;
		private final Object[] hashAppend;

		public URISource(URI uri, Object... hashAppend) {
			this(uri, null, hashAppend);
		}

		public URISource(URI uri, Duration staleDuration, Object... hashAppend) {
			super(staleDuration);
			this.uri = Objects.requireNonNull(uri);
			this.hashAppend = hashAppend;
		}

		@Override
		protected File getFile() throws IOException {
			var hashParts = Streams.<Object>of(uri.toString()).append(Streams.of(hashAppend)).nonNull();
			String hex = Utils.Crypto.hashMD5(hashParts.toArray()).encodeHex();
			var file = Utils.Files.tempFile(THIS_CLASS, "v2", hex, String.format("ext_%s.crx", hex));
			return file;
		}

		@Override
		protected void generateFile(File outFile) throws IOException {
			FileUtils.deleteQuietly(outFile);
			var tempFile = Utils.Files.tempFile(THIS_CLASS, Utils.Crypto.getRandomString());
			try {
				writeURIToFile(uri, tempFile);
				writeToFile(tempFile, outFile);
			} finally {
				tempFile.deleteAll();
			}
		}

		protected void writeToFile(FileExt uriFile, File outFile) throws IOException {
			FileUtils.moveFile(uriFile, outFile);
		}

		protected static void writeURIToFile(URI uri, File file) throws IOException {
			var request = HttpRequests.request().uri(uri);
			var response = HttpClients.send(request);
			try (var is = response.body(); var fos = new FileOutputStream(file)) {
				StatusCodes.validate(response.statusCode(), 2);
				is.transferTo(fos);
			}
		}
	}

}
