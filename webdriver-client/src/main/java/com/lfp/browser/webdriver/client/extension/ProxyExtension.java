package com.lfp.browser.webdriver.client.extension;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.lfp.browser.webdriver.client.extension.config.BrowserExtensionConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;

public class ProxyExtension extends ExtensionSupplier.Abs {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final Proxy proxy;

	public ProxyExtension(Proxy proxy) {
		this.proxy = Objects.requireNonNull(proxy);
		Validate.isTrue(Proxy.Type.HTTP.equals(proxy.getType()), "proxy type not supported:%s", proxy.getType());
	}

	@SuppressWarnings("unchecked")
	@Override
	protected File getFile() throws IOException {
		var cfg = Configs.get(BrowserExtensionConfig.class);
		String manifest = Utils.Resources.getResourceAsString(cfg.proxyManifestJsonPath()).get();
		String background;
		{
			Map<String, Object> params = buildParams(proxy);
			background = Utils.Resources.getResourceAsString(cfg.proxyBackgroundJSPath()).get();
			background = Utils.Strings.templateApply(background, params);
		}
		File result = ExtensionUtils.buildExtension(manifest,
				Utils.Lots.entry("background.js", Bytes.from(background)));
		return result;
	}

	@Override
	protected void generateFile(File file) throws IOException {
	}

	private Map<String, Object> buildParams(Proxy proxy) {
		Map<String, Object> params = new LinkedHashMap<>();
		params.put("PROXY_HOST", proxy.getHostname());
		params.put("PROXY_PORT", proxy.getPort());
		params.put("PROXY_USER", proxy.getUsername().orElse(""));
		params.put("PROXY_PASS", proxy.getPassword().orElse(""));
		return params;
	}

	public Proxy getProxy() {
		return proxy;
	}

	public static void main(String[] args) throws IOException {
		Proxy proxy = Proxy.builder().type(Proxy.Type.HTTP).username("user1").password("pass1")
				.hostname("proxy.fun.com").port(8888).build();
		var file = new ProxyExtension(proxy).get();
		System.out.println(file.getAbsolutePath());
	}

}
