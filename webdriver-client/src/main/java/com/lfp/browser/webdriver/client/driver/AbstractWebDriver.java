package com.lfp.browser.webdriver.client.driver;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.Validate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.SessionStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Interactive;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.internal.FindsByClassName;
import org.openqa.selenium.internal.FindsByCssSelector;
import org.openqa.selenium.internal.FindsById;
import org.openqa.selenium.internal.FindsByLinkText;
import org.openqa.selenium.internal.FindsByName;
import org.openqa.selenium.internal.FindsByTagName;
import org.openqa.selenium.internal.FindsByXPath;
import org.openqa.selenium.remote.SessionId;
import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.browser.webdriver.client.Browsers;
import com.lfp.browser.webdriver.client.driver.capabilities.HasDelegate;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

@SuppressWarnings("deprecation")
public abstract class AbstractWebDriver implements WebDriverLFP, HasDelegate {

	private final MemoizedSupplier<org.slf4j.Logger> loggerSupplier = MemoizedSupplier.create(() -> {
		Class<?> classType = this.getClass();
		var name = classType.getName();
		int splitAt = Utils.Strings.indexOfIgnoreCase(name, "$");
		if (splitAt <= 0)
			return org.slf4j.LoggerFactory.getLogger(classType);
		name = name.substring(0, splitAt);
		classType = JavaCode.Reflections.tryForName(name).orElse(classType);
		return org.slf4j.LoggerFactory.getLogger(classType);
	});
	private final AtomicBoolean quitCalled = new AtomicBoolean();
	private final AtomicReference<SessionId> sessionIdRef = new AtomicReference<>();
	private final MemoizedSupplier<WebDriver> delegateGenerator = Utils.Functions.memoize(() -> {
		return createDelegate();
	});
	private final Date createdAt;
	private boolean closing;
	private Date lastActivityAt;

	public AbstractWebDriver() {
		this.createdAt = new Date();
	}

	@Override
	public SessionId getSessionId() {
		var wd = this.delegateGenerator.getIfLoaded();
		return getSessionIdAndLogUpdates(wd);
	}

	private SessionId getSessionIdAndLogUpdates(WebDriver webDriver) {
		if (webDriver == null)
			return null;
		SessionId webDriverSessionId = Browsers.Controls.tryGetSessiondId(webDriver).orElse(null);
		if (!Objects.equals(this.sessionIdRef.get(), webDriverSessionId))
			synchronized (sessionIdRef) {
				if (!Objects.equals(this.sessionIdRef.get(), webDriverSessionId)) {
					this.sessionIdRef.set(webDriverSessionId);
					logSessionInfo("session created");
				}
			}
		return webDriverSessionId;
	}

	protected org.slf4j.Logger getLogger() {
		return loggerSupplier.get();
	}

	@Override
	public WebDriver getDelegate(boolean create) {
		if (!create)
			return delegateGenerator.getIfLoaded();
		return getDelegateAs(WebDriver.class);
	}

	@SuppressWarnings("unchecked")
	protected <CAST> CAST getDelegateAs(Class<CAST> castType) {
		Objects.requireNonNull(castType);
		WebDriver result;
		synchronized (quitCalled) {
			if (quitCalled.get()) {
				result = this.delegateGenerator.getIfLoaded();
				if (result == null)
					throw new IllegalStateException(
							String.format("browser quit called. can't cast delegate to:%s", castType));
			} else
				result = Objects.requireNonNull(this.delegateGenerator.get(),
						"delegate generator produced null result");
		}
		Validate.isTrue(castType.isAssignableFrom(result.getClass()), "delegate is not instanceof %s", castType);
		getSessionIdAndLogUpdates(result);
		this.lastActivityAt = new Date();
		return (CAST) result;
	}

	@Override
	public void quit() {
		this.closing = true;
		if (!quitCalled.get())
			synchronized (quitCalled) {
				if (!quitCalled.get()) {
					var wd = this.delegateGenerator.getIfLoaded();
					Browsers.Controls.quitQuietly(wd);
					quitCalled.set(true);
				}
			}
	}

	@Override
	public void close() {
		this.closing = true;
		WebDriverLFP.super.close();
	}

	@Override
	public ListenableFuture<Boolean> close(Executor executor) {
		this.closing = true;
		return WebDriverLFP.super.close(executor);
	}

	@Override
	public boolean isClosing() {
		return this.closing;
	}

	@Override
	public void closeWindow() {
		var delegate = getDelegate();
		if (delegate instanceof WebDriverLFP)
			((WebDriverLFP) delegate).closeWindow();
		else
			delegate.close();
	}

	@Override
	public Date getLastActivityAt() {
		return Optional.ofNullable(lastActivityAt).orElse(createdAt);
	}

	protected abstract WebDriver createDelegate();

	@Override
	public void get(String url) {
		getDelegate().get(url);
	}

	@Override
	public String getCurrentUrl() {
		return getDelegate().getCurrentUrl();
	}

	@Override
	public String getTitle() {
		return getDelegate().getTitle();
	}

	@Override
	public List<WebElement> findElements(By by) {
		return getDelegate().findElements(by);
	}

	@Override
	public WebElement findElement(By by) {
		return getDelegate().findElement(by);
	}

	@Override
	public String getPageSource() {
		return getDelegate().getPageSource();
	}

	@Override
	public Set<String> getWindowHandles() {
		return getDelegate().getWindowHandles();
	}

	@Override
	public String getWindowHandle() {
		return getDelegate().getWindowHandle();
	}

	@Override
	public TargetLocator switchTo() {
		return getDelegate().switchTo();
	}

	@Override
	public Navigation navigate() {
		return getDelegate().navigate();
	}

	@Override
	public Options manage() {
		return getDelegate().manage();
	}

	@Override
	public Object executeScript(String script, Object... args) {
		return getDelegateAs(JavascriptExecutor.class).executeScript(script, args);
	}

	@Override
	public Object executeAsyncScript(String script, Object... args) {
		return getDelegateAs(JavascriptExecutor.class).executeAsyncScript(script, args);
	}

	@Override
	public <SS> SS getScreenshotAs(OutputType<SS> target) throws WebDriverException {
		return getDelegateAs(TakesScreenshot.class).getScreenshotAs(target);
	}

	@Override
	public Keyboard getKeyboard() {
		return getDelegateAs(HasInputDevices.class).getKeyboard();
	}

	@Override
	public Mouse getMouse() {
		return getDelegateAs(HasInputDevices.class).getMouse();
	}

	@Override
	public void perform(Collection<Sequence> actions) {
		getDelegateAs(Interactive.class).perform(actions);
	}

	@Override
	public void resetInputState() {
		getDelegateAs(Interactive.class).resetInputState();
	}

	@Override
	public WebElement findElementById(String using) {
		return getDelegateAs(FindsById.class).findElementById(using);
	}

	@Override
	public List<WebElement> findElementsById(String using) {
		return getDelegateAs(FindsById.class).findElementsById(using);
	}

	@Override
	public WebElement findElementByClassName(String using) {
		return getDelegateAs(FindsByClassName.class).findElementByClassName(using);
	}

	@Override
	public List<WebElement> findElementsByClassName(String using) {
		return getDelegateAs(FindsByClassName.class).findElementsByClassName(using);
	}

	@Override
	public WebElement findElementByLinkText(String using) {
		return getDelegateAs(FindsByLinkText.class).findElementByLinkText(using);
	}

	@Override
	public List<WebElement> findElementsByLinkText(String using) {
		return getDelegateAs(FindsByLinkText.class).findElementsByLinkText(using);

	}

	@Override
	public WebElement findElementByPartialLinkText(String using) {
		return getDelegateAs(FindsByLinkText.class).findElementByPartialLinkText(using);
	}

	@Override
	public List<WebElement> findElementsByPartialLinkText(String using) {
		return getDelegateAs(FindsByLinkText.class).findElementsByPartialLinkText(using);
	}

	@Override
	public WebElement findElementByName(String using) {
		return getDelegateAs(FindsByName.class).findElementByName(using);
	}

	@Override
	public List<WebElement> findElementsByName(String using) {
		return getDelegateAs(FindsByName.class).findElementsByName(using);
	}

	@Override
	public WebElement findElementByCssSelector(String using) {
		return getDelegateAs(FindsByCssSelector.class).findElementByCssSelector(using);
	}

	@Override
	public List<WebElement> findElementsByCssSelector(String using) {
		return getDelegateAs(FindsByCssSelector.class).findElementsByCssSelector(using);
	}

	@Override
	public WebElement findElementByTagName(String using) {
		return getDelegateAs(FindsByTagName.class).findElementByTagName(using);
	}

	@Override
	public List<WebElement> findElementsByTagName(String using) {
		return getDelegateAs(FindsByTagName.class).findElementsByTagName(using);
	}

	@Override
	public WebElement findElementByXPath(String using) {
		return getDelegateAs(FindsByXPath.class).findElementByXPath(using);
	}

	@Override
	public List<WebElement> findElementsByXPath(String using) {
		return getDelegateAs(FindsByXPath.class).findElementsByXPath(using);
	}

	@Override
	public LocalStorage getLocalStorage() {
		return getDelegateAs(WebStorage.class).getLocalStorage();
	}

	@Override
	public SessionStorage getSessionStorage() {
		return getDelegateAs(WebStorage.class).getSessionStorage();
	}
}
