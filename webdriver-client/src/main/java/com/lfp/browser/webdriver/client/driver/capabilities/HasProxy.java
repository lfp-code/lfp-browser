package com.lfp.browser.webdriver.client.driver.capabilities;

import java.util.Optional;

import com.lfp.joe.net.proxy.Proxy;

public interface HasProxy {

	Optional<Proxy> getProxy();

}
