package com.lfp.browser.webdriver.client.extension;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Objects;

import org.zeroturnaround.zip.ZipUtil;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.strings.PatternLFP;

public class DisableContentSecurityPolicyExtension extends ExtensionSupplier.URISource {

	public static DisableContentSecurityPolicyExtension getInstance() {
		return Instances.get(DisableContentSecurityPolicyExtension.class, DisableContentSecurityPolicyExtension::new);
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 12;
	private static final String ARCHIVE_URL = "https://github.com/PhilGrayson/chrome-csp-disable/archive/master.zip";

	protected DisableContentSecurityPolicyExtension() {
		super(URI.create(ARCHIVE_URL), VERSION);
	}

	@Override
	protected void writeToFile(FileExt uriFile, File outFile) throws IOException {
		var extensionDir = outFile.getParentFile();
		var outputDir = new FileExt(extensionDir, "extension");
		ZipUtil.unpack(uriFile, outputDir);
		uriFile.deleteAll();
		var backgroundJS = Objects.requireNonNull(findFile(outputDir, "background.js"));
		var manifestJson = Objects.requireNonNull(findFile(backgroundJS.getParentFile(), "manifest.json"));
		{
			var backgroundJSContent = Utils.Files.readFileToString(backgroundJS);
			backgroundJSContent = modifyBackgroundJS(backgroundJSContent,
					PatternLFP.compile("var\\s+isCSPDisabled\\s*?=\\s+.*?{"),
					"\nvar isCSPDisabled =  function () { return true; }\n");
			backgroundJSContent = Utils.Strings.replace(backgroundJSContent, "['*://*/*']", "['<all_urls>']");
			backgroundJSContent = Utils.Strings.replace(backgroundJSContent, "details.responseHeaders[i].value = '';",
					"details.responseHeaders[i].value = 'default-src * data: blob: filesystem: about: ws: wss: \\'unsafe-inline\\' \\'unsafe-eval\\'; script-src * data: blob: \\'unsafe-inline\\' \\'unsafe-eval\\'; connect-src * data: blob: \\'unsafe-inline\\'; img-src * data: blob: \\'unsafe-inline\\'; frame-src * data: blob: ; style-src * data: blob: \\'unsafe-inline\\'; font-src * data: blob: \\'unsafe-inline\\'; frame-ancestors * data: blob:;';");
			Files.writeString(backgroundJS.toPath(), backgroundJSContent, StandardCharsets.UTF_8);
		}
		{
			JsonObject manifest = Serials.Gsons.fromStream(new FileInputStream(manifestJson), JsonObject.class);
			var permissions = manifest.get("permissions").getAsJsonArray();
			permissions.add(new JsonPrimitive("<all_urls>"));
			Files.writeString(manifestJson.toPath(), Serials.Gsons.getPretty().toJson(manifest),
					StandardCharsets.UTF_8);
		}
		ZipUtil.pack(backgroundJS.getParentFile(), outFile);
		outputDir.deleteAll();
	}

	@SuppressWarnings("resource")
	private static File findFile(File file, String name) {
		if (file == null)
			return null;
		if (file.isFile() && file.getName().equals(name))
			return file;
		for (var subFile : Streams.of(file.listFiles()).sortedBy(v -> v.isFile() ? 0 : 1)) {
			var result = findFile(subFile, name);
			if (result != null)
				return result;
		}
		return null;
	}

	private static String modifyBackgroundJS(String backgroundJSContent, PatternLFP pattern, String replace) {
		var matcher = pattern.matcher(backgroundJSContent);
		while (matcher.find()) {
			var start = matcher.start();
			var code = backgroundJSContent.substring(start, backgroundJSContent.length());
			var boundries = Utils.Strings.getBoundaries(code, '{', '}');
			if (boundries == null)
				continue;
			var end = start + boundries.getValue() + 1;
			var pre = backgroundJSContent.substring(0, start);
			pre = Utils.Strings.trim(pre);
			var post = backgroundJSContent.substring(end, backgroundJSContent.length());
			while (true) {
				post = Utils.Strings.trimToNull(post);
				if (!Utils.Strings.startsWith(post, ";"))
					break;
				post = post.substring(1);
			}
			var result = Streams.of(pre, replace, post).joining();
			return result;
		}
		throw new IllegalStateException("unable to modify backgroundJS");
	}

	public static void main(String[] args) throws IOException {
		System.out.println(DisableContentSecurityPolicyExtension.getInstance().get().getAbsoluteFile());
	}
}
