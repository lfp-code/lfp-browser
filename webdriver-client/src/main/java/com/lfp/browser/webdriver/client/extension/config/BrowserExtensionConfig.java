package com.lfp.browser.webdriver.client.extension.config;

import org.aeonbits.owner.Config;

public interface BrowserExtensionConfig extends Config {

	@DefaultValue("com/lfp/browser/client/extension/proxy/background.js")
	String proxyBackgroundJSPath();

	@DefaultValue("com/lfp/browser/client/extension/proxy/manifest.json")
	String proxyManifestJsonPath();

	@DefaultValue("com/lfp/browser/client/extension/detection-prevention/inject.js")
	String detectionPreventionJSPath();

	@DefaultValue("com/lfp/browser/client/extension/detection-prevention/manifest.json")
	String detectionPreventionManifestJsonPath();

	@DefaultValue("com/lfp/browser/client/extension/detection-prevention/stealth-lfp.js")
	String detectionPreventionStealthLFPJSPath();
}
