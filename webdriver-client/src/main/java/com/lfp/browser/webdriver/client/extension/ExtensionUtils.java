package com.lfp.browser.webdriver.client.extension;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.Validate;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import at.favre.lib.bytes.Bytes;

public class ExtensionUtils {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Map<String, Optional<Void>> LOCK_MAP = new ConcurrentHashMap<>();

	@SuppressWarnings("unchecked")
	public static File buildExtension(String manifest, Entry<String, Bytes>... content) throws IOException {
		return buildExtension(manifest, Utils.Lots.stream(content));
	}

	public static File buildExtension(String manifest, Iterable<? extends Entry<String, Bytes>> content)
			throws IOException {
		var estream = Utils.Lots.streamEntries(content).nonNull();
		Map<String, ThrowingSupplier<InputStream, IOException>> contentMap = estream.mapValues(value -> {
			if (value == null)
				return null;
			ThrowingSupplier<InputStream, IOException> isSupplier = () -> value.inputStream();
			return isSupplier;
		}).toCustomMap(LinkedHashMap::new);
		return buildExtension(manifest, contentMap);
	}

	public static File buildExtension(String manifest, Map<String, ThrowingSupplier<InputStream, IOException>> content)
			throws IOException {
		Validate.isTrue(Utils.Strings.isNotBlank(manifest), "manifest is required");
		Map<String, ThrowingSupplier<InputStream, IOException>> zipContents = buildZipContents(manifest, content);
		Bytes idBytes = hashZipContents(zipContents);
		String id = idBytes.encodeHex();
		File zipFile = Utils.Files.tempFile(THIS_CLASS, "v5", String.format("ext_%s.zip", id));
		if (zipFile.exists())
			return zipFile;
		AtomicReference<Throwable> error = new AtomicReference<Throwable>();
		LOCK_MAP.compute(id, (k, v) -> {
			if (!zipFile.exists())
				try {
					createZipFile(zipContents, zipFile);
				} catch (IOException e) {
					error.set(e);
				}
			return null;
		});
		if (error.get() != null)
			throw Utils.Exceptions.as(error.get(), IOException.class);
		return zipFile;
	}

	private static void createZipFile(Map<String, ThrowingSupplier<InputStream, IOException>> zipContents, File zipFile)
			throws IOException {
		FileOutputStream fos = new FileOutputStream(zipFile);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		for (var ent : zipContents.entrySet()) {
			ZipEntry zipEntry = new ZipEntry(ent.getKey());
			zipOut.putNextEntry(zipEntry);
			try (var is = ent.getValue().get()) {
				Utils.Bits.copy(is, zipOut);
			}
		}
		zipOut.close();
	}

	private static Map<String, ThrowingSupplier<InputStream, IOException>> buildZipContents(String manifest,
			Map<String, ThrowingSupplier<InputStream, IOException>> additionalFiles) throws IOException {
		Map<String, ThrowingSupplier<InputStream, IOException>> zipContents = new HashedMap<>();
		zipContents.put("manifest.json", () -> Bytes.from(manifest).inputStream());
		for (var ent : Utils.Lots.stream(additionalFiles).nonNull()) {
			String name = ent.getKey();
			if (Utils.Strings.isBlank(name) || zipContents.containsKey(name))
				continue;
			if (ent.getValue() == null)
				continue;
			zipContents.put(name, ent.getValue());
		}
		return zipContents;
	}

	private static Bytes hashZipContents(Map<String, ThrowingSupplier<InputStream, IOException>> zipContents)
			throws IOException {
		var digest = Utils.Crypto.getMessageDigestMD5();
		for (Entry<String, ThrowingSupplier<InputStream, IOException>> ent : Utils.Lots.stream(zipContents)
				.sortedBy(Entry::getKey)) {
			digest.update(ent.getKey().getBytes(StandardCharsets.UTF_8));
			try (var is = ent.getValue().get()) {
				digest.update(Utils.Crypto.hashMD5(is).array());
			}
		}
		return Utils.Bits.from(digest.digest());
	}
}
