package com.lfp.browser.webdriver.client.command;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Collections;
import java.util.Map;

import org.openqa.selenium.remote.CommandInfo;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.http.HttpClient;

import com.lfp.browser.webdriver.client.config.BrowserClientConfig;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.properties.Configs;

public class HttpCommandExecutorLFP extends HttpCommandExecutor implements Scrapable {

	private final Scrapable _scrapable = Scrapable.create();

	public HttpCommandExecutorLFP(URI addressOfRemoteServer) {
		this(defaultCommands(null), addressOfRemoteServer);
	}

	public HttpCommandExecutorLFP(Map<String, CommandInfo> additionalCommands, URI addressOfRemoteServer) {
		this(defaultCommands(additionalCommands), addressOfRemoteServer, OkHttpHttpClientFactory.INSTANCE);
	}

	public HttpCommandExecutorLFP(Map<String, CommandInfo> additionalCommands, URI addressOfRemoteServer,
			HttpClient.Factory httpClientFactory) {
		super(defaultCommands(additionalCommands), toURL(addressOfRemoteServer), httpClientFactory);
	}

	@Override
	public boolean isScrapped() {
		return _scrapable.isScrapped();
	}

	@Override
	public Scrapable onScrap(Runnable task) {
		return _scrapable.onScrap(task);
	}

	@Override
	public boolean scrap() {
		return _scrapable.scrap();
	}

	private static Map<String, CommandInfo> defaultCommands(Map<String, CommandInfo> additionalCommands) {
		if (additionalCommands != null)
			return additionalCommands;
		return Collections.emptyMap();
	}

	private static URL toURL(URI addressOfRemoteServer) {
		var cfg = Configs.get(BrowserClientConfig.class);
		if (addressOfRemoteServer == null)
			addressOfRemoteServer = URI
					.create(System.getProperty("webdriver.remote.server", String.format("http://%s:%s%s",
							cfg.webDriverDefaultHost(), cfg.webDriverDefaultPort(), cfg.webDriverDefaultPath())));
		try {
			return addressOfRemoteServer.toURL();
		} catch (MalformedURLException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

}
