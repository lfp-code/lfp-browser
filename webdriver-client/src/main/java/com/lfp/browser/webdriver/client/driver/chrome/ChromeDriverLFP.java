package com.lfp.browser.webdriver.client.driver.chrome;

import java.util.Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.lfp.browser.webdriver.client.driver.AbstractWebDriver;
import com.lfp.browser.webdriver.client.driver.WebDriverLFP;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Utils;

import io.github.bonigarcia.wdm.WebDriverManager;

public interface ChromeDriverLFP extends WebDriverLFP {

	@Override
	ChromeOptionsLFP getCapabilities();

	public abstract static class Abs extends AbstractWebDriver implements ChromeDriverLFP {

		private final ChromeOptionsLFP chromeOptions;

		public Abs(ChromeOptionsLFP chromeOptions) {
			super();
			this.chromeOptions = chromeOptions != null ? chromeOptions : new ChromeOptionsLFP();
		}

		@Override
		public ChromeOptionsLFP getCapabilities() {
			return chromeOptions;
		}

	}

	public static class Remote extends Abs {

		private HttpCommandExecutor httpCommandExecutor;

		public Remote(ChromeOptionsLFP chromeOptions, HttpCommandExecutor httpCommandExecutor) {
			super(chromeOptions);
			this.httpCommandExecutor = Objects.requireNonNull(httpCommandExecutor);
		}

		@Override
		protected WebDriver createDelegate() {
			var webDriver = new RemoteWebDriver(httpCommandExecutor, getCapabilities());
			webDriver.setFileDetector(new LocalFileDetector());
			this.getCapabilities().getWindowSize().ifPresent(webDriver.manage().window()::setSize);
			return webDriver;
		}

		@Override
		public void quit() {
			try {
				super.quit();
			} finally {
				if (httpCommandExecutor instanceof Scrapable)
					((Scrapable) httpCommandExecutor).scrap();
			}
		}
	}

	public static class Local extends Abs {

		private static final MemoizedSupplier<Void> CHROME_DRIVER_SETUP = Utils.Functions
				.memoize(() -> WebDriverManager.chromedriver().setup());

		public Local() {
			this(null);
		}

		public Local(ChromeOptionsLFP chromeOptions) {
			super(chromeOptions);
		}

		@Override
		protected WebDriver createDelegate() {
			CHROME_DRIVER_SETUP.get();
			var webDriver = new ChromeDriver(this.getCapabilities());
			this.getCapabilities().getWindowSize().ifPresent(webDriver.manage().window()::setSize);
			return webDriver;
		}

	}

}
