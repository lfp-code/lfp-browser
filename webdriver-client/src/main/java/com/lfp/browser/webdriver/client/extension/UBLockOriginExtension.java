package com.lfp.browser.webdriver.client.extension;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;

import org.zeroturnaround.zip.ZipUtil;

import com.google.gson.JsonElement;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

public class UBLockOriginExtension extends ExtensionSupplier.URISource {

	public static UBLockOriginExtension getInstance() {
		return Instances.get(UBLockOriginExtension.class, UBLockOriginExtension::new);
	}

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String API_URL = "https://api.github.com/repos/gorhill/uBlock/releases/latest";

	protected UBLockOriginExtension() {
		super(URI.create(API_URL));
	}

	@Override
	protected void writeToFile(FileExt uriFile, File outFile) throws IOException {
		URI releaseURI;
		{
			JsonElement releases;
			try (var fis = new FileInputStream(uriFile)) {
				releases = Serials.Gsons.fromStream(fis, JsonElement.class);
			}
			var jarr = Serials.Gsons.tryGetAsJsonArray(releases, "assets").get();
			var uriStream = Streams.of(jarr).map(v -> {
				var name = Serials.Gsons.tryGetAsString(v, "name").orElse(null);
				if (!Utils.Strings.startsWithIgnoreCase(name, "ublock"))
					return null;
				if (!Utils.Strings.endsWithIgnoreCase(name, "chromium.zip"))
					return null;
				return Serials.Gsons.tryGetAsString(v, "browser_download_url").flatMap(URIs::parse).orElse(null);
			});
			uriStream = uriStream.nonNull();
			releaseURI = uriStream.findFirst().get();
		}
		var buildDir = new FileExt(outFile.getParentFile(), "build");
		buildDir.mkdirs();
		try {
			var releaseZip = new File(buildDir, "release.zip");
			writeURIToFile(releaseURI, releaseZip);
			var release = new File(buildDir, "release");
			ZipUtil.unpack(releaseZip, release);
			var manifest = findManifest(release);
			ZipUtil.pack(manifest.getParentFile(), outFile);
		} finally {
			buildDir.deleteAll();
		}
	}

	@SuppressWarnings("resource")
	private static File findManifest(File file) {
		if (file == null)
			return null;
		if (file.isFile() && file.getName().equals("manifest.json"))
			return file;
		for (var subFile : Streams.of(file.listFiles()).sortedBy(v -> v.isFile() ? 0 : 1)) {
			var result = findManifest(subFile);
			if (result != null)
				return result;
		}
		return null;
	}

	public static void main(String[] args) throws IOException {
		System.out.println(UBLockOriginExtension.getInstance().get().getAbsoluteFile());
	}
}
