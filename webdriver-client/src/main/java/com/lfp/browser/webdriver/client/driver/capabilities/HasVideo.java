package com.lfp.browser.webdriver.client.driver.capabilities;

public interface HasVideo {

	boolean isVideoEnabled();

	void setVideoEnabled(boolean enabled);

}
