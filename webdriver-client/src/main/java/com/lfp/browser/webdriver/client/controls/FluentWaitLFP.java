package com.lfp.browser.webdriver.client.controls;

import java.time.Clock;
import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Sleeper;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;

public class FluentWaitLFP<T> extends FluentWait<T> {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static FieldAccessor<FluentWait, Duration> FluentWait_timeout_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(FluentWait.class, true, v -> Duration.class.isAssignableFrom(v.getType()),
					v -> v.getName().equals("timeout"));

	public FluentWaitLFP(T input) {
		super(input);
	}

	public FluentWaitLFP(T input, Clock clock, Sleeper sleeper) {
		super(input, clock, sleeper);
	}

	public Duration getTimeout() {
		return FluentWait_timeout_FA.get(this);
	}

	@Override
	public FluentWaitLFP<T> withTimeout(Duration timeout) {
		if (timeout == null)
			timeout = Duration.ZERO;
		return (FluentWaitLFP<T>) super.withTimeout(timeout);
	}

	// delegates

	@Override
	public FluentWaitLFP<T> withMessage(String message) {
		return (FluentWaitLFP<T>) super.withMessage(message);
	}

	@Override
	public FluentWaitLFP<T> withMessage(Supplier<String> messageSupplier) {
		return (FluentWaitLFP<T>) super.withMessage(messageSupplier);
	}

	@Override
	public FluentWaitLFP<T> pollingEvery(Duration interval) {
		return (FluentWaitLFP<T>) super.pollingEvery(interval);
	}

	@Override
	public <K extends Throwable> FluentWaitLFP<T> ignoreAll(Collection<Class<? extends K>> types) {
		return (FluentWaitLFP<T>) super.ignoreAll(types);
	}

	@Override
	public FluentWaitLFP<T> ignoring(Class<? extends Throwable> exceptionType) {
		return (FluentWaitLFP<T>) super.ignoring(exceptionType);
	}

	@Override
	public FluentWaitLFP<T> ignoring(Class<? extends Throwable> firstType, Class<? extends Throwable> secondType) {
		return (FluentWaitLFP<T>) super.ignoring(firstType, secondType);
	}

}
