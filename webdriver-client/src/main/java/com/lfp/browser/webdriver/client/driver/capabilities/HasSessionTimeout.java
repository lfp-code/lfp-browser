package com.lfp.browser.webdriver.client.driver.capabilities;

import java.time.Duration;
import java.util.Optional;

public interface HasSessionTimeout {

	Optional<Duration> getSessionTimeout();

	void setSessionTimeout(Duration timeout);

}
