package com.lfp.browser.webdriver.client.driver.chrome;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.logging.Level;

import org.apache.commons.lang3.Validate;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;

import com.lfp.browser.webdriver.client.driver.WebDriverReflections;
import com.lfp.browser.webdriver.client.driver.capabilities.HasProxy;
import com.lfp.browser.webdriver.client.driver.capabilities.HasWindowSize;
import com.lfp.browser.webdriver.client.extension.DetectionPreventionExtension;
import com.lfp.browser.webdriver.client.extension.DisableContentSecurityPolicyExtension;
import com.lfp.browser.webdriver.client.extension.ExtensionSupplier;
import com.lfp.browser.webdriver.client.extension.ModHeaderExtension;
import com.lfp.browser.webdriver.client.extension.ProxyExtension;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;

import eu.mihosoft.vcollections.VList;
import eu.mihosoft.vcollections.VListChange;
import vjavax.observer.collection.CollectionChangeEvent;
import vjavax.observer.collection.CollectionChangeListener;

@SuppressWarnings({ "serial", "unchecked" })
public class ChromeOptionsLFP extends ChromeOptions implements HasProxy, HasWindowSize {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private static final String DEFAULT_START_PAGE = "data:text/html,<body></body>";
	private static final String WINDOW_SIZE_FLAG = "--window-size";
	private static final boolean BROWSERLESS_SUGGESTED_ARGUMENTS = false;
	private static final int BROWSER_WIDTH_DEFAULT = 1440;
	private static final int BROWSER_HEIGHT_DEFAULT = 900;
	protected final Date createdAt;
	private final Proxy proxy;
	private final LoggingPreferences loggingPreferences;
	private Dimension windowSize;

	public ChromeOptionsLFP() {
		this(null);
	}

	public ChromeOptionsLFP(Proxy proxy) {
		super(); // DO NOT FORGET THIS
		this.proxy = proxy;
		configureStartPageListener();
		this.setW3cEnabled(false);
		this.setPageLoadStrategy(PageLoadStrategy.EAGER);
		this.getPreferences().put("credentials_enable_service", false);
		this.getPreferences().put("profile.password_manager_enabled", false);
		this.setExperimentalOption("excludeSwitches", new String[] { "enable-automation" });
		this.addArguments("--disable-blink-features=AutomationControlled");
		this.addArguments("--disable-features=UserAgentClientHint");
		this.addArguments("--disable-web-security");
		this.addArguments("--ignore-certificate-errors");
		this.addArguments("--no-sandbox");
		// this.addArguments("--disable-notifications");
		if (BROWSERLESS_SUGGESTED_ARGUMENTS) {
			this.addArguments("--disable-background-timer-throttling");
			this.addArguments("--disable-backgrounding-occluded-windows");
			this.addArguments("--disable-breakpad");
			this.addArguments("--disable-dev-shm-usage");
			this.addArguments("--disable-features=TranslateUI,BlinkGenPropertyTrees");
			this.addArguments("--disable-ipc-flooding-protection");
			this.addArguments("--disable-renderer-backgrounding");
			this.addArguments("--enable-features=NetworkService,NetworkServiceInProcess");
			this.addArguments("--force-color-profile=srgb");
			this.addArguments("--hide-scrollbars");
			this.addArguments("--metrics-recording-only");
		}
		this.loggingPreferences = new LoggingPreferences();
		ProxyExtension proxyExtension;
		if (proxy == null)
			proxyExtension = null;
		else if (!proxy.isAuthenticationEnabled()) {
			proxyExtension = null;
			this.setArgumentFlag("--proxy-server",
					String.format("%s://%s:%s", proxy.getType().asScheme(), proxy.getHostname(), proxy.getPort()));
		} else
			proxyExtension = new ProxyExtension(proxy);
		this.addExtensionSupplier(DetectionPreventionExtension.getInstance());
		this.addExtensionSupplier(DisableContentSecurityPolicyExtension.getInstance());
		this.addExtensionSupplier(ModHeaderExtension.getInstance());
		this.addExtensionSupplier(proxyExtension);
		this.setWindowSize(new Dimension(BROWSER_WIDTH_DEFAULT, BROWSER_HEIGHT_DEFAULT));
		this.createdAt = new Date();
	}

	public void setUserAgent(String userAgent) {
		String flag = "--user-agent";
		if (Utils.Strings.isBlank(userAgent))
			this.removeArgumentFlag(flag);
		else
			this.setArgumentFlag(flag, userAgent);
	}

	public void setUserDataDir(File dir) {
		String flag = "--user-data-dir";
		if (dir == null)
			this.removeArgumentFlag(flag);
		else
			this.setArgumentFlag(flag, dir.getAbsolutePath());
	}

	@Override
	public void setWindowSize(Dimension dimension) {
		this.windowSize = dimension;
		if (this.windowSize == null)
			this.removeArgumentFlag(WINDOW_SIZE_FLAG);
		else
			this.setArgumentFlag(WINDOW_SIZE_FLAG,
					String.format("%s,%s", this.windowSize.getWidth(), this.windowSize.getHeight()));
	}

	@Override
	public Optional<Dimension> getWindowSize() {
		return Optional.ofNullable(windowSize);
	}

	@Override
	public Optional<Proxy> getProxy() {
		return Optional.ofNullable(this.proxy);
	}

	public void setArgumentFlag(String flag, String value) {
		removeArgumentFlag(flag);
		flag = normalizeArgumentFlag(flag);
		Validate.isTrue(Utils.Strings.isNotBlank(flag), "flag must not be blank");
		String newArgument = String.format("%s%s", flag, Utils.Strings.isBlank(value) ? "" : "=" + value);
		this.addArguments(newArgument);
	}

	public boolean removeArgumentFlag(String flag) {
		flag = normalizeArgumentFlag(flag);
		if (flag.isEmpty())
			return false;
		List<String> remove = new ArrayList<String>();
		for (var arg : this.getMutableArguments()) {
			if (Utils.Strings.equals(arg, flag))
				remove.add(arg);
			else if (Utils.Strings.startsWith(arg, flag + "="))
				remove.add(arg);
		}
		return this.getMutableArguments().removeAll(remove);
	}

	public void setLogTypeLevel(String logType, Level logLevel) {
		Validate.notBlank(logType);
		modifyLoggingPreferences(lp -> {
			if (logLevel == null)
				lp.enable(logType, Level.OFF);
			else
				lp.enable(logType, logLevel);
		});

	}

	public void modifyLoggingPreferences(Consumer<LoggingPreferences> loggingPreferencesModifier) {
		if (loggingPreferencesModifier == null)
			return;
		loggingPreferencesModifier.accept(this.loggingPreferences);
		this.setCapability(CapabilityType.LOGGING_PREFS, this.loggingPreferences);
	}

	@SuppressWarnings({ "deprecation" })
	public Map<String, Object> getPreferences() {
		var key = "prefs";
		Map<String, Object> prefs = Utils.Types.tryCast(this.getExperimentalOption(key), Map.class).orElse(null);
		if (prefs == null)
			synchronized (this) {
				prefs = Utils.Types.tryCast(this.getExperimentalOption(key), Map.class).orElse(null);
				if (prefs == null) {
					prefs = new ConcurrentHashMap<>();
					this.setExperimentalOption(key, prefs);
				}
			}
		return prefs;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getProfileDefaultContentSettingValues() {
		return (Map<String, Object>) this.getPreferences().computeIfAbsent("profile.default_content_setting_values",
				nil -> new ConcurrentHashMap<>());
	}

	public void setImagesEnabled(boolean enabled) {
		var key = "images";
		if (enabled)
			this.getProfileDefaultContentSettingValues().remove(key);
		else
			this.getProfileDefaultContentSettingValues().put(key, 2);
	}

	public void setW3cEnabled(boolean enabled) {
		this.setExperimentalOption("w3c", enabled);
	}

	public boolean removeExtensionSupplier(ExtensionSupplier extensionSupplier) {
		if (extensionSupplier == null)
			return false;
		var base64 = Utils.Functions.unchecked(extensionSupplier::getBase64);
		var list = Utils.Lots.stream(this.getMutableExtensions()).nonNull().distinct().toList();
		var result = list.remove(base64);
		this.setMutableExtensions(Utils.Lots.unmodifiable(list));
		return result;
	}

	public boolean addExtensionSupplier(ExtensionSupplier extensionSupplier) {
		if (extensionSupplier == null)
			return false;
		var base64 = Utils.Functions.unchecked(extensionSupplier::getBase64);
		var list = Utils.Lots.stream(this.getMutableExtensions()).nonNull().distinct().toList();
		var result = list.add(base64);
		this.setMutableExtensions(Utils.Lots.unmodifiable(list));
		return result;
	}

	protected void setMutableArguments(List<String> list) {
		WebDriverReflections.setArgs(this, list);
	}

	protected List<String> getMutableArguments() {
		return WebDriverReflections.getArgs(this);
	}

	protected void setMutableExtensions(List<String> list) {
		WebDriverReflections.setExtensions(this, list);
	}

	protected List<String> getMutableExtensions() {
		return WebDriverReflections.getExtensions(this);
	}

	protected void setMutableExtensionFiles(List<File> list) {
		WebDriverReflections.setExtensionFiles(this, list);
	}

	protected List<File> getMutableExtensionFiles() {
		return WebDriverReflections.getExtensionFiles(this);
	}

	private void configureStartPageListener() {
		var argumentList = VList.newInstance(this.getMutableArguments());
		this.setMutableArguments(argumentList);
		CollectionChangeListener<String, ? super VList<String>, ? super VListChange<String>> changeListener = new CollectionChangeListener<String, VList<String>, VListChange<String>>() {
			private boolean running = false;

			@Override
			public void onChange(CollectionChangeEvent<String, VList<String>, VListChange<String>> evt) {
				if (running)
					return;
				running = true;
				try {
					ensureStartPagePresent();
				} finally {
					running = false;
				}
			}
		};
		argumentList.addChangeListener(changeListener);
		changeListener.onChange(null);
	}

	private void ensureStartPagePresent() {
		List<String> startPages = new ArrayList<>();
		for (String arg : this.getMutableArguments()) {
			if (!Utils.Strings.startsWithIgnoreCase(arg, "-"))
				startPages.add(arg);
		}
		this.getMutableArguments().removeAll(startPages);
		startPages.remove(DEFAULT_START_PAGE);
		if (startPages.isEmpty())
			startPages.add(DEFAULT_START_PAGE);
		this.getMutableArguments().addAll(startPages);
		if (startPages.size() > 1)
			logger.warn("multiple start pages defined in arguments:"
					+ Utils.Lots.stream(this.getMutableArguments()).joining(", "));
	}

	private static String normalizeArgumentFlag(String flag) {
		if (flag == null)
			return "";
		boolean mod = true;
		while (mod) {
			mod = false;
			flag = Utils.Strings.trim(flag);
			if (Utils.Strings.startsWith(flag, "-")) {
				flag = flag.substring(1);
				mod = true;
			}
		}
		if (Utils.Strings.isBlank(flag))
			return "";
		return "--" + flag;
	}

}
