package com.lfp.browser.webdriver.client.extension;

import java.net.URI;

import com.lfp.joe.core.classpath.Instances;

public class ModHeaderExtension extends ExtensionSupplier.URISource {

	public static ModHeaderExtension getInstance() {
		return Instances.get(ModHeaderExtension.class, ModHeaderExtension::new);
	}

	protected ModHeaderExtension() {
		super(URI.create("https://github.com/modheader/modheader_selenium/raw/main/chrome-modheader/modheader.crx"));
	}


}
