package com.lfp.browser.webdriver.client.command;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.remote.http.HttpClient;
import org.openqa.selenium.remote.http.HttpClient.Builder;

import com.google.common.base.Strings;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import okhttp3.ConnectionPool;
import okhttp3.Credentials;
import okhttp3.JavaNetCookieJar;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public enum OkHttpHttpClientFactory implements HttpClient.Factory {
	INSTANCE;

	private final ConnectionPool pool = new ConnectionPool(5 * Utils.Machine.logicalProcessorCount(),
			Duration.ofMinutes(5).toMillis(), TimeUnit.MILLISECONDS);

	@Override
	public Builder builder() {
		return new Builder() {
			@Override
			public HttpClient createClient(URL url) {
				return OkHttpHttpClientFactory.this.createClient(url, proxy, readTimeout, connectionTimeout, null);
			}
		};
	}

	@Override
	public void cleanupIdleClients() {
		pool.evictAll();
	}

	public HttpClient.Factory withClientModifier(Function<OkHttpClient.Builder, OkHttpClient.Builder> clientModifier) {
		return new HttpClient.Factory() {

			@Override
			public Builder builder() {
				return new Builder() {
					@Override
					public HttpClient createClient(URL url) {
						return OkHttpHttpClientFactory.this.createClient(url, proxy, readTimeout, connectionTimeout,
								clientModifier);
					}
				};
			}

			@Override
			public void cleanupIdleClients() {
				pool.evictAll();
			}
		};
	}

	protected HttpClient createClient(URL url, Proxy proxy, Duration readTimeout, Duration connectionTimeout,
			Function<OkHttpClient.Builder, OkHttpClient.Builder> clientModifier) {
		var cookieJar = CookieJars.create();
		okhttp3.OkHttpClient.Builder client = Ok.Clients.builderDefault().cookieJar(cookieJar).connectionPool(pool)
				.followRedirects(true).followSslRedirects(true).proxy(proxy)
				.readTimeout(readTimeout.toMillis(), MILLISECONDS)
				.connectTimeout(connectionTimeout.toMillis(), MILLISECONDS);
		String info = url.getUserInfo();
		if (!Strings.isNullOrEmpty(info)) {
			String[] parts = info.split(":", 2);
			String user = parts[0];
			String pass = parts.length > 1 ? parts[1] : null;
			String credentials = Credentials.basic(user, pass);
			client.authenticator((route, response) -> {
				if (response.request().header("Authorization") != null) {
					return null; // Give up, we've already attempted to authenticate.
				}
				return response.request().newBuilder().header(Headers.AUTHORIZATION, credentials).build();
			});
		}
		client.addNetworkInterceptor(chain -> {
			var response = chain.proceed(chain.request());
			if (response.isSuccessful())
				return response;
			var peekBody = response.peekBody(1);
			var barr = peekBody.bytes();
			if (barr.length > 0)
				return response;
			response.close();
			var newBody = Ok.Calls.createResponseBody(MediaType.parse("application/json"),
					Bytes.from("{}".getBytes(StandardCharsets.UTF_8)));
			return response.newBuilder().body(newBody).build();
		});
		client.addNetworkInterceptor(chain -> {
			Request request = chain.request();
			Response response = chain.proceed(request);
			return response.code() == 408 ? response.newBuilder().code(500).message("Server-Side Timeout").build()
					: response;
		});
		if (clientModifier != null) {
			var newClient = clientModifier.apply(client);
			if (newClient != null)
				client = newClient;
		}
		return new org.openqa.selenium.remote.internal.OkHttpClient(client.build(), url);
	}

}
