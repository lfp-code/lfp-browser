package com.lfp.browser.webdriver.client.driver.capabilities;

import org.openqa.selenium.WebDriver;

public interface HasDelegate {

	default WebDriver getDelegate() {
		return getDelegate(true);
	}

	WebDriver getDelegate(boolean create);
}
