package com.lfp.browser.webdriver.client.driver.capabilities;

import org.openqa.selenium.remote.SessionId;

public interface HasSessionId {

	SessionId getSessionId();
}
