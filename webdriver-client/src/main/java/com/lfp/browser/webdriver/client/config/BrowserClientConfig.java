package com.lfp.browser.webdriver.client.config;

import java.net.URI;
import java.time.Duration;
import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.properties.converter.DurationConverter;

public interface BrowserClientConfig extends Config {

	@ConverterClass(DurationConverter.class)
	@DefaultValue("10s")
	Duration discoveryServiceCacheExpireAfterAccess();

	@ConverterClass(DurationConverter.class)
	@DefaultValue("2s")
	Duration discoveryServiceCacheRefreshAfterWrite();

	@DefaultValue("/ping")
	String webDriverPingPath();

	@DefaultValue("localhost")
	String webDriverDefaultHost();

	@DefaultValue("4444")
	int webDriverDefaultPort();

	@DefaultValue("/wd/hub")
	String webDriverDefaultPath();

	@DefaultValue("4445,${webDriverDefaultPort},5555")
	List<Integer> webDriverTestPorts();

	@DefaultValue("${webDriverPingPath},${webDriverHubPath},/")
	List<String> webDriverTestPaths();

	@ConverterClass(URIConverter.class)
	@DefaultValue("https://api.ipify.org/?format=txt")
	URI ipAddressLookupURI();

	@DefaultValue("com/lfp/browser/client/scripts/sendXMLHttpRequest.js")
	String sendXMLHttpRequestScriptPath();

	@DefaultValue("com/lfp/browser/client/scripts/cssInjector.js")
	String cssInjectorScriptPath();

	@DefaultValue("com/lfp/browser/client/scripts/trackAjaxHeaders.js")
	String trackAjaxHeadersScriptPath();

	@DefaultValue("com/lfp/browser/client/scripts/hover.js")
	String hoverScriptPath();

}
