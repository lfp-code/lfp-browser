package com.lfp.browser.webdriver.client.driver;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebElement;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.joe.utils.Utils;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class WebDriverReflections {

	private static final FieldAccessor<ChromeOptions, List<String>> ChromeOptions_args_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(ChromeOptions.class, true, f -> "args".equals(f.getName()),
					f -> List.class.isAssignableFrom(f.getType()));

	public static List<String> getArgs(ChromeOptions chromeOptions) {
		return ChromeOptions_args_FA.get(chromeOptions);
	}

	public static void setArgs(ChromeOptions chromeOptions, Iterable<String> ible) {
		ChromeOptions_args_FA.set(chromeOptions, Utils.Lots.stream(ible).nonNull().toList());
	}

	private static final FieldAccessor<ChromeOptions, List<String>> ChromeOptions_extensions_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(ChromeOptions.class, true, f -> "extensions".equals(f.getName()),
					f -> List.class.isAssignableFrom(f.getType()));

	public static List<String> getExtensions(ChromeOptions chromeOptions) {
		return ChromeOptions_extensions_FA.get(chromeOptions);
	}

	public static void setExtensions(ChromeOptions chromeOptions, List<String> list) {
		ChromeOptions_extensions_FA.set(chromeOptions, list);
	}

	private static final FieldAccessor<ChromeOptions, List<File>> ChromeOptions_extensionFiles_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(ChromeOptions.class, true, f -> "extensionFiles".equals(f.getName()),
					f -> List.class.isAssignableFrom(f.getType()));

	public static List<File> getExtensionFiles(ChromeOptions chromeOptions) {
		return ChromeOptions_extensionFiles_FA.get(chromeOptions);
	}

	public static void setExtensionFiles(ChromeOptions chromeOptions, List<File> list) {
		ChromeOptions_extensionFiles_FA.set(chromeOptions, list);
	}

	private static final FieldAccessor<RemoteWebElement, WebDriver> RemoteWebElement_parent_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(RemoteWebElement.class, true, f -> "parent".equals(f.getName()),
					f -> WebDriver.class.isAssignableFrom(f.getType()));

	public static WebDriver getParent(RemoteWebElement webElement) {
		return RemoteWebElement_parent_FA.get(webElement);
	}

	public static void main(String[] args) {
		var stream = JavaCode.Reflections.streamSubTypesOf(WebElement.class);
		stream.forEach(v -> {
			System.out.println(v.getName());
		});
	}
}
