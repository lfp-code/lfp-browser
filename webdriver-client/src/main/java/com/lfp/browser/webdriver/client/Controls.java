package com.lfp.browser.webdriver.client;

import java.time.Duration;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.threadly.concurrent.SameThreadSubmitterExecutor;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.wrapper.SubmitterExecutorAdapter;

import com.lfp.browser.webdriver.client.controls.FluentWaitLFP;
import com.lfp.browser.webdriver.client.driver.capabilities.HasSessionId;
import com.lfp.joe.net.html.Htmls;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

import one.util.streamex.StreamEx;

public class Controls {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void ping(SearchContext input) {
		var webDriverOp = Browsers.Drivers.tryGet(input);
		var selector = String.format("#%s", Htmls.generateRandomKey("ping"));
		if (webDriverOp.isEmpty())
			input.findElements(By.cssSelector(selector));
		else
			webDriverOp.get().executeScript(String.format("document.querySelectorAll('%s');", selector));
	}

	public static void until(SearchContext input, Duration timeout) {
		untilNotNull(input, fw -> fw.withTimeout(timeout), nil -> {
			ping(input);
			return null;
		});
	}

	public static <X> X untilComplete(SearchContext input, Duration timeout, boolean cancelOnTimeout, Future<X> future)
			throws ExecutionException {
		var result = untilNotNull(input, fw -> fw.withTimeout(timeout), nil -> {
			ping(input);
			if (future.isDone())
				return true;
			return null;
		});
		result = Boolean.TRUE.equals(result);
		if (!result && cancelOnTimeout)
			Threads.Futures.cancel(future, true);
		try {
			return future.get();
		} catch (InterruptedException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <I, X, T extends Throwable> X untilNotNull(I input, Consumer<FluentWaitLFP<I>> fluentWaitConfig,
			ThrowingFunction<? super I, X, T> accessor) throws T {
		var fluentWait = new FluentWaitLFP<I>(input);
		if (fluentWaitConfig != null)
			fluentWaitConfig.accept(fluentWait);
		var timeout = fluentWait.getTimeout();
		var errorRef = new AtomicReference<Throwable>();
		Function<? super I, X> accessorFunction = accessor == null ? i -> null : accessor.onError(errorRef::set);
		X result;
		if (timeout == null || timeout.toMillis() <= 0)
			result = accessorFunction.apply(input);
		else {
			try {
				result = fluentWait.until(accessorFunction::apply);
			} catch (org.openqa.selenium.TimeoutException e) {
				result = null;
			}
		}
		if (errorRef.get() != null)
			throw (T) errorRef.get();
		return result;
	}

	public static StreamEx<WebElement> select(SearchContext input, String... selects) {
		Objects.requireNonNull(input);
		var selectStream = Utils.Lots.stream(selects);
		selectStream = selectStream.filter(Utils.Strings::isNotBlank);
		selectStream = selectStream.distinct();
		var streams = selectStream.map(v -> {
			var elements = input.findElements(By.cssSelector(v));
			return elements.stream();
		});
		return Utils.Lots.flatMap(streams);
	}

	public static StreamEx<WebElement> select(SearchContext input,
			Consumer<FluentWaitLFP<SearchContext>> fluentWaitConfig, Predicate<WebElement> filter, String... selects) {
		Iterator<WebElement> elementIter = untilNotNull(input, fluentWaitConfig, nil -> {
			var elementStream = select(input, selects);
			if (filter != null)
				elementStream = elementStream.filter(filter);
			var iter = elementStream.iterator();
			if (iter.hasNext())
				return iter;
			return null;
		});
		if (elementIter == null)
			return StreamEx.empty();
		return Utils.Lots.stream(elementIter);
	}

	public static WebElement parent(SearchContext input) {
		return input.findElement(By.xpath("./.."));
	}

	public static boolean quitQuietly(WebDriver webDriver) {
		return Threads.Futures.join(quitQuietly(webDriver, null));
	}

	public static ListenableFuture<Boolean> quitQuietly(WebDriver webDriver, Executor executor) {
		if (webDriver == null)
			return FutureUtils.immediateResultFuture(false);
		SubmitterExecutor submitterExecutor;
		if (executor == null)
			submitterExecutor = SameThreadSubmitterExecutor.instance();
		else
			submitterExecutor = SubmitterExecutorAdapter.adaptExecutor(executor);
		return submitterExecutor.submit(() -> {
			try {
				webDriver.quit();
				return true;
			} catch (Exception e) {
				logger.trace("error during webdriver quit", e);
				return false;
			}
		});
	}

	public static Optional<SessionId> tryGetSessiondId(WebDriver webDriver) {
		if (webDriver == null)
			return Optional.empty();
		SessionId sessionId = null;
		if (sessionId == null && webDriver instanceof HasSessionId)
			sessionId = ((HasSessionId) webDriver).getSessionId();
		if (sessionId == null && webDriver instanceof RemoteWebDriver)
			sessionId = ((RemoteWebDriver) webDriver).getSessionId();
		return Optional.ofNullable(sessionId);
	}

	public static Predicate<WebElement> isDisplayed() {
		return we -> {
			if (we == null)
				return false;
			var isDisplayed = Utils.Functions.catching(() -> we.isDisplayed(), t -> false);
			return isDisplayed;
		};

	}

	public static Predicate<WebElement> isNotStale() {
		return we -> {
			boolean isStale;
			if (we == null)
				isStale = true;
			else {
				var webDriver = Browsers.Drivers.tryGet(we).get();
				isStale = Utils.Functions.catching(() -> ExpectedConditions.stalenessOf(we).apply(webDriver),
						t -> true);
			}
			return !isStale;
		};

	}

}
