package chrome;

import java.util.concurrent.Semaphore;

public class SemaTest {

	public static void main(String[] args) {
		Semaphore sema = new Semaphore(10);
		sema.release(-1);
		System.out.println(sema.availablePermits());
	}

}
