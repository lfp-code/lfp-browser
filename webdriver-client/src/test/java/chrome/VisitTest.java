package chrome;

import java.net.MalformedURLException;
import java.net.URI;

import org.openqa.selenium.Dimension;

import com.lfp.browser.webdriver.client.Browsers;
import com.lfp.joe.net.proxy.Proxy;

public class VisitTest {

	public static void main(String[] args) throws MalformedURLException {
		Proxy proxy = Proxy.builder(URI.create("http://localhost:8888")).build();
		proxy = null;
		var cops = Browsers.Drivers.chromeOptions(proxy);
		cops.setWindowSize(new Dimension(1366, 768 - 40));
		try (var chrome = Browsers.Drivers.chrome(cops)) {
			chrome.get("https://bot.sannysoft.com/");
			var ss = chrome.screenshot();
			System.out.println(ss.getAbsolutePath());
		}
	}

}
