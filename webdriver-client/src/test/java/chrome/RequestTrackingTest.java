package chrome;

import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;

import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;

import com.google.common.reflect.TypeToken;
import com.lfp.browser.webdriver.client.Browsers;
import com.lfp.browser.webdriver.client.driver.chrome.ChromeOptionsLFP;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public class RequestTrackingTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final TypeToken<Map<String, String>> TYPE_TOKEN = new TypeToken<Map<String, String>>() {
	};

	public static void main(String[] args) {
		var cops = new ChromeOptionsLFP();
		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
		// logPrefs.enable(LogType.BROWSER, Level.ALL);
		// logPrefs.enable(LogType.CLIENT, Level.ALL);
		cops.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		cops.setExperimentalOption("w3c", false);
		var driver = Browsers.Drivers.chromeLocal(cops);
		// driver.get("http://api.ipify.org");
		// driver.get("http://example.com");
		driver.get("https://twitter.com/search?q=shakira&src=typed_query&f=live");
		for (int i = 0; i < 3; i++)
			driver.scrollToBottom();
		LogEntries logEntries = driver.manage().logs().get(LogType.PERFORMANCE);
		var headerStream = Utils.Lots.stream(logEntries).map(v -> parseRequestHeaders(v));
		headerStream = headerStream.nonNull().filter(v -> v.size() > 0);
		headerStream = headerStream.filter(v -> {
			if (getHeaderValue(v, "x-csrf-token").isEmpty())
				return false;
			if (getHeaderValue(v, "x-guest-token").isEmpty())
				return false;
			return true;
		});
		var result = headerStream.reduce((f, s) -> s);
		System.out.println(Serials.Gsons.getPretty().toJson(result));
		driver.streamCookies().forEach(v -> {
			System.out.println(v);
		});
	}

	private static Optional<String> getHeaderValue(Map<String, String> headers, String name) {
		if (Utils.Strings.isBlank(name))
			return Optional.empty();
		var stream = Utils.Lots.stream(headers).filterKeys(v -> name.equalsIgnoreCase(v)).values();
		stream = stream.filter(Utils.Strings::isNotBlank);
		return stream.findFirst();
	}

	private static Map<String, String> parseRequestHeaders(LogEntry logEntry) {
		var je = Utils.Functions.catching(() -> Serials.Gsons.getJsonParser().parse(logEntry.getMessage()), t -> {
			logger.warn("unable to parse json:{}", logEntry.getMessage());
			return null;
		});
		if (je == null)
			return null;
		var method = Serials.Gsons.tryGetAsString(je, "message", "method").orElse(null);
		if (!Utils.Strings.startsWith(method, "Network.requestWillBeSent"))
			return null;
		System.out.println(je);
		var headers = Serials.Gsons.tryGetAsJsonObject(je, "message", "params", "request", "headers").orElse(null);
		if (headers == null)
			headers = Serials.Gsons.tryGetAsJsonObject(je, "message", "params", "headers").orElse(null);
		if (headers == null || headers.size() == 0)
			return null;
		return Serials.Gsons.get().fromJson(headers, TYPE_TOKEN.getType());
	}

}
