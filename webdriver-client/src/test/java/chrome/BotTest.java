package chrome;

import com.lfp.browser.webdriver.client.Browsers;

public class BotTest {

	public static void main(String[] args) {
		String url = "https://bot.sannysoft.com/";
		try (var wd = Browsers.Drivers.chromeLocal()) {
			wd.get(url);
			System.out.println(wd.getTitle());
		}
	}

}
