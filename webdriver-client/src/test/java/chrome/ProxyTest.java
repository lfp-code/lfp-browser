package chrome;

import com.lfp.browser.webdriver.client.Browsers;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;

public class ProxyTest {

	public static void main(String[] args) {
		var proxyJson = "{\"publicIPAddress\":\"68.57.61.90\",\"type\":\"HTTP\",\"hostname\":\"id2ufpu2f6m2w3jh.134.122.115.253.nip.io\",\"port\":30001,\"username\":\"2Wzkq8uKQteyqq8Z7CWa1j4CE8sXJ25ejdbP7na3nfuXiJny8soA7LgtWT2X4pphCWHDE-6nzyKrhnb3osCDXyYxRbpPSLVGoLHAW5FN1iDz9mN2dbUoT7pWQfNg8ftLsvBKckBep6iTU4UzZXKN2fVckKhNL7bQ4vPqGwmaKaUMbmgwtUZqZpYibkvnRojcRm9TedvM7DaAwmB6ATn4C8V6CSRQ6aPoBwQ5zgx2ZHdDFM5VKVuELsqW2VTnjhQB6VM53nbLUbePa68H7XKdtxg4761uphR4LpEuySsPdzQAR49pGfm3KZHiNBKJ9iW6mpXRLZYocVUySNKgomBFAhPKTmAq8qKNQLfNU4QFKxFfyrQBfvGZzRdFWk64PichJfMMdxJmsvdWTK1skudcEo5YiaNSKFiTUuVC-3NsmmHzfrPtJ372Yj-XFs68UfWT3xPe1smVvNsXQdtnc2vWzkgxrzPwSz3qMzeYCsquHszE1Sk8rfR5gBRsm1aXiBecRNEerSypP5AHQ7YsCzJp1f5UdA2NaNMK6rP\",\"password\":\"7aRVauw3dNgtYqStMeNA1jX9Asu6wH2FixPkcw1y8zGXfHukxhbxLd3tz2wonvtMUA3oR1qnthArtt4kPGTpckTrRZWVyRy6PVqGd5psFgV9PydM9b4obYK3oqoCjo4Ek4gqH9v4rgnErRaojxPhftCsusL826YKiLamUmKVjkLDqMUsWThbjG47ArEmGMkYtSMYxs83u86QkDnXV1BgTwLHHNFk6zjRQnn8UrtXzUfhYd6q8Aw3vFdoHwgticSpwzftXBmVRP1TBJLgcXyWaLb7FV1ZHtXaCCD2LiQoisj2nsCWY2uhHbjciRtf8HGKUWu217uuncZBY4SfX7VwNCLpZLzuN9koe63VYyvD1rW1Qwdg5WxmmjeJmJxSAH9eVpc3FavTtXA1XtGqT712BVJTHXjwvKQPdsY2UsGHhi5v4vWYhQ6xVTgXogBneVJBQKUcCxqDqmkf4N9WwqbRKogMZubN1KwM2fspw82Ea7MjqHRQ2N1uewv3qTQ9ZJDuzGqh4Qgt9Lyd-7uYpWqfCDZWkThRxxyU57G\"}\r\n"
				+ "";
		Proxy proxy = Serials.Gsons.get().fromJson(proxyJson, Proxy.class);
		try (var wd = Browsers.Drivers.chromeLocal(proxy)) {
			var ipAddress = wd.getPublicIPAddress();
			System.out.println(ipAddress);
			wd.get("https://ipinfo.io");
			var ss = wd.screenshot();
			System.out.println(ss.getAbsolutePath());
		}

	}
}
