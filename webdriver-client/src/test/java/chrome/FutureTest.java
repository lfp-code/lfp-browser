package chrome;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

import com.lfp.joe.threads.Threads;

public class FutureTest {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		var pool = Threads.Pools.centralPool().limit();
		var fut = pool.submit(() -> {
			System.out.println("starting instance");
			Threads.sleep(Duration.ofSeconds(5));
			return "instance";
		});
		Threads.Futures.callback(fut, (r, t) -> {
			System.out.println("instance done:" + r);
		});
		fut = fut.flatMap(v -> {
			return pool.submit(() -> {
				System.out.println("starting mapping");
				Threads.sleep(Duration.ofSeconds(5));
				return "mapping";
			});
		});
		Threads.Futures.callback(fut, (r, t) -> {
			System.out.println("mapping done:" + r);
		});
		fut.get();
	}

}
