package chrome;

import java.time.Duration;
import java.util.Date;

import com.lfp.joe.utils.Utils;

public class LogTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		Date quitAt = new Date(System.currentTimeMillis() + Duration.ofMinutes(5).toMillis());
		for (long i = 0; new Date().before(quitAt); i++) {
			logger.debug(i + "_" + Utils.Crypto.getSecureRandomString(128));
			if (i % 10_000 == 0)
				System.out.println(i);
		}
		System.err.println("done");
	}
}
