package test;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.BrowserType.LaunchOptions;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;

public class ScreenshotTest {

	public static void main(String[] args) {
		try (Playwright playwright = Playwright.create()) {
			List<BrowserType> browserTypes = Arrays.asList(playwright.chromium(), playwright.firefox());
			for (BrowserType browserType : browserTypes) {
				for (var headless : List.of(false, true)) {
					for (var swiftshader : List.of(false, true)) {
						var launchOptions = new LaunchOptions().setHeadless(headless);
						if (swiftshader)
							launchOptions.setArgs(List.of("--use-gl=swiftshader"));
						try (Browser browser = browserType.launch(launchOptions)) {
							BrowserContext context = browser.newContext();
							Page page = context.newPage();
							page.navigate(
									"https://stackoverflow.com/questions/33770549/viewport-vs-window-vs-document");
							var path = Paths.get(String.format("temp/screenshot-%s-%s-%s-%s.png", browserType.name(),
									headless ? "headless" : "headfull", swiftshader ? "swiftshader" : "defaultshader",
									UUID.randomUUID().toString()));
							page.screenshot(new Page.ScreenshotOptions().setFullPage(true).setPath(path));
							System.out.println(path.toFile().getAbsolutePath());
						}
					}
				}
			}
		}
	}
}
