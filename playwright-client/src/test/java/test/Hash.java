package test;

import com.lfp.browser.hub.service.config.BrowserHubServiceConfig;
import com.lfp.joe.core.properties.Configs;

public class Hash {

	public static void main(String[] args) {
		var hash = Configs.get(BrowserHubServiceConfig.class).hash();
		System.out.println(hash.encodeHex());
	}
}
