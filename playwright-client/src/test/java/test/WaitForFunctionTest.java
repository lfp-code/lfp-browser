package test;

import java.nio.file.Paths;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;

public class WaitForFunctionTest {

	public static void main(String[] args) {
		var options = new Page.WaitForFunctionOptions().setTimeout(Duration.ofSeconds(5).toMillis());
		String jsFunction = "()=>document.querySelectorAll('#dummy').length>0";
		try (Playwright playwright = Playwright.create()) {
			List<BrowserType> browserTypes = Arrays.asList(playwright.chromium(), playwright.webkit(),
					playwright.firefox());
			for (BrowserType browserType : browserTypes) {
				try (Browser browser = browserType.launch()) {
					BrowserContext context = browser.newContext();
					Page page = context.newPage();
					page.navigate("https://example.com/");
					try {
						page.waitForFunction(jsFunction, null, options);
					} catch (Exception e) {
						System.err.println("expected error:" + e.getMessage());
					}
					page.navigate("https://api.ipify.org/");
					System.out.println(page.textContent("body"));
				}
			}
		}
	}

}
