package test;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.lfp.browser.playwright.client.BrowserContextLFP;
import com.lfp.browser.playwright.client.BrowserContexts;
import com.lfp.browser.script.stealth.StealthScript;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.ElementHandle.ClickOptions;
import com.microsoft.playwright.ElementHandle.TypeOptions;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.LoadState;

import one.util.streamex.StreamEx;

public class ConnectTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int WIDTH = 1920;
	private static final int HEIGHT = 1040;
	private static final int MAX_CONCURRENT = true ? 10 : (20 * 4) + (8 * 0);

	public static void main(String[] args) throws IOException, InterruptedException {
		var asinQueue = getASINs();
		var futureTracker = Threads.Futures.createFutureTracker();
		for (int i = 0; i < MAX_CONCURRENT; i++) {
			var future = Threads.Pools.centralPool().submit(() -> {
				runTest(asinQueue);
			});
			Threads.Futures.logFailureError(future, true, "error:{}", i);
			futureTracker.add(future);
		}

		futureTracker.blockAll(false);
		System.exit(0);
	}

	private static void runTest(BlockingQueue<String> asinQueue) {
		try (BrowserContextLFP context = BrowserContexts.connect()) {
			Page page = context.newPage();
			page.addInitScript(StealthScript.INSTANCE.get());
			page.navigate("https://bot.sannysoft.com/");
			page.onLoad(p -> {
				try {
					p.evaluate("document.querySelectorAll('.a-modal-scroller').forEach(node=>node.remove())");
				} catch (Exception e) {
					// suppress
				}
			});
			while (true) {
				var asin = asinQueue.poll();
				if (asin == null)
					break;
				try {
					visitASIN(page, asin);
				} catch (Exception e) {
					e.printStackTrace();
				}
				boolean quit = false;
				if (quit)
					return;
			}
		}

	}

	private static boolean visitASIN(Page page, String asin) {
		ElementHandle searchField = null;
		for (int i = 0; searchField == null && i < 2; i++) {
			URI uri = i > 0 ? null : URIs.parse(page.url()).orElse(null);
			if (!Utils.Strings.startsWith(TLDs.parseDomainName(uri), "amazon."))
				page.navigate("https://amazon.com/");
			else
				page.waitForLoadState(LoadState.LOAD);
			searchField = page.querySelector(".nav-search-field input[name=\"field-keywords\"]");
		}
		System.out.println(page.title());
		searchField.evaluate("node => node.value=''");
		searchField.type(asin, new TypeOptions().setDelay(5));
		searchField.press("Enter");
		page.waitForFunction("asin => window.location.href.indexOf(asin) >= 0", asin);
		page.waitForLoadState(LoadState.DOMCONTENTLOADED);
		StreamEx<ElementHandle> dataAsins = StreamEx.empty();
		dataAsins = dataAsins.append(Utils.Lots.stream(page.querySelectorAll(".s-result-list [data-asin]")));
		dataAsins = dataAsins.append(Utils.Lots.stream(page.querySelectorAll(".a-carousel [data-asin]")));
		dataAsins = dataAsins.filter(v -> {
			var dataAsin = Utils.Functions.catching(() -> v.getAttribute("data-asin"), t -> null);
			return Utils.Strings.equals(asin, dataAsin);
		});
		for (var dataAsin : dataAsins) {
			try {
				var link = dataAsin.querySelector("a.a-link-normal[href]");
				if (link == null)
					continue;
				if (!link.isVisible())
					continue;
				link.click(new ClickOptions());
				return true;
			} catch (Exception e) {
				// suppress
			}
		}
		page.navigate(String.format("https://www.amazon.com/dp/%s", asin));
		return false;
	}

	private static LinkedBlockingQueue<String> getASINs() {
		String blob = "0060586605\r\n" + "0060586621\r\n" + "0060747684\r\n" + "0062435272\r\n" + "0062498533\r\n"
				+ "0062871358\r\n" + "0375870733\r\n" + "0394801717\r\n" + "0674065891\r\n" + "0674416864\r\n"
				+ "0679891153\r\n" + "1406372153\r\n" + "1409265641\r\n" + "1425152104\r\n" + "1466963263\r\n"
				+ "1470827131\r\n" + "185681453X\r\n" + "6555123591\r\n" + "6685738375\r\n" + "8501110817\r\n"
				+ "8542625838\r\n" + "8542627695\r\n" + "8542628632\r\n" + "9026141904\r\n" + "9026141912\r\n"
				+ "904171362X\r\n" + "9048837189\r\n" + "B000026WG1\r\n" + "B000R4LGMA\r\n" + "B002XWV3OA\r\n"
				+ "B005EI2U9M\r\n" + "B005KT0HBS\r\n" + "B0079IJ0ZC\r\n" + "B00ALTYISU\r\n" + "B00G6BE5W6\r\n"
				+ "B00GZKDY6Q\r\n" + "B00OSW5LOG\r\n" + "B00XX66NYA\r\n" + "B00XX66O30\r\n" + "B016MUY7XW\r\n"
				+ "B01DKMSNOE\r\n" + "B01IBYTJZI\r\n" + "B01IXUR9R0\r\n" + "B01M0614T9\r\n" + "B01N4MTFO2\r\n"
				+ "B06XRS9349\r\n" + "B071LL3LNN\r\n" + "B073SF525D\r\n" + "B0744DC5YM\r\n" + "B07489H1C2\r\n"
				+ "B0748TT9D2\r\n" + "B077N7PGBQ\r\n" + "B077SB9MT5\r\n" + "B07F6GHCSR\r\n" + "B07GTRRRXP\r\n"
				+ "B07KX46CLX\r\n" + "B07MDLK7F8\r\n" + "B07MKSCGB3\r\n" + "B07MNGT9CK\r\n" + "B07MP1NZMB\r\n"
				+ "B07MP1Q616\r\n" + "B07NS8DMPM\r\n" + "B07PHKQHGW\r\n" + "B07PK1FFSL\r\n" + "B07QFZFXXX\r\n"
				+ "B07QS7FGL8\r\n" + "B07R8KTBPZ\r\n" + "B07R8QX2J4\r\n" + "B07R9TTGCV\r\n" + "B07T7TK3L1\r\n"
				+ "B07V2HKK7C\r\n" + "B07V3MJWHL\r\n" + "B07V5PZ61B\r\n" + "B07WCQVQY9\r\n" + "B07WCQWB79\r\n"
				+ "B07WFQCHS1\r\n" + "B07WHF3KSQ\r\n" + "B07WSPH5B5\r\n" + "B07X34W4GG\r\n" + "B07ZMWXNG7\r\n"
				+ "B07ZMXC7W6\r\n" + "B08293S8V3\r\n" + "B083G67NWK\r\n" + "B083GB7996\r\n" + "B084247Q8Z\r\n"
				+ "B0849TKGHN\r\n" + "B086553KTN\r\n" + "B088GW14KB\r\n" + "B08B5PDGB6\r\n" + "B08CWBFFM4\r\n"
				+ "B08G4H3XR2\r\n" + "B08G4H4WVP\r\n" + "B08G4JRDSZ\r\n" + "B08G4S4BW7\r\n" + "B08G4XFDN3\r\n"
				+ "B08GKQMC72\r\n" + "B08GY9K6J8\r\n" + "B08HR1MLWT\r\n" + "B08HYXJGZK\r\n" + "B08K38RQ3N\r\n"
				+ "B08KTFBHZ3\r\n" + "B08LTJSVRC\r\n" + "B08MJCRB23\r\n" + "B08QRHW837\r\n" + "B08QRK1M29\r\n"
				+ "B08QRNK8YH\r\n" + "B08QRWZ1QK\r\n" + "B08QS6PZ51\r\n" + "B08R6VM95T\r\n" + "B08TWWYWS1\r\n"
				+ "B08V1HCVSF\r\n" + "B08WRR96P9\r\n" + "B08X218Y1D\r\n" + "B08X6XF94X\r\n" + "B08X6YHLW9\r\n"
				+ "B08X73YT48\r\n" + "B08XNF56P8\r\n" + "B08XRZD1BD\r\n" + "B08YNNHLF1\r\n" + "B08YNRM3HW\r\n"
				+ "B091G8DKR1\r\n" + "B091HVLZPG\r\n" + "B091J5NGQ2";
		var list = Utils.Strings.streamLines(blob).filter(Utils.Strings::isNotBlank).map(v -> v.toUpperCase())
				.distinct().toList();
		Collections.shuffle(list);
		return new LinkedBlockingQueue<String>(list);
	}

}
