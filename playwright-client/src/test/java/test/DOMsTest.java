package test;

import java.time.Duration;

import com.lfp.browser.playwright.client.BrowserContexts;
import com.lfp.browser.playwright.client.DOMs;
import com.lfp.joe.utils.Utils;

public class DOMsTest {

	public static void main(String[] args) {
		try (var bc = BrowserContexts.launch()) {
			var page = bc.getPage();
			page.navigate("https://stackoverflow.com/questions/4110081/difference-between-html-and-dom");
			DOMs.select(page, (Duration) null, "img", ".x" + Utils.Crypto.getRandomString()).forEach(v -> {
				System.out.println(v.getAttribute("src"));
			});
		}
	}
}
