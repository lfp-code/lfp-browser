package test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Duration;
import java.util.Optional;

import com.lfp.browser.playwright.client.BrowserContexts;
import com.lfp.browser.playwright.client.Pages;
import com.lfp.browser.script.stealth.StealthScript;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import com.microsoft.playwright.Page.ScreenshotOptions;
import com.microsoft.playwright.options.ScreenshotType;

public class SSTest {
	public static void main(String[] args) throws IOException {
		var folder = new File("temp/" + Utils.Crypto.getSecureRandomString());
		folder.mkdirs();
		try (var bc = BrowserContexts.connect()) {
			bc.addInitScript("console.log('neat');");
			var page = bc.getPage();
			page.navigate("https://bot.sannysoft.com/");
			page.onConsoleMessage(msg -> {
				System.out
						.println(Utils.Lots.stream(msg.args()).map(v -> v.jsonValue()).map(v -> v.toString()).toList());
			});
			page.evaluate("console.log('neat2');");
			page.evaluate(StealthScript.INSTANCE.get());
			long windowHeight;
			double scrollCount;
			{
				var sizes = Pages.getSizes(page);
				var pageWidth = sizes.getPageWidth();
				var pageHeight = sizes.getPageHeight();
				windowHeight = sizes.getWindowHeight();
				scrollCount = Math.ceil(pageHeight / windowHeight);
				scrollCount = Math.max(scrollCount, 1);
			}
			for (int i = 0; i < scrollCount; i++) {
				if (i > 0)
					Pages.scrollDown(page, i * windowHeight);
				File file = new File(folder, String.format("ss-%s.png", i));
				var viewportSize = page.viewportSize();
				var sizes = Pages.getSizes(page);
				var width = Optional.ofNullable(viewportSize).map(v -> v.width).orElse((int) sizes.getWindowWidth());
				var height = Optional.ofNullable(viewportSize).map(v -> v.width).orElse((int) sizes.getWindowWidth());
				page.setViewportSize(width, height + 1);
				Threads.sleepUnchecked(Duration.ofMillis(1_000));
				var ops = new ScreenshotOptions().setType(ScreenshotType.JPEG).setFullPage(true);
				boolean usePath = true;
				if (usePath)
					ops.setPath(file.toPath());
				var barr = page.screenshot(ops);
				if (!usePath)
					Files.write(file.toPath(), barr);
				page.setViewportSize(width, height);
				System.out.println(file.getAbsolutePath());
			}

		}
	}
}
