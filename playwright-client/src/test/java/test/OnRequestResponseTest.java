package test;

import java.net.URI;
import java.util.List;
import java.util.Locale;

import com.lfp.browser.playwright.client.BrowserContexts;

import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.Browser.NewContextOptions;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;

public class OnRequestResponseTest {

	public static void main(String[] args) {
		try (Playwright playwright = Playwright.create()) {
			for (var userAgent : List.of("",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36")) {
				try (BrowserContext context = BrowserContexts.connect()) {
					context.onRequest(r -> {
						System.out.println(String.format("type:%s event:%s userAgent:%s url:%s", "context", "request",
								!userAgent.isBlank(), r.url()));
					});
					context.onResponse(r -> {
						System.out.println(String.format("type:%s event:%s userAgent:%s url:%s", "context", "response",
								!userAgent.isBlank(), r.request().url()));
					});
					Page page = context.newPage();
					page.onRequest(r -> {
						System.out.println(String.format("type:%s event:%s userAgent:%s url:%s", "page", "request",
								!userAgent.isBlank(), r.url()));
					});
					page.onResponse(r -> {
						System.out.println(String.format("type:%s event:%s userAgent:%s url:%s", "page", "response",
								!userAgent.isBlank(), r.request().url()));
					});
					page.navigate("https://example.com/");
					System.out.println(page.title());
				}
			}
		}
	}

	private static NewContextOptions createNewContextOptions(int width, int height, Proxy proxy,
			NewContextOptionsModifier... newContextOptionsModifiers) {
		var options = new NewContextOptions();
		options.setUserAgent(UserAgents.CHROME_LATEST.get());
		// options.setScreenSize(width, height);
		options.setViewportSize(width, height);
		options.setBypassCSP(true);
		options.setLocale(Locale.US.toString());
		if (proxy != null && proxy.isAuthenticationEnabled())
			options.setHttpCredentials(proxy.getUsername().orElse(""), proxy.getPassword().orElse(""));
		Utils.Lots.stream(newContextOptionsModifiers).nonNull().forEach(v -> v.accept(options));
		return options;
	}
}
