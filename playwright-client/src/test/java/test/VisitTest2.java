package test;

import java.util.List;

import com.lfp.browser.playwright.client.BrowserContexts;
import com.lfp.joe.utils.Utils;
import com.microsoft.playwright.Page.ScreenshotOptions;

public class VisitTest2 {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		try (var bc = BrowserContexts.connect()) {
			bc.getPage().onRequest(r -> {
				// var json = Utils.Functions.catching(() -> Serials.Gsons.get().toJson(r), t ->
				// null);
				// System.out.println(json);
				System.out.println(r);
			});
			bc.getPage().navigate("https://help.twitter.com/en/forms/ipi/trademark");
			System.out.println(bc.getPage().title());
			bc.getPage().navigate("https://api.ipify.org");
			System.out.println(bc.getPage().content());
			bc.getPage().navigate("https://amazon.com");
			for (var ssUrl : List.of("https://bot.sannysoft.com", "https://arh.antoinevastel.com/bots/areyouheadless",
					"https://intoli.com/blog/not-possible-to-block-chrome-headless/chrome-headless-test.html")) {
				bc.getPage().navigate(ssUrl);
				var file = Utils.Files.tempFile(THIS_CLASS, "ss-" + Utils.Crypto.getSecureRandomString() + ".png");
				bc.getPage().screenshot(new ScreenshotOptions().setPath(file.toPath()));
				System.out.println(file.getAbsolutePath());
			}
		}
	}
}
