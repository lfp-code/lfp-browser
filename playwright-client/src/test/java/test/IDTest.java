package test;

import com.lfp.browser.playwright.client.BrowserContexts;

public class IDTest {

	public static void main(String[] args) {
		try (var bc = BrowserContexts.connect()) {
			var page = bc.getPage();
			page.navigate("http://example.com");
			var evaluateHandle = page.evaluateHandle(
					"fetch('http://localhost:3000/sessions/?token=BtufUg1KUHZpHPUJoIwgJH6ti1Z6CDscYL6HQ0mWNUbzY6gvS9ehSZ7PvCx4O5sP')\r\n"
							+ "  .then(response => response.text()).catch(error=>error.toString());");
			System.out.println(evaluateHandle.jsonValue());

		}
	}

}
