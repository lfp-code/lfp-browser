package test;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;

import com.lfp.browser.playwright.client.BrowserContexts;
import com.lfp.browser.playwright.client.Pages;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import com.microsoft.playwright.ElementHandle;

import one.util.streamex.StreamEx;

public class QuerySelectorAllContainsTextTest {

	public static void main(String[] args) {
		var counter = new AtomicInteger(0);
		try (var bc = BrowserContexts.launch()) {
			bc.getPage().navigate("https://example.com");
			System.out.println(bc.getPage().title());
			StreamEx<ElementHandle> result = bc.poll(Duration.ofSeconds(1), nil -> {
				System.out.println("polling:" + Thread.currentThread().getName());
				Threads.sleepUnchecked(2000);
				var stream = Pages.querySelectorAllContainsText(bc.getPage(), "example domain", false, "h1", "h2");
				if (counter.incrementAndGet() < 5)
					return null;
				var hasNext = Utils.Lots.hasNext(stream);
				if (!hasNext.getKey())
					return null;
				return hasNext.getValue();
			}).timeout(Duration.ofSeconds(30)).blockFirst();
			System.out.println(result.count());
		}
	}
}
