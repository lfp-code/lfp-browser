package test;

import com.lfp.browser.playwright.client.BrowserContexts;
import com.lfp.joe.utils.Utils;

import com.microsoft.playwright.Browser.NewContextOptions;
import com.microsoft.playwright.Page;

public class HarTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		var harFile = Utils.Files.tempFile(THIS_CLASS, "har", Utils.Crypto.getRandomString() + ".har");
		var videoDir = Utils.Files.tempFile(THIS_CLASS, "video", Utils.Crypto.getRandomString());
		videoDir.mkdirs();
		try (var bc = BrowserContexts.connect(ops -> {
			ops.setRecordHarPath(harFile.toPath());
		})) {
			Page page = bc.getPage();
			page.navigate("https://example.com/");
			page.navigate("https://api.ipify.org/");
		}
		System.out.println(harFile.getAbsolutePath());
	}
}
