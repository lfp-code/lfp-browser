package test;

import java.net.URI;

import com.lfp.browser.playwright.client.BrowserContexts;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;

import com.microsoft.playwright.Page.ScreenshotOptions;

public class VisitTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		try (var bc = BrowserContexts.launch(Proxy.builder(URI.create("http://localhost:8888")).build())) {
			bc.onClose(nil -> {
				System.out.println("closing");
			});
		}
		try (var bc = BrowserContexts.launch(Proxy.builder(URI.create("http://localhost:8888")).build())) {
			bc.onClose(nil -> {
				System.out.println("closing");
			});
			System.out.println(bc.getPublicIPAddress());
			bc.getPage().navigate("https://help.twitter.com/en/forms/ipi/trademark");
			System.out.println(bc.getPage().title());
			bc.getPage().navigate("https://amazon.com");
			var file = Utils.Files.tempFile(THIS_CLASS, "ss-" + Utils.Crypto.getSecureRandomString() + ".png");
			bc.getPage().screenshot(new ScreenshotOptions().setPath(file.toPath()));
			System.out.println(file.getAbsolutePath());
		}
	}
}
