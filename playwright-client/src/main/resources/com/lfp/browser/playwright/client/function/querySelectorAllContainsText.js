(arg1, arg2) => {
	console.log(JSON.stringify(arg1))
	console.log(JSON.stringify(arg2))
	var element;
	var args;
	if (Array.isArray(arg1)) {
		element = document;
		args = arg1;
	} else {
		element = arg1;
		args = arg2;
	}
	var getArgAt = (index) => {
		if (!args)
			return null;
		if (index >= args.length)
			return null;
		return args[index];
	};
	var text = getArgAt(0);
	var innerText = getArgAt(1);
	var selectors = getArgAt(2);
	var result = [];
	for (var selector of selectors) {
		if (utz.isBlank(selector))
			continue;
		for (var el of element.querySelectorAll(selector)) {
			var checkText;
			if (!!innerText)
				checkText = el.innerText;
			else
				checkText = el.textContent;
			if (utz.containsIgnoreCase(checkText, text))
				result.push(el);
		}
	}
	console.log(result)
	return result;
}