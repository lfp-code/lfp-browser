(url) => {
	return new Promise((resolve, reject) => {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", url, true);
		xhr.onload = function (e) {
			if (xhr.readyState === 4) {
				if (xhr.status === 200) {
					resolve(xhr.responseText);
				} else {
					resolve(xhr.statusText);
				}
			}
		};
		xhr.onerror = function (e) {
			resolve(xhr.statusText);
		};
		xhr.send(null);
	});
}