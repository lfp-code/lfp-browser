function createUtz() {
	var utz = {};
	utz.isNull = v => v === undefined || v === null;
	utz.isString = v => (typeof v) === 'string'
	utz.isBlank = str => {
		if (utz.isNull(str))
			return true;
		if (str.length == 0)
			return true;
		str = str.trim();
		if (str.length == 0)
			return true;
		return false;
	};
	utz.isNotBlank = str => !utz.isBlank(str);
	utz.trim = str => {
		if (utz.isNull(str))
			return str;
		return str.trim();
	};
	utz.equals = (v1, v2) => {
		if (utz.isNull(v1) || utz.isNull(v2))
			return utz.isNull(v1) === utz.isNull(v2);
		return v1 === v2;
	};
	utz.equalsIgnoreCase = (v1, v2) => {
		if (utz.isNull(v1) || utz.isNull(v2))
			return utz.isNull(v1) === utz.isNull(v2);
		if (v1 === v2)
			return true;
		return v1.toLowerCase(v1) === v2.toLowerCase(v2);
	};
	utz.containsIgnoreCase = (v1, v2) => {
		if (utz.isNull(v1) || utz.isNull(v2))
			return false;
		if (v1.indexOf(v2) >= 0)
			return true;
		return v1.toLowerCase(v1).indexOf(v2.toLowerCase(v2)) >= 0;
	};
	utz.toURL = v => {
		if (utz.isNull(v))
			return null;
		if (Object.getPrototypeOf(v) === URL)
			return v;
		if (!utz.isString(v))
			return null;
		try {
			return new URL(v);
		} catch (_) {
			return null;
		}
	};
	utz.isDisplayed = el => {
		if (utz.isNull(el))
			return false;
		var style = window.getComputedStyle(el);
		if (style.display === 'none')
			return false;
		return true;
	};
	var readyStates = {
		0: 'loading',
		1: 'interactive',
		2: 'complete'
	}
	function getReadyStateRank(readyState) {
		if (readyState == null)
			return null;
		for (const [key, value] of Object.entries(readyStates))
			if (utz.equalsIgnoreCase(readyState, value))
				return key;
		return null;
	}
	utz.whenReadyState = (readyState, func) => {
		var readyStateRank = getReadyStateRank(readyState);
		if (readyStateRank == null)
			throw new Error('invalid readyState:' + readyState)
		var funcRun = false;
		function funcWrap() {
			if (funcRun)
				return;
			var currentReadyStateRank = getReadyStateRank(document.readyState);
			if (currentReadyStateRank == null)
				return;
			if (currentReadyStateRank < readyStateRank)
				return;
			funcRun = true;
			document.removeEventListener('readystatechange', funcWrap)
			func()
		}
		try {
			document.addEventListener('readystatechange', funcWrap);
			funcWrap();
		} catch (e) {
			//suppress
		}
	};
	return utz;
}
try {
	globalThis.utz = createUtz();
} catch (e) {
	document.addEventListener('DOMContentLoaded', function(event) {
		globalThis.utz = createUtz();
	});
}