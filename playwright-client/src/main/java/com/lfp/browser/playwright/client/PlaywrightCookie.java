package com.lfp.browser.playwright.client;

import java.lang.reflect.Field;
import java.net.URI;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.lfp.joe.core.function.Throws;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;
import com.microsoft.playwright.options.Cookie;

public class PlaywrightCookie extends Cookie {

	private static final List<Field> FIELDS = Throws.unchecked(() -> Arrays.asList(Cookie.class.getDeclaredFields()));

	public static PlaywrightCookie from(Cookie cookie) {
		if (cookie == null || cookie instanceof PlaywrightCookie)
			return (PlaywrightCookie) cookie;
		var cookieUpdated = new PlaywrightCookie();
		for (var field : FIELDS)
			Throws.unchecked(() -> field.set(cookieUpdated, field.get(cookie)));
		return cookieUpdated;
	}

	protected PlaywrightCookie() {
		super(null, null);
	}

	public PlaywrightCookie(String name, String value) {
		super(name, value);
	}

	public Cookie setName(String name) {
		this.name = name;
		return this;
	}

	public Cookie setValue(String value) {
		this.value = value;
		return this;
	}

	public Optional<URI> tryGetURI() {
		return URIs.parse(url).or(() -> {
			var domain = TLDs.parseDomainName(Utils.Strings.stripStart(this.domain, "."));
			if (Utils.Strings.isBlank(domain))
				return Optional.empty();
			var uri = URI.create(String.format("http%s://%s", this.secure ? "s" : "", domain));
			return Optional.of(uri);
		});
	}

	public com.lfp.joe.net.cookie.Cookie toCookie() {
		var cb = com.lfp.joe.net.cookie.Cookie.builder();
		cb.name(this.name);
		cb.value(this.value);
		cb.domain(this.domain);
		cb.path(this.path);
		cb.maxAge(-1);
		cb.httpOnly(Boolean.TRUE.equals(this.httpOnly));
		cb.secure(Boolean.TRUE.equals(this.secure));
		var expires = this.expires;
		if (expires == null)
			cb.maxAge(-1);
		else {
			var expiresAt = Duration.ofSeconds(expires.longValue()).toMillis();
			var ttlSeconds = Duration.ofMillis(expiresAt - System.currentTimeMillis()).toSeconds();
			ttlSeconds = Math.max(0, ttlSeconds);
			cb.maxAge(ttlSeconds);
		}
		return cb.build();
	}
}
