package com.lfp.browser.playwright.client.function;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiFunction;

import com.lfp.joe.utils.Utils;

import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.JSHandle;

import one.util.streamex.StreamEx;

public enum QuerySelectorAllContainsText {
	INSTANCE;

	public StreamEx<ElementHandle> apply(BiFunction<String, Object, JSHandle> evaluator, String text, boolean innerText,
			String... selectors) {
		if (evaluator == null || text == null)
			return StreamEx.empty();
		var selectorList = Utils.Lots.stream(selectors).filter(Utils.Strings::isNotBlank).distinct().toList();
		if (selectorList.isEmpty())
			return StreamEx.empty();
		var script = Utils.Resources.getResourceAsString("querySelectorAllContainsText.js").get();
		var args = Arrays.asList(text, innerText, selectorList);
		var jsHandle = evaluator.apply(script, args);
		if (jsHandle == null)
			return StreamEx.empty();
		var properties = jsHandle.getProperties();
		return Utils.Lots.stream(properties).values().select(ElementHandle.class);
	}

}
