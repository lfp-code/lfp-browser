package com.lfp.browser.playwright.client.reflect;

import java.util.Objects;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.microsoft.playwright.Browser;
import com.microsoft.playwright.impl.Connection;
import com.microsoft.playwright.impl.Transport;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class Accessors {

	private static Class<? extends Browser> BrowserImpl_CLASS_TYPE = (Class) JavaCode.Reflections
			.tryForName("com.microsoft.playwright.impl.BrowserImpl").get();

	private static FieldAccessor<Browser, Connection> BrowserImpl_connection_FA = (FieldAccessor) JavaCode.Reflections
			.getFieldAccessorUnchecked(BrowserImpl_CLASS_TYPE, true, v -> "connection".equals(v.getName()),
					v -> Connection.class.isAssignableFrom(v.getType()));
	private static FieldAccessor<Connection, Transport> Transport_transport_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(Connection.class, true, v -> "transport".equals(v.getName()),
					v -> Transport.class.isAssignableFrom(v.getType()));

	public static Connection getConnection(Browser browser) {
		var result = BrowserImpl_connection_FA.get(browser);
		return Objects.requireNonNull(result);
	}

	public static Transport getTransport(Browser browser) {
		var connection = getConnection(browser);
		var result = Transport_transport_FA.get(connection);
		return Objects.requireNonNull(result);
	}

	public static void setTransport(Browser browser, Transport transport) {
		var connection = getConnection(browser);
		Transport_transport_FA.set(connection, transport);
	}
}
