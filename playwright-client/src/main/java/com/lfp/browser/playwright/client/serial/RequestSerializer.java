package com.lfp.browser.playwright.client.serial;

import java.lang.reflect.Type;
import java.util.Set;

import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.utils.Utils;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.github.throwable.beanref.BeanRoot;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.microsoft.playwright.Request;

@Serializer(value = Request.class, hierarchy = true)
public class RequestSerializer implements JsonSerializer<Request> {

	private static final Set<BeanPath<Request, ?>> BEAN_PATHS = BeanRef.$(Request.class).all();

	@Override
	public JsonElement serialize(Request src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		var jo = new JsonObject();
		for (var bp : BEAN_PATHS) {
			var value = serialize(src, bp, context);
			if (value != null)
				jo.add(bp.getPath(), value);
		}
		return jo;
	}

	private JsonElement serialize(Request src, BeanPath<Request, ?> bp, JsonSerializationContext context) {
		if (BeanRef.$(Request::timing).equals(bp))
			return null;
		if (BeanRef.$(Request::frame).equals(bp))
			return null;
		if (BeanRef.$(Request::response).equals(bp))
			return null;
		if (BeanRef.$(Request::redirectedTo).equals(bp))
			return null;
		if (BeanRef.$(Request::postData).equals(bp))
			return null;
		var value = bp.get(src);
		if (value instanceof byte[])
			value = Utils.Bits.from((byte[]) value).encodeBase64();
		return value == null ? null : context.serialize(value);
	}

	public static void main(String[] args) {
		var bps = BeanRef.$(Request.class).all();
		for (var bp : bps)
			System.out.println(String.format("%s - %s", bp.getPath(), bp.getType()));
	}

}
