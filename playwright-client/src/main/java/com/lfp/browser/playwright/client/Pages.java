package com.lfp.browser.playwright.client;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.jsoup.nodes.Document;

import com.lfp.browser.playwright.client.function.BrowserSizesFunction;
import com.lfp.browser.playwright.client.function.BrowserSizesFunction.BrowserSizes;
import com.lfp.browser.playwright.client.function.QuerySelectorAllContainsText;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Requires;
import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Page.ScreenshotOptions;

import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.StreamEx;

public class Pages {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final List<String> INVALID_URI_PREFIXES = List.of("data:", "javascript:");
	private static final File SCREEN_SHOT_DIR;
	static {
		var file = Utils.Files.tempFile(THIS_CLASS, "screen-shots");
		file.mkdirs();
		SCREEN_SHOT_DIR = file;
	}

	public static FileExt screenshot(Page page) {
		return screenshot(page, (ScreenshotOptions) null);
	}

	@SuppressWarnings("resource")
	public static FileExt screenshot(Page page, ScreenshotOptions screenshotOptions) {
		if (screenshotOptions == null)
			screenshotOptions = new ScreenshotOptions();
		if (screenshotOptions.fullPage == null)
			screenshotOptions.setFullPage(true);
		FileExt file = null;
		if (screenshotOptions.path == null) {
			file = new FileExt(SCREEN_SHOT_DIR, "ss" + Utils.Crypto.getRandomString() + ".png").deleteAllOnScrap(true);
			screenshotOptions.setPath(file.toPath());
		}
		page.screenshot(screenshotOptions);
		if (file == null)
			file = FileExt.from(screenshotOptions.path.toFile());
		return file;
	}

	public static void scrollBy(Page page, long horizontalPx, long verticalPx) {
		Function<Long, Double> normalize = v -> {
			if (v == null)
				return 0.0;
			if (v > Double.MAX_VALUE)
				return Double.MAX_VALUE;
			if (v < Double.MIN_VALUE)
				return Double.MIN_VALUE;
			return v.doubleValue();
		};
		var h = normalize.apply(horizontalPx);
		var v = normalize.apply(verticalPx);
		page.evaluate("([h,v])=>window.scrollBy(h,v);", Arrays.asList(h, v));
	}

	public static void scrollUp(Page page, long pixelCount) {
		Requires.isPositive(pixelCount);
		scrollBy(page, 0, Optional.ofNullable(pixelCount).map(v -> -1 * v).orElse(null));
	}

	public static void scrollUpToTop(Page page) {
		scrollBy(page, 0, Long.MIN_VALUE);
	}

	public static void scrollDown(Page page, long pixelCount) {
		Requires.isPositive(pixelCount);
		scrollBy(page, 0, pixelCount);
	}

	public static void scrollDownToBottom(Page page) {
		scrollBy(page, 0, Long.MAX_VALUE);
	}

	public static void removeVideo(Page page) {
		page.evaluate("for(video of document.querySelectorAll(\"video\")) {video.remove()}");
	}

	public static Document html(Page page) {
		var html = page.content();
		try (var is = Utils.Bits.from(html).inputStream()) {
			return Jsoups.parse(is, Utils.Bits.getDefaultCharset(), URIs.parse(page.url()).orElse(null));
		} catch (IOException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}

	public static Optional<URI> getAttributeAsURI(Page page, ElementHandle element, String attribute) {
		if (page == null || element == null)
			return Optional.empty();
		if (Utils.Strings.isBlank(attribute))
			return Optional.empty();
		var value = element.getAttribute(attribute);
		if ("#".equals(value))
			return URIs.parse(page.url());
		if (Utils.Strings.isBlank(value))
			return Optional.empty();
		for (var prefix : INVALID_URI_PREFIXES)
			if (Utils.Strings.startsWith(value, prefix))
				return Optional.empty();
		var uriOp = URIs.parse(value);
		if (uriOp.isPresent())
			return uriOp;
		var currentURI = URIs.parse(page.url()).orElse(null);
		if (currentURI == null)
			return Optional.empty();
		var url = UrlBuilder.fromUri(currentURI).withPath(null).toUri() + value;
		return URIs.parse(url);
	}

	public static BrowserSizes getSizes(Page page) {
		return BrowserSizesFunction.INSTANCE.apply(page);
	}

	public static StreamEx<ElementHandle> select(Page page, String... selectors) {
		return DOMs.select(page, selectors);
	}

	public static StreamEx<ElementHandle> querySelectorAllContainsText(Page page, String text, boolean innerText,
			String... selectors) {
		return QuerySelectorAllContainsText.INSTANCE.apply(page::evaluateHandle, text, innerText, selectors);
	}

	public static Optional<URI> followRedirect(Page page, URI uri) {
		if (page == null || uri == null)
			return Optional.empty();
		var script = "(url)=>{try{return fetch(url).then(response=>response.url).catch(e=>null);}catch(e){return null;}}";
		var scriptResult = page.evaluate(script, uri.toString());
		if (scriptResult == null)
			return Optional.empty();
		return URIs.parse(scriptResult.toString());
	}
}
