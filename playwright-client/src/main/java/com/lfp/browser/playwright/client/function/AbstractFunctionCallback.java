package com.lfp.browser.playwright.client.function;

import java.util.Optional;
import java.util.function.Function;

import com.microsoft.playwright.options.FunctionCallback;

public abstract class AbstractFunctionCallback implements FunctionCallback {

	@Override
	public Object call(Object... args) {
		return call(index -> {
			if (index < 0 || args == null || args.length <= index)
				return Optional.empty();
			return Optional.ofNullable(args[index]);
		});
	}

	protected abstract Object call(Function<Integer, Optional<Object>> argFunc);
}
