package com.lfp.browser.playwright.client.serial;

import java.lang.reflect.Type;
import java.util.Set;

import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.serial.serializer.Serializer;
import com.lfp.joe.utils.Utils;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.microsoft.playwright.Response;

@Serializer(value = Response.class, hierarchy = true)
public class ResponseSerializer implements JsonSerializer<Response> {

	private static final Set<BeanPath<Response, ?>> BEAN_PATHS = BeanRef.$(Response.class).all();

	@Override
	public JsonElement serialize(Response src, Type typeOfSrc, JsonSerializationContext context) {
		if (src == null)
			return JsonNull.INSTANCE;
		var jo = new JsonObject();
		for (var bp : BEAN_PATHS) {
			var value = serialize(src, bp, context);
			if (value != null)
				jo.add(bp.getPath(), value);
		}
		return jo;
	}

	private JsonElement serialize(Response src, BeanPath<Response, ?> bp, JsonSerializationContext context) {
		if (BeanRef.$(Response::frame).equals(bp))
			return null;
		if (BeanRef.$(Response::text).equals(bp))
			return null;
		if (BeanRef.$(Response::body).equals(bp) && !shouldReadBody(src))
			return null;
		var value = Utils.Functions.catching(() -> bp.get(src), t -> null);
		if (value instanceof byte[])
			value = Utils.Bits.from((byte[]) value).encodeBase64();
		return value == null ? null : context.serialize(value);
	}

	private boolean shouldReadBody(Response src) {
		var headers = HeaderMap.of(src.headers());
		var mediaType = Headers.tryGetMediaType(headers).orElse(null);
		if (mediaType == null)
			return true;
		if ("video".equalsIgnoreCase(mediaType.type()))
			return false;
		if ("image".equalsIgnoreCase(mediaType.type()))
			return false;
		return true;
	}

	public static void main(String[] args) {
		var bps = BeanRef.$(Response.class).all();
		for (var bp : bps)
			System.out.println(String.format("%s - %s", bp.getPath(), bp.getType()));
	}

}
