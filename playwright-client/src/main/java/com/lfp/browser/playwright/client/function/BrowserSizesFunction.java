package com.lfp.browser.playwright.client.function;

import java.util.function.Function;

import com.lfp.browser.playwright.client.function.BrowserSizesFunction.BrowserSizes;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.microsoft.playwright.Page;

public enum BrowserSizesFunction implements Function<Page, BrowserSizes> {
	INSTANCE;

	@Override
	public BrowserSizes apply(Page page) {
		var script = Utils.Resources.getResourceAsString("browserSizes.js").get();
		var returnValue = page.evaluateHandle(script);
		var returnValueJson = Serials.Gsons.get().toJson(returnValue.jsonValue());
		return Serials.Gsons.get().fromJson(returnValueJson, BrowserSizes.class);
	}

	public static class BrowserSizes {

		@SerializedName("windowWidth")
		@Expose
		private long windowWidth;
		@SerializedName("windowHeight")
		@Expose
		private long windowHeight;
		@SerializedName("pageWidth")
		@Expose
		private double pageWidth;
		@SerializedName("pageHeight")
		@Expose
		private long pageHeight;
		@SerializedName("screenWidth")
		@Expose
		private long screenWidth;
		@SerializedName("screenHeight")
		@Expose
		private long screenHeight;
		@SerializedName("pageX")
		@Expose
		private long pageX;
		@SerializedName("pageY")
		@Expose
		private long pageY;
		@SerializedName("screenX")
		@Expose
		private long screenX;
		@SerializedName("screenY")
		@Expose
		private long screenY;

		public long getWindowWidth() {
			return windowWidth;
		}

		public long getWindowHeight() {
			return windowHeight;
		}

		public double getPageWidth() {
			return pageWidth;
		}

		public long getPageHeight() {
			return pageHeight;
		}

		public long getScreenWidth() {
			return screenWidth;
		}

		public long getScreenHeight() {
			return screenHeight;
		}

		public long getPageX() {
			return pageX;
		}

		public long getPageY() {
			return pageY;
		}

		public long getScreenX() {
			return screenX;
		}

		public long getScreenY() {
			return screenY;
		}

	}

}
