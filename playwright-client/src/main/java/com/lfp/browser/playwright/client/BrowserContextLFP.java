package com.lfp.browser.playwright.client;

import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Function;

import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Page.WaitForFunctionOptions;
import com.microsoft.playwright.options.Cookie;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import reactor.core.publisher.Flux;

public interface BrowserContextLFP extends BrowserContext {

	Optional<Proxy> proxy();

	String trackingId();

	default Page getPage() {
		var page = Utils.Lots.stream(this.pages()).findFirst().orElseGet(() -> this.newPage());
		return page;
	}

	default <U, T extends Throwable> U newPage(ThrowingFunction<Page, U, T> accessor) throws T {
		var page = newPage();
		try {
			return accessor.apply(page);
		} finally {
			page.close();
		}
	}

	default Optional<String> getPublicIPAddress() {
		var script = Utils.Resources.getResourceAsString("ipAddressLookup.js").get();
		var result = this.getPage().evaluate(script, IPs.getIPLookupServiceURI().toString());
		return Utils.Types.tryCast(result, String.class).filter(IPs::isValidIpAddress);
	}

	default String getUserAgent() {
		var result = this.getPage().evaluate("() => navigator.userAgent;");
		return Utils.Types.tryCast(result, String.class).orElse(null);
	}

	default void disableServiceWorkers() {
		this.addInitScript(
				"navigator.serviceWorker.register=()=>new Pact(()=>{},()=>{});console.log('disabled serviceWorkers');");
	}

	default StorageState getStorageState() {
		return new StorageState(storageState());
	}

	default StreamEx<PlaywrightCookie> streamPlaywrightCookies(URI... uris) {
		var urls = Utils.Lots.stream(uris).nonNull().distinct().map(Object::toString).toList();
		List<Cookie> cookies;
		if (urls.isEmpty())
			cookies = cookies();
		else
			cookies = cookies(urls);
		return Streams.of(cookies).map(PlaywrightCookie::from);
	}

	default EntryStream<Optional<URI>, com.lfp.joe.net.cookie.Cookie> streamCookies(URI... uris) {
		return streamPlaywrightCookies(uris).mapToEntry(v -> v.tryGetURI(), v -> v.toCookie());
	}

	default boolean addCookies(CookieStoreLFP cookieStore) {
		if (cookieStore == null)
			return false;
		boolean result = false;
		try (var estream = cookieStore.streamURIs().mapToEntry(v -> cookieStore.get(v));) {
			for (var ent : estream) {
				var uri = ent.getKey();
				var httpCookies = ent.getValue();
				if (this.addCookies(Utils.Lots.stream(httpCookies).nonNull()
						.mapToEntry(v -> uri, com.lfp.joe.net.cookie.Cookie::build).nonNull()))
					result = true;
			}
		}
		return result;
	}

	default boolean addCookies(Iterable<? extends Entry<URI, com.lfp.joe.net.cookie.Cookie>> cookieEntries) {
		if (cookieEntries == null)
			return false;
		boolean result = false;
		var uriToCookieMap = Utils.Lots.streamEntries(cookieEntries).mapKeys(k -> Optional.ofNullable(k)).grouping();
		for (var ent : uriToCookieMap.entrySet()) {
			var uri = ent.getKey().orElse(null);
			var browserCookies = Utils.Lots.stream(ent.getValue()).map(v -> toCookie(uri, v)).nonNull().toList();
			if (!browserCookies.isEmpty()) {
				result = true;
				this.addCookies(browserCookies);
			}
		}
		return result;
	}

	@Deprecated
	default <X> Flux<X> poll(Duration pollInterval, Function<BrowserContextLFP, X> waitFunction) {
		Callable<X> untilTask = waitFunction == null ? null : () -> waitFunction.apply(this);
		WaitForFunctionOptions waitForFunctionOptions = new WaitForFunctionOptions();
		if (pollInterval != null)
			waitForFunctionOptions = waitForFunctionOptions.setPollingInterval(pollInterval.toMillis());
		return DOMs.pollUntil(untilTask, waitForFunctionOptions);
	}

	private static Cookie toCookie(URI uri, com.lfp.joe.net.cookie.Cookie cookie) {
		if (cookie == null)
			return null;
		if (uri == null && Utils.Strings.isBlank(cookie.getDomain()))
			return null;
		var browserCookie = new Cookie(cookie.getName(), cookie.getValue());
		browserCookie.setSecure(cookie.isSecure());
		browserCookie.setHttpOnly(cookie.isHttpOnly());
		if (Utils.Strings.isNotBlank(cookie.getDomain()))
			browserCookie.setDomain(cookie.getDomain());
		else if (uri != null)
			browserCookie.setUrl(uri.toString());
		var expires = cookie.getExpiresAt().map(v -> v.getTime()).map(v -> Duration.ofMillis(v).toSeconds())
				.orElse(null);
		if (expires != null)
			browserCookie.setExpires(expires);
		if (Utils.Strings.isNotBlank(cookie.getPath()))
			browserCookie.setPath(cookie.getPath());
		return browserCookie;
	}

}
