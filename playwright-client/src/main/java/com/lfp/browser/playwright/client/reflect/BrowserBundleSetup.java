package com.lfp.browser.playwright.client.reflect;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.json.XML;

import com.google.gson.JsonElement;
import com.google.re2j.Pattern;
import com.lfp.browser.playwright.client.config.PlaywrightClientConfig;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.maven.MavenOrgSearchService;
import com.lfp.joe.reflections.reflection.LazyJar;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.time.TimeParser;
import com.microsoft.playwright.Playwright;

import at.favre.lib.bytes.Bytes;
import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.StreamEx;

public class BrowserBundleSetup extends MemoizedSupplier.Impl<Nada> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String DRIVER_JAR_CLASS_NAME = "com.microsoft.playwright.impl.DriverJar";
	private static final URI RESPOSITORY_URI = URI.create(
			"https://oss.sonatype.org/service/local/repositories/snapshots/content/com/microsoft/playwright/driver-bundle");
	private static final CharSequence SNAPSHOT_APPEND = "-SNAPSHOT";
	public static final BrowserBundleSetup INSTANCE = new BrowserBundleSetup();

	public BrowserBundleSetup() {
		super(() -> {
			setup();
			return Nada.get();
		});
	}

	private static void setup() {
		if (CoreReflections.tryForName(DRIVER_JAR_CLASS_NAME).isPresent())
			return;
		Utils.Functions.unchecked(() -> setupInternal());
	}

	private static void setupInternal() throws IOException, InterruptedException {
		String version = getVersion();
		URI jarURI = getJarURI(version);
		LazyJar.create(DRIVER_JAR_CLASS_NAME, () -> jarURI).setup();
	}

	private static URI getJarURI(String version) throws IOException, InterruptedException {
		Set<String> versionAttempts;
		{
			StreamEx<String> stream = StreamEx.of(version);
			if (!Utils.Strings.endsWithIgnoreCase(version, SNAPSHOT_APPEND))
				stream = stream.append(version + SNAPSHOT_APPEND);
			var preDash = Utils.Strings.substringBefore(version, "-");
			if (Utils.Strings.isNotBlank(preDash))
				stream = stream.append(preDash + SNAPSHOT_APPEND);
			stream = stream.filter(Utils.Strings::isNotBlank);
			versionAttempts = stream.toCollection(LinkedHashSet::new);
		}
		for (var versionAttempt : versionAttempts) {
			var respositoryURI = URIs.normalize(RESPOSITORY_URI);
			var versionAttemptURI = UrlBuilder.fromUri(respositoryURI)
					.withPath(respositoryURI.getPath() + "/" + versionAttempt)
					.toUri();
			var je = getJsonElement(versionAttemptURI).orElse(null);
			var jarr = Serials.Gsons.tryGetAsJsonArray(je, "content", "data", "content-item").orElse(null);
			var result = parseContentItem(jarr);
			if (result != null)
				return result;
		}
		for (var versionAttempt : versionAttempts) {
			var result = MavenOrgSearchService.get()
					.getURI("com.microsoft.playwright", "driver-bundle", versionAttempt)
					.orElse(null);
			if (result != null)
				return result;
		}
		throw new IllegalArgumentException("unable to find jar URI:" + version);
	}

	private static URI parseContentItem(com.google.gson.JsonArray contentItem) {
		if (contentItem == null || contentItem.size() <= 0)
			return null;
		var estream = Streams.of(contentItem).mapToEntry(v -> {
			return Serials.Gsons.tryGetAsString(v, "resourceURI").flatMap(URIs::parse).orElse(null);
		}, v -> {
			return Serials.Gsons.tryGetAsString(v, "lastModified")
					.flatMap(TimeParser.instance()::tryParseDate)
					.orElse(null);
		});
		estream = estream.nonNullKeys().nonNullValues();
		estream = estream.filterKeys(v -> {
			var path = URIs.normalizePath(v);
			if (!Utils.Strings.endsWith(path, ".jar"))
				return false;
			if (Utils.Strings.isBlank(path))
				return false;
			path = Utils.Strings.substringBeforeLast(path, ".jar");
			if (Utils.Strings.endsWith(path, "-sources"))
				return false;
			if (Utils.Strings.endsWith(path, "-javadoc"))
				return false;
			var lastChar = path.substring(path.length() - 1, path.length());
			if (!Utils.Strings.isNumeric(lastChar))
				return false;
			return true;
		});
		estream = estream.sortedBy(ent -> ent.getValue().getTime() * -1);
		return estream.keys().findFirst().orElse(null);
	}

	private static String getVersion() throws IOException, InterruptedException {
		String msgTemplate = "browser bundle version:{} source:{}";
		var version = getVersionFromJar().orElse(null);
		if (Utils.Strings.isNotBlank(version)) {
			logger.info(msgTemplate, version, "jar");
			return version;
		}
		version = getVersionConfig().orElse(null);
		if (Utils.Strings.isNotBlank(version)) {
			logger.info(msgTemplate, version, "properties");
			return version;
		}
		version = getVersionLatest();
		if (Utils.Strings.isNotBlank(version)) {
			logger.info(msgTemplate, version, "latestURL");
			return version;
		}
		throw new IllegalArgumentException("unable to lookup browser bundle version");
	}

	@SuppressWarnings("deprecation")
	private static Optional<String> getVersionFromJar() {
		var jarFile = JavaCode.Reflections.getJarFile(Playwright.class).orElse(null);
		if (jarFile == null)
			return Optional.empty();
		String path = String.format("META-INF/maven/%s/playwright/pom.xml", Playwright.class.getPackage().getName());
		var jsonElement = Utils.Functions.catching(() -> {
			var entry = jarFile.getEntry(path);
			if (entry == null)
				return null;
			var is = jarFile.getInputStream(entry);
			return getJsonElement(is).orElse(null);
		}, t -> null);
		var version = Serials.Gsons.tryGetAsString(jsonElement, "project", "parent", "version").orElse(null);
		return Utils.Strings.trimToNullOptional(version);
	}

	private static Optional<String> getVersionConfig() {
		var version = Configs.get(PlaywrightClientConfig.class).playwrightVersion();
		return Utils.Strings.trimToNullOptional(version);
	}

	private static String getVersionLatest() throws IOException, InterruptedException {
		var jsonElement = getJsonElement(RESPOSITORY_URI).orElse(null);
		var jarr = Serials.Gsons.tryGetAsJsonArray(jsonElement, "content", "data", "content-item").get();
		var versionStream = Utils.Lots.stream(jarr).map(v -> {
			return Serials.Gsons.tryGetAsString(v, "text").orElse(null);
		});
		versionStream = versionStream.filter(Utils.Strings::isNotBlank);
		Function<String, List<Integer>> versionParser = (v) -> {
			v = Utils.Strings.trimToEmpty(v);
			v = Utils.Strings.substringBefore(v, "-");
			var parts = v.split(Pattern.quote("."));
			var intStream = Utils.Lots.stream(parts).map(vv -> {
				var intValue = Utils.Strings.parseNumber(vv).map(Number::intValue).orElse(null);
				return intValue;
			});
			intStream = intStream.nonNull();
			return intStream.toList();
		};
		BiFunction<List<Integer>, Integer, Integer> sortValueGetter = (ver, i) -> {
			if (ver.size() <= i)
				return Integer.MAX_VALUE;
			return -1 * ver.get(i);
		};
		Comparator<String> sorter = (v1, v2) -> {
			var ver1 = versionParser.apply(v1);
			var ver2 = versionParser.apply(v2);
			Comparator<List<Integer>> subComparator = Comparator.comparing(v -> 0);
			for (int i = 0; i < Math.max(ver1.size(), ver2.size()); i++) {
				int index = i;
				subComparator = subComparator.thenComparing(ver -> {
					return sortValueGetter.apply(ver, index);
				});
			}
			return subComparator.compare(ver1, ver2);
		};
		versionStream = versionStream.sorted(sorter);
		var version = versionStream.findFirst().get();
		return version;
	}

	private static Optional<JsonElement> getJsonElement(URI uri) throws IOException, InterruptedException {
		HttpClient client = HttpClients.newClient();
		HttpRequest request = HttpRequests.request().uri(uri).build();
		return getJsonElement(client.send(request, BodyHandlers.ofInputStream()).body());
	}

	private static Optional<JsonElement> getJsonElement(InputStream is) throws IOException {
		if (is == null)
			return Optional.empty();
		Bytes bytes;
		try {
			bytes = Utils.Bits.from(is);
		} finally {
			is.close();
		}
		var str = bytes.encodeCharset(Utils.Bits.getDefaultCharset());
		str = Utils.Strings.trimToNull(str);
		if (str == null)
			return Optional.empty();
		var asJson = Utils.Functions.catching(str, v -> XML.toJSONObject(v), t -> null);
		if (asJson == null)
			return Optional.empty();
		var je = Serials.Gsons.getJsonParser().parse(asJson.toString());
		return Optional.of(je);
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		System.out.println(BrowserBundleSetup.getVersion());
		System.out.println(BrowserBundleSetup.getVersionLatest());
		BrowserBundleSetup.INSTANCE.get();
		System.err.println("done");
	}
}
