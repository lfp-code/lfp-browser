package com.lfp.browser.playwright.client;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nullable;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.JsonAdapter;
import com.lfp.browser.playwright.client.ImmutableStorageState.StorageStateOrigin;
import com.lfp.browser.playwright.client.ImmutableStorageState.StorageStateStorage;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

@Gson.TypeAdapters
@JsonAdapter(StorageState.Serializer.class)
@ValueLFP.Style
@Value.Enclosing
public class StorageState {

	private static final String COOKIES_KEY = "cookies";
	private static final String ORIGINS_KEY = "origins";
	private static final String EMPTY = "{}";

	private String raw;
	private JsonObject rawJson;

	public StorageState(String raw) {
		super();
		this.raw = Utils.Strings.trimToNullOptional(raw).orElse(EMPTY);
	}

	public StorageState(JsonObject rawJson) {
		super();
		this.rawJson = Optional.ofNullable(rawJson).orElseGet(JsonObject::new);
	}

	public String getRaw() {
		return Optional.ofNullable(raw).orElseGet(rawJson::toString);
	}

	@SuppressWarnings("deprecation")
	public JsonObject getRawJson() {
		if (rawJson == null) {
			rawJson = Serials.Gsons.getJsonParser().parse(raw).getAsJsonObject();
			raw = null;
		}
		return rawJson;
	}

	private transient List<PlaywrightCookie> _cookies;

	public List<PlaywrightCookie> getPlaywrightCookies() {
		if (_cookies == null) {
			var jarr = Serials.Gsons.tryGetAsJsonArray(getRawJson(), COOKIES_KEY).orElse(null);
			_cookies = Streams.of(jarr).map(v -> Serials.Gsons.get().fromJson(v, PlaywrightCookie.class)).nonNull()
					.toMutableList();
		}
		return _cookies;
	}

	private transient List<StorageStateOrigin> _origins;

	public List<StorageStateOrigin> getOrigins() {
		if (_origins == null) {
			var jarr = Serials.Gsons.tryGetAsJsonArray(getRawJson(), ORIGINS_KEY).orElse(null);
			_origins = Streams.of(jarr).map(v -> Serials.Gsons.get().fromJson(v, StorageStateOrigin.class)).nonNull()
					.toMutableList();
		}
		return _origins;
	}

	@Value.Immutable
	public static abstract class AbstractStorageStateOrigin {

		@Nullable
		public abstract URI origin();

		public abstract List<StorageStateStorage> localStorage();

	}

	@Value.Immutable
	public static abstract class AbstractStorageStateStorage {

		@Nullable
		public abstract String name();

		@Nullable
		public abstract String value();

	}

	public static class Serializer implements JsonSerializer<StorageState>, JsonDeserializer<StorageState> {

		@Override
		public JsonElement serialize(StorageState src, Type typeOfSrc, JsonSerializationContext context) {
			return Optional.ofNullable(src).map(v -> (JsonElement) v.getRawJson()).orElseGet(() -> JsonNull.INSTANCE);
		}

		@Override
		public StorageState deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			if (json == null || !json.isJsonObject())
				return null;
			return new StorageState(json.getAsJsonObject());
		}

	}
}
