package com.lfp.browser.playwright.client.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources({ "file:src/main/resources/com/lfp/browser/playwright/client/config/PlaywrightClientConfig.properties",
		"classpath:com/lfp/browser/playwright/client/config/PlaywrightClientConfig.properties" })
public interface PlaywrightClientConfig extends Config {

	@Key("com.microsoft.playwright.version")
	String playwrightVersion();
}
