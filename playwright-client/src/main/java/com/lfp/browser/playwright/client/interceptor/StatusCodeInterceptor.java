package com.lfp.browser.playwright.client.interceptor;

import java.net.URI;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

import org.apache.commons.io.comparator.CompositeFileComparator;

import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Response;

import one.util.streamex.EntryStream;

public class StatusCodeInterceptor implements Consumer<Response> {

	private final Map<URI, List<Entry<Date, Integer>>> codeMap = new ConcurrentHashMap<>();
	private final Predicate<Response> responseFilter;

	public StatusCodeInterceptor() {
		this(null);
	}

	public StatusCodeInterceptor(Predicate<Response> responseFilter) {
		this.responseFilter = responseFilter;
	}

	@Override
	public void accept(Response response) {
		var uri = URIs.parse(response.request().url()).orElse(null);
		if (uri == null)
			return;
		if (responseFilter != null && !responseFilter.test(response))
			return;
		var sc = response.status();
		codeMap.compute(uri, (nil, scList) -> {
			if (scList == null)
				scList = new ArrayList<>();
			scList.add(Utils.Lots.entry(new Date(), sc));
			return scList;
		});
	}

	public void apply(BrowserContext browserContext) {
		for (var page : browserContext.pages())
			apply(page);
		browserContext.onPage(this::apply);
	}

	public void apply(Page page) {
		page.onResponse(this);
	}

	public Optional<Integer> get(Page page) {
		if (page == null)
			return Optional.empty();
		var uri = URIs.parse(page.url()).orElse(null);
		return stream(uri).values().findFirst();
	}

	public Optional<Integer> get(URI uri) {
		return stream(uri).values().findFirst();
	}

	public Optional<Integer> get() {
		return stream().values().findFirst();
	}

	public EntryStream<Date, Integer> stream() {
		return streamInternal(null);
	}

	public EntryStream<Date, Integer> stream(URI uri) {
		if (uri == null)
			return EntryStream.empty();
		return streamInternal(uri);
	}

	protected EntryStream<Date, Integer> streamInternal(URI uri) {
		var uriNorm = URIs.normalize(uri);
		var codeMapStream = Utils.Lots.stream(this.codeMap);
		if (uriNorm != null)
			codeMapStream = codeMapStream.filterKeys(k -> {
				return uriNorm.equals(URIs.normalize(k));
			});
		var parentChildEntryStreams = Utils.Lots.stream(this.codeMap).map(ent -> {
			return Utils.Lots.stream(ent.getValue()).mapToEntry(v -> ent, v -> v);
		});
		var parentChildEntryStream = Utils.Lots.flatMap(parentChildEntryStreams);
		// sort by time first then position
		Comparator<Entry<Entry<URI, List<Entry<Date, Integer>>>, Entry<Date, Integer>>> sorter = Comparator
				.comparing(v -> 0);
		sorter = sorter.thenComparing(ent -> {
			var createdAt = ent.getValue().getKey();
			return createdAt.getTime() * -1;
		});
		sorter = sorter.thenComparing(ent -> {
			var list = ent.getKey().getValue();
			return list.indexOf(ent.getKey()) * -1;
		});
		parentChildEntryStream = parentChildEntryStream.sorted(sorter);
		var resultStream = parentChildEntryStream.map(Entry::getValue).mapToEntry(Entry::getKey, Entry::getValue);
		return resultStream;
	}
}
