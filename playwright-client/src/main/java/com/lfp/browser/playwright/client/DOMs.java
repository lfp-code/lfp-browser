package com.lfp.browser.playwright.client;

import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import com.lfp.browser.playwright.client.function.QuerySelectorAllContainsText;
import com.lfp.joe.core.function.Durations;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.ElementHandle.ClickOptions;
import com.microsoft.playwright.ElementHandle.TypeOptions;
import com.microsoft.playwright.JSHandle;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Page.WaitForFunctionOptions;
import com.microsoft.playwright.TimeoutError;

import one.util.streamex.StreamEx;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class DOMs {

	private static final Duration TIMEOUT_DEFAULT = Duration.ofSeconds(30);
	private static final Duration POLL_INTERVAL_DEFAULT = Duration.ofMillis(150);
	private static final String QUERY_SELECTOR_ALL_FUNCTION = "(selectors)=>{var els=[];for(var selector of selectors){document.querySelectorAll(selector).forEach((v=>els.push(v)))}return els.length>0?els:null};";
	private static final String KEY_BACKSPACE = "Backspace";

	/**
	 * page
	 */

	public static StreamEx<ElementHandle> select(Page page, String... selectors) {
		return select(page, withTimeout(null, null), selectors);
	}

	public static StreamEx<ElementHandle> select(Page page, Duration timeout, String... selectors) {
		return select(page, withTimeout(null, timeout), selectors);
	}

	/**
	 * 
	 * @param page
	 * @param waitForFunctionOptions - timeout: must be >=0, 0ms waits forever, null
	 *                               or 1ms does not wait
	 * @param selectors
	 * @return
	 */
	public static StreamEx<ElementHandle> select(Page page, WaitForFunctionOptions waitForFunctionOptions,
			String... selectors) {
		if (page == null)
			return Streams.empty();
		var selectorList = Streams.of(selectors).filter(Utils.Strings::isNotBlank).distinct().toList();
		if (selectorList.isEmpty())
			return Streams.empty();
		if (getTimeout(waitForFunctionOptions).isEmpty())
			return Streams.of(selectorList).map(page::querySelectorAll).chain(Streams.flatMap(Streams::of)).distinct();
		Supplier<Iterable<JSHandle>> loader = () -> {
			JSHandle jsHandle;
			try {
				jsHandle = page.waitForFunction(QUERY_SELECTOR_ALL_FUNCTION, selectors, waitForFunctionOptions);
			} catch (TimeoutError e) {
				return null;
			}
			return Optional.ofNullable(jsHandle).map(JSHandle::getProperties).map(Map::values).orElse(null);

		};
		return Streams.of(loader).map(JSHandle::asElement).distinct();
	}

	public static StreamEx<ElementHandle> selectContainsText(Page page, String text, boolean innerText,
			String... selectors) {
		return QuerySelectorAllContainsText.INSTANCE.apply(page::evaluateHandle, text, innerText, selectors);
	}

	/**
	 * element
	 */

	public static StreamEx<ElementHandle> select(ElementHandle element, String... selectors) {
		if (element == null)
			return Streams.empty();
		var selectorStream = Streams.of(selectors).filter(Utils.Strings::isNotBlank).distinct();
		return selectorStream.map(v -> element.querySelectorAll(v)).chain(Streams.flatMap(Streams::of));
	}

	public static StreamEx<ElementHandle> selectContainsText(ElementHandle elementHandle, String text,
			boolean innerText, String... selectors) {
		return QuerySelectorAllContainsText.INSTANCE.apply(elementHandle::evaluateHandle, text, innerText, selectors);
	}

	public static ElementHandle closest(ElementHandle element, String selector) {
		if (element == null)
			return null;
		var handle = element.evaluateHandle("(el,selector)=>el.closest(selector)", selector);
		if (handle == null)
			return null;
		return handle.asElement();
	}

	public static ElementHandle parent(ElementHandle element) {
		Objects.requireNonNull(element);
		return element.evaluateHandle("el => el.parentElement").asElement();
	}

	public static void clearAndType(ElementHandle element, String value) {
		clearAndType(element, value, null);
	}

	public static void clearAndType(ElementHandle element, String text, TypeOptions typeOptions) {
		Objects.requireNonNull(element);
		element.click(new ClickOptions().setClickCount(3));
		element.press(KEY_BACKSPACE);
		element.type(text, typeOptions);
	}

	/**
	 * utils
	 */

	public static <U, E extends Exception> Flux<U> pollUntil(Callable<U> untilTask) {
		return pollUntil(untilTask, withTimeout(null, null));
	}

	public static <U, E extends Exception> Flux<U> pollUntil(Callable<U> untilTask, Duration timeout) {
		return pollUntil(untilTask, withTimeout(null, timeout));
	}

	public static <U, E extends Exception> Flux<U> pollUntil(Callable<U> untilTask,
			WaitForFunctionOptions waitForFunctionOptions) {
		if (untilTask == null)
			return Flux.empty();
		Duration pollInterval = getPollingInterval(waitForFunctionOptions);
		Flux<Integer> seedFlux = Flux.concat(Flux.just(0),
				Flux.just(0).repeat().delayElements(pollInterval, Scheduli.unlimited()));
		Flux<U> resultFlux = seedFlux.flatMap(nil -> Mono.fromCallable(untilTask));
		Duration timeout = getTimeout(waitForFunctionOptions).orElse(null);
		if (timeout != null)
			resultFlux = resultFlux.timeout(timeout, Mono.empty());
		return resultFlux;
	}

	private static Duration getPollingInterval(WaitForFunctionOptions waitForFunctionOptions) {
		if (waitForFunctionOptions == null || waitForFunctionOptions.pollingInterval == null)
			return POLL_INTERVAL_DEFAULT;
		return Duration.ofMillis(waitForFunctionOptions.pollingInterval.longValue());
	}

	private static Optional<Duration> getTimeout(WaitForFunctionOptions waitForFunctionOptions) {
		if (waitForFunctionOptions == null)
			return Optional.empty();
		if (waitForFunctionOptions.timeout == null)
			return Optional.of(TIMEOUT_DEFAULT);
		if (waitForFunctionOptions.timeout.equals(0d))
			return Optional.of(Durations.max());
		if (waitForFunctionOptions.timeout.equals(1d))
			return Optional.empty();
		return Optional.of(Duration.ofMillis(waitForFunctionOptions.timeout.longValue()));
	}

	private static WaitForFunctionOptions withTimeout(WaitForFunctionOptions waitForFunctionOptions, Duration timeout) {
		if (waitForFunctionOptions == null)
			waitForFunctionOptions = new WaitForFunctionOptions();
		if (timeout == null)
			return waitForFunctionOptions.setTimeout(1d);
		if (Durations.max().equals(timeout))
			return waitForFunctionOptions.setTimeout(0d);
		if (timeout.toMillis() >= Double.MAX_VALUE)
			return waitForFunctionOptions.setTimeout(0d);
		return waitForFunctionOptions.setTimeout(timeout.toMillis());
	}
}
