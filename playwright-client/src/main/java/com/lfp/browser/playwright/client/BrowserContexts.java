package com.lfp.browser.playwright.client;

import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;

import com.lfp.browser.hub.service.config.BrowserHubServiceConfig;
import com.lfp.browser.playwright.client.reflect.BrowserBundleSetup;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.ServiceConfig;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Semaphore;
import com.microsoft.playwright.Browser;
import com.microsoft.playwright.Browser.NewContextOptions;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.BrowserType.ConnectOptions;
import com.microsoft.playwright.BrowserType.LaunchOptions;
import com.microsoft.playwright.Playwright;

import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.StreamEx;

public class BrowserContexts {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final int VERSION = 0;
	private static final Semaphore LAUNCH_SEMAPHORE = !MachineConfig.isDeveloper() ? null
			: Semaphore.create(Utils.Machine.logicalProcessorCount(), true);
	private static final Map<String, String> ENVIRONMENT_VARIABLES;
	static {
		Map<String, String> envMap = new LinkedHashMap<>();
		{
			var tempDir = Utils.Files.tempDirectory(true);
			var folder = new File(tempDir,
					StreamEx.of(THIS_CLASS.getName(), VERSION, "cache", "ms-playwright").joining(File.separator));
			folder.mkdirs();
			envMap.put("PLAYWRIGHT_BROWSERS_PATH", folder.getAbsolutePath());
		}
		for (var ent : envMap.entrySet()) {
			Utils.Machine.setEnvironmentVariable(ent.getKey(), ent.getValue());
			System.setProperty(ent.getKey(), ent.getValue());
		}
		ENVIRONMENT_VARIABLES = envMap;
	}
	@SuppressWarnings("unused")
	private static final Nada INIT;
	static {
		BrowserBundleSetup.INSTANCE.get();
		INIT = Nada.get();
	}

	@SafeVarargs
	public static BrowserContextLFP connect(Consumer<NewContextOptions>... newContextOptionsModifiers) {
		return connect(null, newContextOptionsModifiers);
	}

	@SafeVarargs
	public static BrowserContextLFP connect(Proxy proxy, Consumer<NewContextOptions>... newContextOptionsModifiers) {
		var cfg = Configs.get(BrowserHubServiceConfig.class);
		return connect(cfg, proxy, newContextOptionsModifiers);
	}

	@SafeVarargs
	public static BrowserContextLFP connect(ServiceConfig serviceConfig, Proxy proxy,
			Consumer<NewContextOptions>... newContextOptionsModifiers) {
		URI uri;
		if (serviceConfig instanceof BrowserHubServiceConfig)
			uri = ((BrowserHubServiceConfig) serviceConfig).playwrightURI();
		else
			uri = serviceConfig.uri();
		return connect(uri, proxy, newContextOptionsModifiers);
	}

	@SafeVarargs
	public static BrowserContextLFP connect(URI uri, Proxy proxy,
			Consumer<NewContextOptions>... newContextOptionsModifiers) {
		String connectURL;
		{// build uri
			var urlb = UrlBuilder.fromUri(uri);
			urlb = urlb.withScheme(URIs.isSecure(uri) ? "wss" : "ws");
			var qp = urlb.queryParameters;
			urlb = urlb.withQuery((String) null);
			for (var ent : appendDefaultArgs(qp, false).entrySet()) {
				var key = ent.getKey();
				for (var value : ent.getValue()) {
					value = Optional.ofNullable(value).orElse("");
					urlb = urlb.addParameter(key, value);
				}
			}
			if (proxy != null) {
				var proxyServerURI = UrlBuilder.fromUri(proxy.toURI()).withUserInfo(null).toUri();
				urlb = urlb.addParameter("--proxy-server", proxyServerURI.toString());
			}
			connectURL = urlb.toString();
		}
		var browserContext = new AbstractBrowserContextLFP(proxy) {

			@Override
			protected Semaphore createSemaphore(Proxy proxy, String trackingId) {
				return null;
			}

			@Override
			protected Playwright createPlaywright(Proxy proxy, String trackingId) {
				return Playwright.create();
			}

			@Override
			protected Browser createBrowser(Proxy proxy, String trackingId, Playwright playwright) {
				var urlb = UrlBuilder.fromString(connectURL).addParameter("trackingId", trackingId);
				var connectOptions = new ConnectOptions();
				try {
					return playwright.chromium().connect(urlb.toString(), connectOptions);
				} catch (Throwable t) {
					var message = Utils.Strings.trimToNull(t.getMessage());
					if (message == null)
						message = "Unknown connect error";
					Utils.Exceptions.setMessage(t, message + " - " + uri);
					throw Utils.Exceptions.asRuntimeException(t);
				}
			}

			@Override
			protected BrowserContext createBrowserContext(Proxy proxy, String trackingId, Playwright playwright,
					Browser browser) {
				var cfg = Configs.get(BrowserHubServiceConfig.class);
				var newContextOptions = createNewContextOptions(cfg.browserWidth(), cfg.browserHeight(), proxy,
						newContextOptionsModifiers);
				var bc = browser.newContext(newContextOptions);
				return bc;
			}


		};
		return browserContext;
	}

	@SafeVarargs
	public static BrowserContextLFP launch(Consumer<NewContextOptions>... newContextOptionsModifiers) {
		return launch(null, newContextOptionsModifiers);
	}

	@SafeVarargs
	public static BrowserContextLFP launch(Proxy proxy, Consumer<NewContextOptions>... newContextOptionsModifiers) {
		return launch(proxy, null, newContextOptionsModifiers);
	}

	@SafeVarargs
	public static BrowserContextLFP launch(Proxy proxy, LaunchOptions launchOptions,
			Consumer<NewContextOptions>... newContextOptionsModifiers) {
		if (launchOptions == null) {
			launchOptions = new LaunchOptions();
			launchOptions.setHeadless(false);
		}
		launchOptions.setArgs(appendDefaultArgs(launchOptions.args, true));
		if (proxy != null) {
			var proxyURI = UrlBuilder.fromUri(proxy.toURI()).withUserInfo(null).toUri();
			var playwrightProxy = new com.microsoft.playwright.options.Proxy(proxyURI.toString());
			proxy.getUsername().ifPresent(playwrightProxy::setUsername);
			proxy.getPassword().ifPresent(playwrightProxy::setPassword);
			launchOptions.setProxy(playwrightProxy);
		}
		{
			var estream = Utils.Lots.stream(launchOptions.env);
			estream = estream.append(Utils.Lots.stream(ENVIRONMENT_VARIABLES));
			estream = estream.distinctKeys();
			launchOptions.setEnv(estream.toMap());
		}
		final LaunchOptions launchOptionsF = launchOptions;
		var browserContext = new AbstractBrowserContextLFP(proxy) {

			@Override
			protected Semaphore createSemaphore(Proxy proxy, String trackingId) {
				return LAUNCH_SEMAPHORE;
			}

			@Override
			protected Playwright createPlaywright(Proxy proxy, String trackingId) {
				return Playwright.create();
			}

			@Override
			protected Browser createBrowser(Proxy proxy, String trackingId, Playwright playwright) {
				return playwright.chromium().launch(launchOptionsF);
			}

			@Override
			protected BrowserContext createBrowserContext(Proxy proxy, String trackingId, Playwright playwright,
					Browser browser) {
				Entry<Integer, Integer> windowSize = getWindowSize(launchOptionsF);
				// do not use proxy here, handled in impl when launching
				var newContextOptions = createNewContextOptions(windowSize.getKey(), windowSize.getValue(), null,
						newContextOptionsModifiers);
				var bc = browser.newContext(newContextOptions);
				return bc;
			}

		};
		return browserContext;
	}

	private static List<String> appendDefaultArgs(List<String> args, boolean includeWindowSize) {
		var argMap = Utils.Lots.stream(args).mapToEntry(v -> {
			return Utils.Strings.substringBefore(v, "=");
		}, v -> {
			if (!Utils.Strings.contains(v, "="))
				return null;
			return Utils.Strings.substringAfter(v, "=");
		}).grouping();
		argMap = appendDefaultArgs(argMap, includeWindowSize);
		args = Utils.Lots.stream(argMap).flatMapValues(List::stream).map(ent -> {
			var arg = String.format("%s%s", ent.getKey(),
					Optional.ofNullable(ent.getValue()).map(v -> "=" + v).orElse(""));
			return arg;
		}).toList();
		return args;
	}

	private static Map<String, List<String>> appendDefaultArgs(Map<String, List<String>> argMap,
			boolean includeWindowSize) {
		argMap = Utils.Lots.stream(argMap).nonNullValues().toMap();
		for (var ent : Utils.Lots.stream(getDefaultArgs(includeWindowSize))) {
			var k = ent.getKey();
			var v = ent.getValue();
			if (argMap.containsKey(k))
				continue;
			argMap.put(k, Arrays.asList(v));
		}
		return argMap;
	}

	private static Map<String, String> getDefaultArgs(boolean includeWindowSize) {
		Map<String, String> argMap = new LinkedHashMap<>();
		if (includeWindowSize) {
			var cfg = Configs.get(BrowserHubServiceConfig.class);
			argMap.put("--window-size", String.format("%s,%s", cfg.browserWidth(), cfg.browserHeight()));
		}
		argMap.put("--disable-web-security", null);
		return argMap;
	}

	@SafeVarargs
	private static NewContextOptions createNewContextOptions(int width, int height, Proxy proxy,
			Consumer<NewContextOptions>... newContextOptionsModifiers) {
		var options = new NewContextOptions();
		options.setUserAgent(UserAgents.CHROME_LATEST.get());
		// options.setScreenSize(width, height);
		options.setViewportSize(width, height);
		options.setBypassCSP(true);
		options.setLocale(formatLocale(Locale.US));
		if (proxy != null && proxy.isAuthenticationEnabled())
			options.setHttpCredentials(proxy.getUsername().orElse(""), proxy.getPassword().orElse(""));
		Utils.Lots.stream(newContextOptionsModifiers).nonNull().forEach(v -> v.accept(options));
		return options;
	}

	private static Entry<Integer, Integer> getWindowSize(LaunchOptions launchOptions) {
		var argStream = Utils.Lots.stream(launchOptions == null ? null : launchOptions.args);
		argStream = argStream.filter(v -> Utils.Strings.startsWithIgnoreCase(v, "--window-size="));
		for (var arg : argStream) {
			String post = Utils.Strings.substringAfter(arg, "=");
			var chunks = post.split(Pattern.quote(","));
			if (chunks.length != 2)
				continue;
			Function<Integer, Integer> getter = i -> {
				if (i < 0 || chunks.length <= i)
					return null;
				return Utils.Strings.parseNumber(chunks[i]).map(Number::intValue).filter(v -> v > 0).orElse(null);
			};
			var width = getter.apply(0);
			var height = getter.apply(1);
			if (width != null && height != null)
				return Utils.Lots.entry(width, height);
		}
		var cfg = Configs.get(BrowserHubServiceConfig.class);
		return Utils.Lots.entry(cfg.browserWidth(), cfg.browserHeight());
	}

	private static String formatLocale(Locale locale) {
		if (locale == null)
			return null;
		var result = String.format("%s,%s", Utils.Strings.replace(locale.toString(), "_", "-"), locale.getLanguage());
		return result;
	}

}
