package com.lfp.browser.playwright.client;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import com.lfp.joe.core.function.Muto;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.net.proxy.Proxy;

public class BrowserContextSupplier extends Scrapable.Impl implements Supplier<BrowserContextLFP> {
	private final Muto<BrowserContextLFP> browserContextMuto = Muto.create();
	private final Supplier<Proxy> proxySupplier;
	private final boolean launch;

	public BrowserContextSupplier(Supplier<Proxy> proxySupplier) {
		this(proxySupplier, false);
	}

	public BrowserContextSupplier(Supplier<Proxy> proxySupplier, boolean launch) {
		super();
		this.proxySupplier = Optional.ofNullable(proxySupplier).orElse(() -> null);
		this.launch = launch;
	}

	@Override
	public BrowserContextLFP get() {
		return browserContextMuto.updateAndGet(bc -> {
			return create();
		}, v -> {
			return v == null;
		});
	}

	public boolean reset() {
		var bc = browserContextMuto.getAndUpdate(v -> {
			var closeTask = new CloseTask(v);
			this.tryRemove(closeTask);
			closeTask.run();
			return null;
		}, v -> {
			return v != null;
		});
		return bc != null;
	}

	protected BrowserContextLFP create() {
		var proxy = this.proxySupplier.get();
		var bc = create(proxy, this.launch);
		if (bc != null)
			this.onScrap(new CloseTask(bc));
		return bc;
	}

	protected BrowserContextLFP create(Proxy proxy, boolean launch) {
		var bc = launch ? BrowserContexts.launch(proxy) : BrowserContexts.connect(proxy);
		return bc;
	}

	private static class CloseTask implements Runnable {

		private final BrowserContextLFP browserContext;

		public CloseTask(BrowserContextLFP browserContext) {
			super();
			this.browserContext = Objects.requireNonNull(browserContext);
		}

		@Override
		public void run() {
			browserContext.close();

		}

		@Override
		public int hashCode() {
			return Objects.hash(browserContext);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CloseTask other = (CloseTask) obj;
			return Objects.equals(browserContext, other.browserContext);
		}
	}
}
