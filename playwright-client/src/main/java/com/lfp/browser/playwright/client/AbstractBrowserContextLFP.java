package com.lfp.browser.playwright.client;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.slf4j.Logger;

import com.lfp.browser.script.stealth.StealthScript;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Semaphore;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.Request;
import com.microsoft.playwright.Response;
import com.microsoft.playwright.Route;
import com.microsoft.playwright.Tracing;
import com.microsoft.playwright.options.BindingCallback;
import com.microsoft.playwright.options.Cookie;
import com.microsoft.playwright.options.FunctionCallback;
import com.microsoft.playwright.options.Geolocation;

import ch.qos.logback.classic.Level;

public abstract class AbstractBrowserContextLFP implements BrowserContextLFP {

	private static final String UTZ_SCRIPT = "utz.js";
	private final org.slf4j.Logger logger = creatLogger(this.getClass());
	private final String trackingId = "pw" + Utils.Crypto.getRandomString();
	private final List<AutoCloseable> autoCloseables = Collections.synchronizedList(new ArrayList<>());
	private final Proxy proxy;
	private final BrowserContext browserContextDelegate;

	public AbstractBrowserContextLFP(Proxy proxy) {
		this.proxy = proxy;
		if (MachineConfig.isDeveloper())
			logger.info("browser context requested:{} proxy:{}", trackingId, Optional.ofNullable(proxy).orElse(null));
		this.browserContextDelegate = createBrowserContextDelegate();
		if (MachineConfig.isDeveloper())
			this.onClose(nil -> {
				logger.info("browser context closed:{} proxy:{}", trackingId, Optional.ofNullable(proxy).orElse(null));
			});
	}

	protected BrowserContext createBrowserContextDelegate() {
		var semaphore = createSemaphore(proxy, trackingId);
		if (semaphore != null) {
			Utils.Functions.unchecked(() -> semaphore.acquire());
			this.autoCloseables.add(semaphore::release);
		}
		BrowserContext browserContext = null;
		try {
			var playwright = this.createPlaywright(proxy, trackingId);
			this.autoCloseables.add(0, playwright);
			var browser = this.createBrowser(proxy, trackingId, playwright);
			{// onDisconnected close
				var browserClosed = new AtomicBoolean();
				this.autoCloseables.add(0, () -> {
					if (browserClosed.compareAndSet(false, true))
						browser.close();
				});
				browser.onDisconnected(nil -> {
					if (browserClosed.compareAndSet(false, true))
						this.close();
				});
			}
			browserContext = this.createBrowserContext(proxy, trackingId, playwright, browser);
			browserContext.addInitScript(StealthScript.INSTANCE.get());
			browserContext.addInitScript(Utils.Resources.getResourceAsString(UTZ_SCRIPT).get());
			var initLogged = new AtomicBoolean();
			browserContext.onPage(p -> {
				if (initLogged.compareAndSet(false, true))
					logger.info("browser context started:{} proxy:{}", trackingId,
							Optional.ofNullable(proxy).orElse(null));
			});
		} catch (Throwable t) {
			var toClose = Utils.Lots.stream(this.autoCloseables).prepend(browserContext);
			Utils.Exceptions.closeQuietly(Level.WARN, toClose);
			throw Utils.Exceptions.asRuntimeException(t);
		}
		return browserContext;
	}

	@Override
	public Optional<Proxy> proxy() {
		return Optional.ofNullable(this.proxy);
	}

	@Override
	public String trackingId() {
		return this.trackingId;
	}

	protected abstract Semaphore createSemaphore(Proxy proxy, String trackingId);

	protected abstract Playwright createPlaywright(Proxy proxy, String trackingId);

	protected abstract Browser createBrowser(Proxy proxy, String trackingId, Playwright playwright);

	protected abstract BrowserContext createBrowserContext(Proxy proxy, String trackingId, Playwright playwright,
			Browser browser);

	private static Logger creatLogger(Class<?> classType) {
		classType = CoreReflections.getNamedClassType(classType);
		if (AbstractBrowserContextLFP.class.equals(classType))
			classType = BrowserContextLFP.class;
		return Logging.logger(classType);
	}

	// delegtes

	@Override
	public void close() {
		try {
			this.browserContextDelegate.close();
		} finally {
			while (!this.autoCloseables.isEmpty()) {
				Utils.Exceptions.closeQuietly(Level.WARN, this.autoCloseables.remove(0));
			}
		}
	}

	@Override
	public void onClose(Consumer<BrowserContext> handler) {
		this.browserContextDelegate.onClose(handler);
	}

	@Override
	public void offClose(Consumer<BrowserContext> handler) {
		this.browserContextDelegate.offClose(handler);
	}

	@Override
	public void onPage(Consumer<Page> handler) {
		browserContextDelegate.onPage(handler);
	}

	@Override
	public void offPage(Consumer<Page> handler) {
		browserContextDelegate.offPage(handler);
	}

	@Override
	public void addCookies(List<Cookie> cookies) {
		browserContextDelegate.addCookies(cookies);
	}

	@Override
	public void addInitScript(String script) {
		browserContextDelegate.addInitScript(script);
	}

	@Override
	public void addInitScript(Path script) {
		browserContextDelegate.addInitScript(script);
	}

	@Override
	public Browser browser() {
		return browserContextDelegate.browser();
	}

	@Override
	public void clearCookies() {
		browserContextDelegate.clearCookies();
	}

	@Override
	public void clearPermissions() {
		browserContextDelegate.clearPermissions();
	}

	@Override
	public List<Cookie> cookies(String urls) {
		return browserContextDelegate.cookies(urls);
	}

	@Override
	public List<Cookie> cookies(List<String> urls) {
		return browserContextDelegate.cookies(urls);
	}

	@Override
	public void exposeBinding(String name, BindingCallback callback, ExposeBindingOptions options) {
		browserContextDelegate.exposeBinding(name, callback, options);
	}

	@Override
	public void exposeFunction(String name, FunctionCallback callback) {
		browserContextDelegate.exposeFunction(name, callback);
	}

	@Override
	public void grantPermissions(List<String> permissions, GrantPermissionsOptions options) {
		browserContextDelegate.grantPermissions(permissions, options);
	}

	@Override
	public Page newPage() {
		return browserContextDelegate.newPage();
	}

	@Override
	public List<Page> pages() {
		return browserContextDelegate.pages();
	}

	@Override
	public void route(String url, Consumer<Route> handler) {
		browserContextDelegate.route(url, handler);
	}

	@Override
	public void route(Pattern url, Consumer<Route> handler) {
		browserContextDelegate.route(url, handler);
	}

	@Override
	public void route(Predicate<String> url, Consumer<Route> handler) {
		browserContextDelegate.route(url, handler);
	}

	@Override
	public void setDefaultNavigationTimeout(double timeout) {
		browserContextDelegate.setDefaultNavigationTimeout(timeout);
	}

	@Override
	public void setDefaultTimeout(double timeout) {
		browserContextDelegate.setDefaultTimeout(timeout);
	}

	@Override
	public void setExtraHTTPHeaders(Map<String, String> headers) {
		browserContextDelegate.setExtraHTTPHeaders(headers);
	}

	@Override
	public void setGeolocation(Geolocation geolocation) {
		browserContextDelegate.setGeolocation(geolocation);
	}

	@Override
	public void setOffline(boolean offline) {
		browserContextDelegate.setOffline(offline);
	}

	@Override
	public String storageState(StorageStateOptions options) {
		return browserContextDelegate.storageState(options);
	}

	@Override
	public void unroute(String url, Consumer<Route> handler) {
		browserContextDelegate.unroute(url, handler);
	}

	@Override
	public void unroute(Pattern url, Consumer<Route> handler) {
		browserContextDelegate.unroute(url, handler);
	}

	@Override
	public void unroute(Predicate<String> url, Consumer<Route> handler) {
		browserContextDelegate.unroute(url, handler);
	}

	@Override
	public Page waitForPage(WaitForPageOptions options, Runnable callback) {
		return browserContextDelegate.waitForPage(options, callback);
	}

	@Override
	public List<Cookie> cookies() {
		return browserContextDelegate.cookies();
	}

	@Override
	public void exposeBinding(String name, BindingCallback callback) {
		browserContextDelegate.exposeBinding(name, callback);
	}

	@Override
	public void grantPermissions(List<String> permissions) {
		browserContextDelegate.grantPermissions(permissions);
	}

	@Override
	public String storageState() {
		return browserContextDelegate.storageState();
	}

	@Override
	public void unroute(String url) {
		browserContextDelegate.unroute(url);
	}

	@Override
	public void unroute(Pattern url) {
		browserContextDelegate.unroute(url);
	}

	@Override
	public void unroute(Predicate<String> url) {
		browserContextDelegate.unroute(url);
	}

	@Override
	public Page waitForPage(Runnable callback) {
		return browserContextDelegate.waitForPage(callback);
	}

	@Override
	public void onRequest(Consumer<Request> handler) {
		browserContextDelegate.onRequest(handler);
	}

	@Override
	public void offRequest(Consumer<Request> handler) {
		browserContextDelegate.offRequest(handler);
	}

	@Override
	public void onRequestFailed(Consumer<Request> handler) {
		browserContextDelegate.onRequestFailed(handler);
	}

	@Override
	public void offRequestFailed(Consumer<Request> handler) {
		browserContextDelegate.offRequestFailed(handler);
	}

	@Override
	public void onRequestFinished(Consumer<Request> handler) {
		browserContextDelegate.onRequestFinished(handler);
	}

	@Override
	public void offRequestFinished(Consumer<Request> handler) {
		browserContextDelegate.offRequestFinished(handler);
	}

	@Override
	public void onResponse(Consumer<Response> handler) {
		browserContextDelegate.onResponse(handler);
	}

	@Override
	public void offResponse(Consumer<Response> handler) {
		browserContextDelegate.offResponse(handler);
	}

	@Override
	public Tracing tracing() {
		return browserContextDelegate.tracing();
	}

	@Override
	public void route(String url, Consumer<Route> handler, RouteOptions options) {
		browserContextDelegate.route(url, handler, options);
	}

	@Override
	public void route(Pattern url, Consumer<Route> handler, RouteOptions options) {
		browserContextDelegate.route(url, handler, options);
	}

	@Override
	public void route(Predicate<String> url, Consumer<Route> handler, RouteOptions options) {
		browserContextDelegate.route(url, handler, options);
	}

	@Override
	public APIRequestContext request() {
		return browserContextDelegate.request();
	}

	@Override
	public void routeFromHAR(Path har, RouteFromHAROptions options) {
		browserContextDelegate.routeFromHAR(har, options);
	}

}
