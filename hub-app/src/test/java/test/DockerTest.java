package test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.TaskState;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.okhttp.OkDockerHttpClient;
import com.github.dockerjava.transport.DockerHttpClient;
import com.lfp.joe.utils.Utils;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;

import one.util.streamex.StreamEx;

public class DockerTest {

	public static void main(String[] args)
			throws DockerException, InterruptedException, DockerCertificateException, IOException {
		DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
				.withDockerHost("tcp://localhost:2377").build();
		DockerHttpClient httpClient = new OkDockerHttpClient.Builder().dockerHost(config.getDockerHost())
				.sslConfig(config.getSSLConfig()).build();
		DockerClient dockerClient = DockerClientImpl.getInstance(config, httpClient);
		var services = dockerClient.listServicesCmd().exec();
		services = Utils.Lots.stream(services).filter(v -> {
			return Utils.Strings.containsIgnoreCase(v.getSpec().getName(), "browserless");
		}).toList();
		for (var service : services) {
			// new ListTasksCmdExec(null, config);

			var cmd = dockerClient.listTasksCmd();
			var tasks = cmd.withServiceFilter(service.getSpec().getName()).withStateFilter(TaskState.RUNNING).exec();
			for (var task : tasks) {
				var valueStream = getUnrecognizedField(task.getUnrecognizedFields(), "NetworksAttachments", -1,
						"Addresses", -1);
				System.out.println(valueStream.toList());
				// System.out.println(Serials.Gsons.getPretty().toJson(task));
			}

		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static StreamEx<Object> getUnrecognizedField(Object input, Object... paths) {
		if (input == null)
			return StreamEx.empty();
		var pathIter = Utils.Lots.stream(paths).iterator();
		if (!pathIter.hasNext())
			return StreamEx.of(input);
		var firstPath = pathIter.next();
		var nextPaths = Utils.Lots.stream(pathIter).toArray(Object.class);
		if (firstPath instanceof Number) {
			var index = ((Number) firstPath).intValue();
			var stream = Utils.Lots.stream(validateInput(input, Iterable.class, paths));
			if (index != -1) {
				var nextInput = stream.skip(index).findFirst().orElse(null);
				return getUnrecognizedField(nextInput, nextPaths);
			}
			var streams = stream.map(nextInput -> {
				return getUnrecognizedField(nextInput, nextPaths);
			});
			return Utils.Lots.flatMap(streams);
		} else if (firstPath instanceof String) {
			var nextInput = validateInput(input, Map.class, paths).get(firstPath);
			return getUnrecognizedField(nextInput, nextPaths);
		} else
			throw new IllegalArgumentException("invalid path:" + firstPath);
	}

	@SuppressWarnings("unchecked")
	private static <X> X validateInput(Object input, Class<X> classType, Object... paths) {
		if (classType.isAssignableFrom(input.getClass()))
			return (X) input;
		throw new IllegalArgumentException(
				String.format("path lookup failed, %s required. paths:%s inputType:%s input:%s", classType.getName(),
						Arrays.asList(paths), input.getClass().getName(), input));

	}
}
