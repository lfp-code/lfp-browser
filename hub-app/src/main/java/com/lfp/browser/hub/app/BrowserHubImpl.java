package com.lfp.browser.hub.app;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import org.threadly.concurrent.future.ListenableFuture;

import com.github.benmanes.caffeine.cache.Cache;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;
import com.lfp.browser.hub.service.BrowserHubService;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.handler.ThreadHttpHandler;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;
import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import okhttp3.Request;
import okhttp3.Response;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class BrowserHubImpl implements BrowserHubService, HttpHandler, io.undertow.predicate.Predicate {
	private static final Duration CACHE_DURATION = Duration.ofMillis(500);
	private final ThreadHttpHandler threadHttpHandler = new ThreadHttpHandler(hsx -> handleRequestInternal(hsx));
	private final BrowserNodeImpl browserNodeImpl;
	private final Cache<URI, ListenableFuture<JsonElement>> cache;

	public BrowserHubImpl(BrowserNodeImpl browserNodeImpl) {
		this.browserNodeImpl = Objects.requireNonNull(browserNodeImpl);
		this.cache = Caches.newCaffeineBuilder(-1, CACHE_DURATION, null).build();
	}

	@Override
	public boolean resolve(HttpServerExchange value) {
		if (!"get".equalsIgnoreCase(value.getRequestMethod().toString()))
			return false;
		var requestURI = UndertowUtils.getRequestURI(value);
		return URIs.pathPrefixMatch(requestURI.getPath(), BrowserHubService.PATH);
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		this.threadHttpHandler.handleRequest(exchange);
	}

	protected void handleRequestInternal(HttpServerExchange exchange) throws Exception {
		var requestURI = UndertowUtils.getRequestURI(exchange);
		var estream = streamResponses(requestURI, null).filterValues(v -> {
			if (v.isJsonNull())
				return false;
			if (v.isJsonArray() && v.getAsJsonArray().size() == 0)
				return false;
			if (v.isJsonObject() && v.getAsJsonObject().size() == 0)
				return false;
			return true;
		});
		estream = estream.sortedBy(ent -> ent.getKey().toString());
		var result = estream.toCustomMap(LinkedHashMap::new);
		var resultBytes = Serials.Gsons.toBytes(result);
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, com.google.common.net.MediaType.JSON_UTF_8.toString());
		exchange.getResponseHeaders().put(Headers.CONTENT_LENGTH, resultBytes.length());
		exchange.getResponseSender().send(resultBytes.buffer());
	}

	private EntryStream<URI, JsonElement> streamResponses(URI requestURI, Predicate<URI> uriFilter) {
		var nodeURIStreams = this.browserNodeImpl.streamURIs();
		if (uriFilter != null)
			nodeURIStreams = nodeURIStreams.filter(uriFilter);
		var pool = Threads.Pools.centralPool().limitCoreMultiplier(4);
		StreamEx<ListenableFuture<Entry<URI, JsonElement>>> futureStream = nodeURIStreams.map(v -> {
			var nodeRequestURI = toNodeRequestURI(requestURI, v);
			var future = this.cache.get(v, nil -> executeAsync(pool, nodeRequestURI));
			return future.map(je -> Utils.Lots.entry(v, mapResult(v, je)));
		});
		return Threads.Futures.completeStream(futureStream, true).chain(EntryStreams::of);
	}

	private URI toNodeRequestURI(URI requestURI, URI nodeURI) {
		var path = URIs.normalizePath(requestURI.getPath());
		if (Utils.Strings.indexOf(path, BrowserHubService.PATH) == 0) {
			path = path.substring(BrowserHubService.PATH.length());
			path = URIs.normalizePath(path);
		}
		var urlb = UrlBuilder.fromUri(requestURI).withScheme(nodeURI.getScheme()).withHost(nodeURI.getHost())
				.withPort(Optional.ofNullable(nodeURI.getPort()).filter(v -> v >= 0).orElse(null)).withPath(path);
		return urlb.toUri();
	}

	private static JsonElement mapResult(URI nodeURI, JsonElement je) {
		if (je == null || je.isJsonNull())
			return je;
		if (je.isJsonArray()) {
			var jarr = je.getAsJsonArray();
			for (int i = 0; i < jarr.size(); i++)
				jarr.set(i, mapResult(nodeURI, jarr.get(i)));
			return jarr;
		}
		if (!je.isJsonObject())
			return je;
		var jo = je.getAsJsonObject();
		for (var key : List.copyOf(jo.keySet())) {
			var value = jo.remove(key);
			jo.add(key, mapResult(nodeURI, key, value));
		}
		return jo;
	}

	private static JsonElement mapResult(URI nodeURI, String key, JsonElement value) {
		String valueStr;
		{
			if (value == null || value.isJsonNull() || !value.isJsonPrimitive())
				return mapResult(nodeURI, value);
			var prim = value.getAsJsonPrimitive();
			if (!prim.isString())
				return mapResult(nodeURI, value);
			valueStr = prim.getAsString();
			if (Utils.Strings.isBlank(valueStr))
				return mapResult(nodeURI, value);
		}
		boolean adapt = false;
		for (var check : List.of("Url", "Endpoint"))
			if (Utils.Strings.endsWith(key, check)) {
				adapt = true;
				break;
			}
		if (!adapt)
			return mapResult(nodeURI, value);
		var urlb = BrowserNodeURIAdapter.INSTANCE.apply(nodeURI, URI.create(valueStr));
		return new JsonPrimitive(urlb.toString());
	}

	private static ListenableFuture<JsonElement> executeAsync(SubmitterSchedulerLFP pool, URI uri) {
		ListenableFuture<JsonElement> future = pool.submit(() -> {
			var rb = new Request.Builder().url(uri.toString());
			JsonElement body;
			try (var response = Ok.Clients.get().newCall(rb.build()).execute();) {
				body = parseResponse(response);
			}
			return body;
		});
		Threads.Futures.logFailureError(future, true, "error during api call:{}", uri);
		return future;
	}

	private static JsonElement parseResponse(Response response) throws IOException {
		Ok.Calls.validateStatusCode(response, 2, 404);
		if (!Ok.Calls.contentTypeMatches(response, null, "json", null))
			return JsonNull.INSTANCE;
		if (response.body() == null || response.body().contentLength() == 0)
			return JsonNull.INSTANCE;
		return Serials.Gsons.fromStream(response.body().byteStream(), JsonElement.class);
	}

}
