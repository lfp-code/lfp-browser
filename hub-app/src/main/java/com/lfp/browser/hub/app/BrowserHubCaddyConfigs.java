package com.lfp.browser.hub.app;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.URI;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.lfp.browser.hub.app.config.BrowserHubAppConfig;
import com.lfp.browser.hub.service.BrowserHubService;
import com.lfp.browser.hub.service.config.BrowserHubServiceConfig;
import com.lfp.caddy.core.CaddyConfig;
import com.lfp.caddy.core.CaddyConfigs;
import com.lfp.caddy.core.config.Apps;
import com.lfp.caddy.core.config.AutomaticHttps;
import com.lfp.caddy.core.config.Backend;
import com.lfp.caddy.core.config.ClaimFilter;
import com.lfp.caddy.core.config.Failure;
import com.lfp.caddy.core.config.Handle;
import com.lfp.caddy.core.config.Http;
import com.lfp.caddy.core.config.LoadBalancing;
import com.lfp.caddy.core.config.MatchHttp;
import com.lfp.caddy.core.config.Providers;
import com.lfp.caddy.core.config.Reauth;
import com.lfp.caddy.core.config.Route;
import com.lfp.caddy.core.config.SelectionPolicy;
import com.lfp.caddy.core.config.Server;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;

import io.undertow.Undertow;

public class BrowserHubCaddyConfigs {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static CaddyConfig createConfig(File caddyExec, int adminPort, Undertow browserHubServer,
			Iterable<URI> upstreamURIs) {
		upstreamURIs = Utils.Lots.asCollection(Utils.Lots.stream(upstreamURIs).nonNull().map(URIs::normalize).distinct());
		var caddyConfig = CaddyConfigs.getDefault(caddyExec, adminPort);
		List<Route> routes = new ArrayList<>();
		routes.addAll(browserNodeDirectRoutes(upstreamURIs));
		routes.add(browserHubServerRoute(browserHubServer));
		routes.add(browserNodeRouteWebDriver(upstreamURIs));
		routes.add(browserNodeRoute(upstreamURIs));
		routes.removeIf(v -> v == null);
		var serverB = Server.builder().listen(String.format("0.0.0.0:%s", getPublicPort())).routes(routes);
		if (MachineConfig.isDeveloper()) {
			caddyConfig = CaddyConfigs.withTlsIssuerInternal(caddyConfig);
			serverB.automaticHttps(AutomaticHttps.builder().disable(true).build());
		}
		caddyConfig = JodaBeans.set(caddyConfig, Map.of("server_0", serverB.build()), CaddyConfig.meta().apps(),
				Apps.meta().http(), Http.meta().servers());
		return caddyConfig;
	}

	private static Route browserHubServerRoute(Undertow browserHubServer) {
		var addressStream = Utils.Lots.stream(browserHubServer.getListenerInfo()).map(v -> v.getAddress())
				.select(InetSocketAddress.class);
		var handle = CaddyConfigs.toReverseProxyHandler(addressStream.toArray(InetSocketAddress.class));
		MatchHttp match = MatchHttp.builder().host(getPublicHost()).path(BrowserHubService.PATH + "/*").build();
		return Route.builder().match(match).handle(authenticationHandle(), handle).build();
	}

	private static List<Route> browserNodeDirectRoutes(Iterable<URI> upstreamURIs) {
		if (!upstreamURIs.iterator().hasNext())
			return List.of();
		return Utils.Lots.stream(upstreamURIs).map(v -> {
			var path = BrowserHubCaddyConfigs.nodeURIToPath(v);
			var match = MatchHttp.builder().host(getPublicHost()).path(path).build();
			var matchWilcard = MatchHttp.builder().host(getPublicHost()).path(path + "/*").build();
			var reverseProxyHandle = toReverseProxyHandle(List.of(v));
			var rewriteHandle = Handle.builder().handler("rewrite").stripPathPrefix(path).build();
			var handlers = Utils.Lots.stream(authenticationHandle(), rewriteHandle, reverseProxyHandle).nonNull()
					.toList();
			return Route.builder().match(match, matchWilcard).handle(handlers).build();
		}).toList();
	}

	private static Route browserNodeRoute(Iterable<URI> nodeURIs) {
		if (!nodeURIs.iterator().hasNext())
			return null;
		var matchB = MatchHttp.builder().host(getPublicHost());
		var reverseProxyHandle = toReverseProxyHandle(nodeURIs);
		var handlers = Utils.Lots.stream(authenticationHandle(), reverseProxyHandle).nonNull().toList();
		return Route.builder().match(matchB.build()).handle(handlers).build();
	}

	private static Route browserNodeRouteWebDriver(Iterable<URI> nodeURIs) {
		if (!nodeURIs.iterator().hasNext())
			return null;
		var matchB = MatchHttp.builder().host(getPublicHost())
				.header(Map.of(Headers.USER_AGENT, List.of(String.format("*%s*", "selenium"))));
		var rewriteHandle = Handle.builder().handler("rewrite").uri("/webdriver{http.request.uri.path}").build();
		var reverseProxyHandle = toReverseProxyHandle(nodeURIs);
		var selectionPolicy = SelectionPolicy.builder().policy("cookie").name("browserless-node-webdriver").build();
		var loadBalancing = LoadBalancing.builder().selectionPolicy(selectionPolicy).build();
		reverseProxyHandle = reverseProxyHandle.toBuilder().loadBalancing(loadBalancing).build();
		var handlers = Utils.Lots.stream(authenticationHandle(), rewriteHandle, reverseProxyHandle).nonNull().toList();
		return Route.builder().match(matchB.build()).handle(handlers).build();
	}

	private static Handle toReverseProxyHandle(Iterable<URI> nodeURIs) {
		HeaderMap setHeaders = HeaderMap.of();
		return CaddyConfigs.toReverseProxyHandler(nodeURIs, null, setHeaders, List.of(Headers.USER_AGENT));
	}

	private static Handle authenticationHandle() {
		var cfg = Configs.get(BrowserHubAppConfig.class);
		var authorizedIssuersRegexps = Utils.Lots.stream(cfg.authorizedIssuersRegexps())
				.filter(Utils.Strings::isNotBlank).distinct().toList();
		if (authorizedIssuersRegexps.isEmpty())
			return null;
		Backend backend;
		{
			var backendB = Backend.builder().type("jwk").tokenSources("cookie", "header", "query")
					.jwkCacheDuration(Duration.ofSeconds(3).toMillis())
					.authorizedIssuersRegexp(authorizedIssuersRegexps);
			var groupIdsClaimValue = Utils.Lots.stream(cfg.groupIdsClaimValue()).filter(Utils.Strings::isNotBlank)
					.distinct().toList();
			if (!groupIdsClaimValue.isEmpty())
				backendB.claimFilter(
						ClaimFilter.builder().name(cfg.groupIdsClaimName()).value(groupIdsClaimValue).build());
			backend = backendB.build();
		}
		Failure failure;
		{
			failure = Failure.builder().mode("status").code(StatusCodes.UNAUTHORIZED).build();
		}
		var reauth = Reauth.builder().backends(backend).failure(failure).build();
		var authHandler = Handle.builder().handler("authentication")
				.providers(Providers.builder().reauth(reauth).build()).build();
		return authHandler;
	}

	public static String nodeURIToNodeId(URI nodeURI) {
		nodeURI = URIs.normalize(nodeURI);
		var encoded = Base58.encode(nodeURI.toString().getBytes(Utils.Bits.getDefaultCharset()));
		encoded = encoded.toLowerCase();
		return encoded;
	}

	public static String nodeURIToPath(URI nodeURI) {
		return BrowserHubService.PATH + "/" + nodeURIToNodeId(nodeURI);
	}

	public static URI pathToNodeURI(String path) {
		path = URIs.normalizePath(path);
		if (Utils.Strings.startsWith(path, BrowserHubService.PATH))
			path = path.substring(BrowserHubService.PATH.length());
		path = Utils.Strings.stripStart(path, "/");
		path = Utils.Strings.substringBefore(path, "/");
		if (Utils.Strings.isBlank(path))
			return null;
		var url = Utils.Functions.catching(path,
				v -> Base58.decodeToBytes(v).encodeCharset(Utils.Bits.getDefaultCharset()), t -> null);
		return URIs.parse(url).orElse(null);
	}

	public static String getPublicHost() {
		var cfg = Configs.get(BrowserHubServiceConfig.class);
		String host = cfg.uri().getHost();
		return host;
	}

	public static int getPublicPort() {
		int port = URIs.getPort(Configs.get(BrowserHubServiceConfig.class).uri());
		if (port == -1)
			port = 443;
		return port;
	}

}
