package com.lfp.browser.hub.app.config;

import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

public interface BrowserHubAppConfig extends Config {

	@DefaultValue("groupIds")
	String groupIdsClaimName();

	List<String> groupIdsClaimValue();

	List<String> authorizedIssuersRegexps();

	@DefaultValue("github.com/regbo/caddy2-reauth")
	List<String> caddyModules();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.jsonBuilder().withSkipPopulated(false).build());
		Configs.printProperties(PrintOptions.properties());
	}

}
