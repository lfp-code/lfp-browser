package com.lfp.browser.hub.app;

import java.net.URI;
import java.util.Map.Entry;
import java.util.function.BiFunction;

import com.lfp.browser.hub.service.config.BrowserHubServiceConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;

public enum BrowserNodeURIAdapter implements BiFunction<URI, Object, UrlBuilder> {
	INSTANCE;

	private static final String SCHEME_SPLIT = "://";

	@Override
	public UrlBuilder apply(URI nodeURI, Object input) {
		if (input == null)
			return null;
		URI inputURI;
		{
			if (input instanceof URI)
				inputURI = (URI) input;
			else if (input instanceof String)
				inputURI = URIs.parse((String) input).orElse(null);
			else
				throw new IllegalArgumentException(String.format("could not parse uri. type:%s value:%s",
						input.getClass().getNestMembers(), input.toString()));
			if (inputURI == null)
				return null;
		}
		var urlb = apply(nodeURI, inputURI);
		return urlb;
	}

	private UrlBuilder apply(URI nodeURI, URI inputURI) {
		String path;
		{
			path = BrowserHubCaddyConfigs.nodeURIToPath(nodeURI);
			var inputPath = URIs.normalizePath(inputURI.getPath());
			if (!"/".equals(inputPath))
				path += inputPath;
		}
		var serviceURI = Configs.get(BrowserHubServiceConfig.class).uri();
		String scheme;
		{
			if (Utils.Strings.startsWithIgnoreCase(inputURI.getScheme(), "ws"))
				scheme = URIs.isSecure(serviceURI) ? "wss" : "ws";
			else
				scheme = URIs.isSecure(serviceURI) ? "https" : "http";
		}
		var urlb = UrlBuilder.fromUri(serviceURI).withScheme(scheme).withPath(path).withQuery(inputURI.getQuery())
				.withFragment(inputURI.getFragment());
		urlb = adaptQueryParams(nodeURI, urlb);
		return urlb;
	}

	private UrlBuilder adaptQueryParams(URI nodeURI, UrlBuilder urlb) {
		var estream = Utils.Lots.streamMultimap(urlb.queryParameters);
		estream = estream.filterKeys(v -> {
			if ("ws".equals(v) || "wss".equals(v))
				return true;
			return false;
		});
		estream = estream.filterValues(Utils.Strings::isNotBlank);
		estream = estream.map(ent -> {
			var scheme = ent.getKey();
			var url = ent.getValue();
			var uri = URIs.parse(url).orElse(null);
			if (uri == null)
				uri = URIs.parse(scheme + SCHEME_SPLIT + url).orElse(null);
			if (uri == null)
				return null;
			var adaptedURI = apply(nodeURI, uri);
			Entry<String, String> updated = Utils.Lots.entry(ent.getKey(), adaptedURI.toString());
			return updated;
		}).nonNull().mapToEntry(Entry::getKey, Entry::getValue);
		var qvMap = estream.grouping();
		for (var ent : qvMap.entrySet()) {
			urlb = urlb.removeParameters(ent.getKey());
			for (var value : ent.getValue()) {
				int splitAt = value.indexOf(SCHEME_SPLIT);
				var newKey = value.substring(0, splitAt);
				var newValue = value.substring(splitAt + SCHEME_SPLIT.length());
				urlb = urlb.addParameter(newKey, newValue);
			}
		}
		return urlb;
	}

}
