package com.lfp.browser.hub.app;

import java.net.URI;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.reactivestreams.Publisher;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.events.bobbus.BobBus;
import com.lfp.joe.reactor.Fluxi;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.threads.ImmutableFutures.ScheduleWhileRequest;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;
import reactor.core.publisher.Mono;

public class BrowserNodeImpl extends Scrapable.Impl {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Duration UPDATE_INTERVAL = Duration.ofSeconds(3);
	private final Map<String, URI> registrationMap = new ConcurrentHashMap<>();
	private final List<Entry<Boolean, URI>> modificationList = new ArrayList<>();
	private final BobBus<Map<Boolean, List<URI>>> updateBus = BobBus.create();

	public BrowserNodeImpl() {
		var future = ScheduleWhileRequest.builder(UPDATE_INTERVAL, () -> {
			checkForModification();
		}, () -> !this.isScrapped()).build().schedule();
		var listener = this.onScrap(() -> future.cancel(true));
		future.listener(() -> listener.scrap());
	}

	private void checkForModification() {
		List<Entry<Boolean, URI>> modificationListCopy;
		synchronized (modificationList) {
			modificationListCopy = List.copyOf(modificationList);
			modificationList.clear();
		}
		if (modificationListCopy.isEmpty())
			return;
		Map<Boolean, List<URI>> modificationMap = Utils.Lots.streamEntries(modificationListCopy).distinct().grouping();
		for (var added : List.of(true, false)) {
			var uris = modificationMap.remove(added);
			if (uris == null || uris.isEmpty())
				continue;
			uris = Utils.Lots.unmodifiable(uris);
			modificationMap.put(added, uris);
			logger.info("node uris {}:{}", added ? "added" : "removed", uris);
		}
		updateBus.publish(Utils.Lots.unmodifiable(modificationMap));
	}

	public Mono<Void> register(Publisher<URI> publisher) {
		String id = Utils.Crypto.getSecureRandomString();
		var flux = Fluxi.subscribeOn(publisher, Scheduli.unlimited());
		flux = Fluxi.logError(flux);
		flux = Fluxi.doOnDone(flux, () -> {
			setURI(id, null);
		});
		flux = flux.map(v -> {
			setURI(id, v);
			return v;
		});
		return flux.last().flatMap(v -> Mono.empty());
	}

	private void setURI(String id, URI addURI) {
		URI removeURI;
		Bytes uriHashPre;
		Bytes uriHashPost;
		synchronized (registrationMap) {
			uriHashPre = hashURIs();
			if (addURI == null)
				removeURI = registrationMap.remove(id);
			else
				removeURI = registrationMap.put(id, addURI);
			uriHashPost = hashURIs();
		}
		if (Objects.equals(uriHashPre, uriHashPost))
			return;
		synchronized (modificationList) {
			if (addURI != null)
				modificationList.add(Utils.Lots.entry(true, addURI));
			if (removeURI != null)
				modificationList.add(Utils.Lots.entry(false, removeURI));
		}
	}

	public StreamEx<URI> streamURIs() {
		return Utils.Lots.stream(registrationMap.values()).distinct();
	}

	public BobBus<Map<Boolean, List<URI>>> getUpdateBus() {
		return updateBus;
	}

	private Bytes hashURIs() {
		return Utils.Crypto.hashMD5(Utils.Lots.stream(registrationMap.values()).sorted().toArray(Object.class));
	}

}
