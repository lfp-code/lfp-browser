package com.lfp.browser.hub.app;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.lfp.browser.hub.app.config.BrowserHubAppConfig;
import com.lfp.caddy.core.CaddyConfigs;
import com.lfp.caddy.run.Caddy;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.handler.MessageHandler;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.events.bobbus.SubscribeOptions;
import com.lfp.joe.net.socket.socks.Sockets;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.utils.Utils;

import io.undertow.Handlers;
import io.undertow.Undertow;
import net.engio.mbassy.listener.Invoke;
import net.engio.mbassy.listener.References;

public class App {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
		var browserNodeImpl = new BrowserNodeImpl();
		Undertow browserHubServer;
		{// build server for browser hub service
			var browserHubImpl = new BrowserHubImpl(browserNodeImpl);
			var serverBuilder = Undertows.serverBuilder(new InetSocketAddress("127.0.0.1", Sockets.allocatePort()));
			serverBuilder.setHandler(
					Handlers.predicate(browserHubImpl, browserHubImpl, new MessageHandler(StatusCodes.NOT_FOUND)));
			browserHubServer = serverBuilder.build();
			browserHubServer.start();
		}
		int adminPort = MachineConfig.isDeveloper() ? 2019 : Sockets.allocatePort();
		var caddyProc = Caddy.start(caddyExec -> {
			return BrowserHubCaddyConfigs.createConfig(caddyExec, adminPort, browserHubServer, null);
		}, Utils.Lots.stream(Configs.get(BrowserHubAppConfig.class).caddyModules()).toArray(String.class));
		var procs = List.of(caddyProc);
		CompletableFuture.anyOf(Utils.Lots.stream(procs).map(v -> v.getFuture()).toArray(CompletableFuture.class))
				.whenComplete((nil, t) -> {
					if (t != null && !Utils.Exceptions.isCancelException(t))
						logger.error("unexpected process error", t);
					procs.forEach(v -> v.getProcess().destroyForcibly());
					System.exit(0);
				});
		CaddyConfigs.adminWait(adminPort).get();
		var caddyExec = Caddy.getExecutable(caddyProc);
		browserNodeImpl.getUpdateBus().subscribe(uriOp -> {
			updateCaddyConfig(caddyExec, adminPort, browserHubServer, browserNodeImpl);
		}, SubscribeOptions.builder().references(References.Strong).delivery(Invoke.Synchronously).build());
		updateCaddyConfig(caddyExec, adminPort, browserHubServer, browserNodeImpl);
		logger.info("listening on:{}:{}", BrowserHubCaddyConfigs.getPublicHost(),
				BrowserHubCaddyConfigs.getPublicPort());
		caddyProc.getFuture().get();
	}

	private static void updateCaddyConfig(File caddyExec, int adminPort, Undertow browserHubServer,
			BrowserNodeImpl browserNodeImpl) {
		var caddyConfig = BrowserHubCaddyConfigs.createConfig(caddyExec, adminPort, browserHubServer,
				browserNodeImpl.streamURIs());
		try {
			CaddyConfigs.adminLoad(adminPort, caddyConfig);
		} catch (IOException e) {
			logger.error("caddy config load failed", e);
		}
	}

}
