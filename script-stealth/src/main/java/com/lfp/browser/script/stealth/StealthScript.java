package com.lfp.browser.script.stealth;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.jsoup.nodes.Document;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.cache.caffeine.writerloader.ByteStreamSerializers;
import com.lfp.joe.cache.caffeine.writerloader.FileWriterLoader;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.process.Procs;
import com.lfp.joe.process.Which;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public enum StealthScript implements Supplier<String> {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 13;
	private static final Duration STORAGE_TTL = Duration.ofDays(3);
	private static final String GITHUB_URL = "https://github.com/berstend/puppeteer-extra/tree/stealth-js";
	private static final String GIT_HACK_TEMPLATE = "https://rawcdn.githack.com/berstend/puppeteer-extra/%s/stealth.min.js";
	private static final String GIT_HACK_FALLBACK_URL = "https://rawcdn.githack.com/berstend/puppeteer-extra/c898625d365e3392b189dd747db82028fde3bbcb/stealth.min.js";
	private static final String NPX_PROGRAM = "extract-stealth-evasions";
	private static final String STEALTH_JS_FILE_NAME = "stealth.js";

	// private static final List<String> EXCLUDE_EVASIONS =
	// List.of("navigator.permissions");
	private static final List<String> EXCLUDE_EVASIONS = List.of();

	private final LoadingCache<Nada, StatValue<List<String>>> fileLoadingCache = new FileWriterLoader<Nada, StatValue<List<String>>>() {

		@Override
		protected File getFile(@NonNull Nada key) {
			var fileParts = Utils.Lots.stream(EXCLUDE_EVASIONS).filter(Utils.Strings::isNotBlank).sorted().distinct()
					.toArray(Object.class);
			String excludeAppend;
			if (fileParts.length > 0)
				excludeAppend = "_" + Utils.Crypto.hashMD5(fileParts).encodeHex();
			else
				excludeAppend = "";
			var file = Utils.Files.tempFile(THIS_CLASS, VERSION,
					String.format("ext_%s%s.js", THIS_CLASS.getName(), excludeAppend));
			return file;
		}

		@Override
		protected ByteStreamSerializer<Nada, StatValue<List<String>>> createByteStreamSerializer() {
			return ByteStreamSerializers.stringList();

		}

		@Override
		protected @Nullable StatValue<List<String>> loadFresh(@NonNull Nada key) throws Exception {
			var script = StealthScript.loadFresh();
			return StatValue.build(List.of(script));
		}

		@Override
		protected @Nullable Duration getStorageTTL(@NonNull Nada key, @NonNull StatValue<List<String>> result) {
			var createdAt = result.getCreatedAt();
			long elapsed = System.currentTimeMillis() - createdAt.getTime();
			long ttl = STORAGE_TTL.toMillis() - elapsed;
			ttl = Math.max(0, ttl);
			return Duration.ofMillis(ttl);
		}

	}.buildCache(Caches.newCaffeineBuilder(-1, Duration.ofDays(1), Duration.ofSeconds(10)));

	@Override
	public String get() {
		var result = Utils.Lots.stream(fileLoadingCache.get(Nada.get()).getValue()).findFirst().orElse(null);
		return result;
	}

	private static String loadFresh() throws IOException {
		Map<String, Callable<String>> loaders = new LinkedHashMap<>();
		loaders.put("npx", () -> loadFreshNPX());
		loaders.put("commit", () -> loadFreshCommit());
		loaders.put("static", () -> loadFreshStatic());
		for (var ent : loaders.entrySet()) {
			var loadMethod = ent.getKey();
			logger.info("load method started:{}", loadMethod);
			var result = Utils.Functions.catching(ent.getValue(), t -> null);
			result = customizeScript(result);
			if (Utils.Strings.isBlank(result))
				logger.warn("load method failed:{}", loadMethod);
			else {
				logger.info("load method success:{}", loadMethod);
				return result;
			}
		}
		throw new IOException("unable to load script. loadMethods:" + Utils.Lots.stream(loaders.keySet()).toList());
	}

	private static String customizeScript(String result) {
		if (Utils.Strings.isBlank(result))
			return result;
		result = String.format(
				"var stealthScriptError=false;\ntry{\n%s\n}catch(e){stealthScriptError=true;console.log(e)}", result);
		// result += "if(navigator.webdriver){Object.defineProperty(navigator,
		// 'webdriver', {get: () => undefined})}";
		result += String.format("console.log(`%s v%s load complete. error:${stealthScriptError}`)",
				THIS_CLASS.getSimpleName(), VERSION);
		return result;
	}

	private static String loadFreshNPX() throws Exception {
		var npxCommand = Which.get("npx").orElse(null);
		if (npxCommand == null)
			return null;
		var workingDirectory = Utils.Files.tempFile(THIS_CLASS, "build", Utils.Crypto.getSecureRandomString());
		workingDirectory.mkdirs();
		try {
			var cmdStream = StreamEx.<Object>of(npxCommand.getAbsolutePath(), NPX_PROGRAM, "--minify", false);
			for (var evasion : EXCLUDE_EVASIONS)
				cmdStream = cmdStream.append("--exclude", evasion);
			var cmdArray = cmdStream.map(v -> v == null ? "" : v.toString()).toArray(String.class);
			var pe = Procs.processExecutor().command(cmdArray).directory(workingDirectory).logOutput();
			pe.execute();
			var fileStream = Utils.Lots.stream(workingDirectory.listFiles());
			int split = Utils.Strings.lastIndexOfIgnoreCase(STEALTH_JS_FILE_NAME, ".");
			fileStream = fileStream.filter(v -> {
				return Utils.Strings.startsWith(v.getName(), STEALTH_JS_FILE_NAME.substring(0, split + 1));
			});
			fileStream = fileStream.filter(v -> {
				return Utils.Strings.endsWith(v.getName(), STEALTH_JS_FILE_NAME.substring(split));
			});
			var files = fileStream.toList();
			Validate.isTrue(!files.isEmpty(), "could not find generated file");
			Validate.isTrue(files.size() == 1, "multiple files generated");
			return Utils.Files.readFileToString(files.get(0));
		} finally {
			FileUtils.deleteDirectory(workingDirectory);
		}
	}

	private static String loadFreshCommit() throws IOException {
		String commit;
		try (var rctx = Ok.Calls.execute(Ok.Clients.get(), GITHUB_URL).validateSuccess().parseHtml()) {
			commit = parseCommit(rctx.getParsedBody());
			Validate.notBlank(commit);
		}
		var uri = URIs.parse(String.format(GIT_HACK_TEMPLATE, commit)).get();
		try (var rctx = Ok.Calls.execute(Ok.Clients.get(), uri).validateSuccess().parseText()) {
			return rctx.getParsedBody();
		}
	}

	private static String parseCommit(Document doc) {
		StreamEx<URI> uriStream = Jsoups.select(doc, "a.js-permalink-shortcut[href]").map(Jsoups::uri);
		uriStream = uriStream.nonNull();
		var commitStream = uriStream.map(v -> {
			for (var split : List.of("/commit/", "/tree/")) {
				var path = v.getPath();
				path = Utils.Strings.substringAfterLast(path, split);
				path = Utils.Strings.substringBefore(path, "/");
				path = Utils.Strings.trimToNull(path);
				if (path != null)
					return path;
			}
			return null;
		});
		commitStream = commitStream.nonNull();
		var commits = commitStream.toSet();
		Validate.isTrue(!commits.isEmpty(), "commit not found");
		Validate.isTrue(commits.size() == 1, "multiple commits found:%s", commits);
		return commits.iterator().next();
	}

	private static String loadFreshStatic() throws IOException {
		try (var rctx = Ok.Calls.execute(Ok.Clients.get(), GIT_HACK_FALLBACK_URL).validateSuccess().parseText()) {
			return rctx.getParsedBody();
		}
	}

	public static void main(String[] args) {
		System.out.println(StealthScript.INSTANCE.get());
	}
}
