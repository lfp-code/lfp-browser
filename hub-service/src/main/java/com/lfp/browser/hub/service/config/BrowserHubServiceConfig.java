package com.lfp.browser.hub.service.config;

import java.net.URI;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.net.http.ServiceConfig;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.properties.converter.DurationConverter;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;
import com.lfp.rsocket.service.RsocketServiceConfig;

import at.favre.lib.bytes.Bytes;
import io.mikael.urlbuilder.UrlBuilder;

public interface BrowserHubServiceConfig extends RsocketServiceConfig {

	@Hashable.Disabled
	@DefaultValue("1366")
	int browserWidth();

	@Hashable.Disabled
	@DefaultValue("768")
	int browserHeight();

	@DefaultValue("/playwright")
	String playwrightPath();

	default URI playwrightURI() {
		return buildURI(this, true, true, playwrightPath());
	}

	@DefaultValue("/webdriver")
	String webDriverPath();

	default URI webDriverURI() {
		return buildURI(this, false, false, webDriverPath());
	}

	@Hashable.Disabled
	default Map<String, String> webDriverCapabilities() {
		Bytes token = this.authorizationTokens().findFirst().orElse(null);
		if (token == null)
			return Map.of();
		return Map.of("browserless.token", token.encodeCharset(Utils.Bits.getDefaultCharset()));
	}

	private static URI buildURI(ServiceConfig serviceConfig, boolean websocket, boolean queryAuth, String path) {
		var uri = Optional.ofNullable(serviceConfig).map(v -> v.uri()).orElse(null);
		if (uri == null)
			return null;
		var urlb = UrlBuilder.fromUri(uri);
		if (websocket)
			urlb = urlb.withScheme(URIs.isSecure(uri) ? "wss" : "ws");
		else
			urlb = urlb.withScheme(URIs.isSecure(uri) ? "https" : "http");
		{
			var uriPath = URIs.normalizePath(uri.getPath());
			path = URIs.normalizePath(path);
			if (!"/".equals(uriPath))
				path = uriPath + path;
			urlb = urlb.withPath(path);
		}
		if (queryAuth) {
			Bytes token = serviceConfig.authorizationTokens().findFirst().orElse(null);
			Map<String, List<String>> queryParameters = new LinkedHashMap<>();
			if (token != null) {
				queryParameters.put("token", List.of(token.encodeCharset(Utils.Bits.getDefaultCharset())));
				queryParameters.put("access_token", List.of(token.encodeBase64Url()));
			}
			for (var ent : Utils.Lots.streamMultimap(queryParameters)) {
				var key = ent.getKey();
				if (Utils.Strings.isBlank(key))
					continue;
				var value = ent.getValue();
				var valueStr = value == null ? "" : value.toString();
				urlb = urlb.addParameter(key, valueStr);
			}
		}
		return urlb.toUri();
	}

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.jsonBuilder().withSkipPopulated(true).build());
		Configs.printProperties(PrintOptions.properties());
	}
}
