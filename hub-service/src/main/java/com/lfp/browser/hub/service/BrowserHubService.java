package com.lfp.browser.hub.service;

import com.lfp.browser.hub.service.config.BrowserHubServiceConfig;
import com.lfp.joe.core.properties.Configs;

public interface BrowserHubService {

	public static BrowserHubServiceConfig config() {
		return Configs.get(BrowserHubServiceConfig.class);
	}

	public static final String PATH = "/hub";
	
	

}
